unit UTimetable;

{$mode ObjFPC}{$H+}

interface

uses
    SysUtils, ULogger,
    UEntity, UEntityList, UStation, URoute,
    UISerializable, UID;

type

    { TTimetable }
    TTimetable = class(TInterfacedObject, ISerializable)
    public
        Stations: specialize TEntityList<TStation>;
        Routes: specialize TEntityList<TRoute>;
        function AddStation(_Name: String = ''; _Order: SizeInt = -1): TID;
        function RemoveStation(_ID: TID): Boolean;
        function AddRoute(_Name: String = ''; _Order: SizeInt = -1): TID;
        function RemoveRoute(_ID: TID): Boolean;
    public
        procedure UpdateAllStationsRoutes();
        procedure UpdateAllStationsDirectionsRoutes();
        procedure UpdateStationsDirectionsRoutesByRoute(RouteID: TID);
    private
        procedure AfterDeserialize();
    public
        function Serialize(): String;
        procedure Deserialize(var S: String);

        constructor Create();
        destructor Destroy(); override;
    end;

    procedure ResetTimetable();

var
    Timetable: TTimetable;

implementation

uses UTrain, UDeserializeUtils;

procedure ResetTimetable();
begin
    FreeAndNil(Timetable);
    Timetable := TTimetable.Create();
end;

{ TTimetable }

function TTimetable.AddStation(_Name: String; _Order: SizeInt): TID;
begin
    if _Order = - 1 then
      Result := Stations.Append(_Name)
    else
      Result := Stations.Insert(_Order, _Name);
end;

function TTimetable.RemoveStation(_ID: TID): Boolean;
begin
    Stations.At(_ID).Purge();
    Result := Stations.Remove(_ID);
end;

function TTimetable.AddRoute(_Name: String; _Order: SizeInt): TID;
begin
    if _Order = - 1 then
        Result := Routes.Append(_Name)
    else
        Result := Routes.Insert(_Order, _Name);
end;

function TTimetable.RemoveRoute(_ID: TID): Boolean;
begin
    Routes.At(_ID).Purge();
    Result := Routes.Remove(_ID);
end;

procedure TTimetable.UpdateAllStationsRoutes();
var Station: TStation;
begin
    for Station in Stations do
        Station.UpdateRoutes();
end;

procedure TTimetable.UpdateAllStationsDirectionsRoutes();
var Station: TStation;
begin
    for Station in Stations do
        Station.UpdateDirectionsRoutes();
end;

procedure TTimetable.UpdateStationsDirectionsRoutesByRoute(RouteID: TID);
var Station: TStation;
begin
    for Station in Stations do
        Station.UpdateDirectionsRoutesIfHasRoute(RouteID);
end;

procedure TTimetable.AfterDeserialize();
var
    Station: TStation;
    Route: TRoute;
    Train: TTrain;
begin
    for Route in Routes do
        for Train in Route.Trains do
            Train.MakeGhosts();
    for Station in Stations do
    begin
        Station.RebuildDirectionsTree();
        Station.UpdateRoutes();
    end;
end;

function TTimetable.Serialize(): String;
begin
    Result := Format('Timetable = { Stations = %s; Routes = %s; }; ',
                     [Stations.Serialize(), Routes.Serialize()]);
end;

procedure TTimetable.Deserialize(var S: String);
var
    TimetableStr, StationsStr, RoutesStr: String;
begin
    ExtractField('Timetable', S, TimetableStr, efBlock);
    TrimBrackets(TimetableStr);
    ExtractField('Stations', TimetableStr, StationsStr, efBlock);
    ExtractField('Routes', TimetableStr, RoutesStr, efBlock);
    Stations.Deserialize(StationsStr);
    Routes.Deserialize(RoutesStr);
    AfterDeserialize();
end;

constructor TTimetable.Create();
begin
    Logger.Log(LERROR, 'Creating timetable');
    Stations := specialize TEntityList<TStation>.Create(station);
    Routes := specialize TEntityList<TRoute>.Create(route);
end;

destructor TTimetable.Destroy();
begin
    Logger.Log(LERROR, 'Deleting timetable');
    FreeAndNil(Stations);
    FreeAndNil(Routes);
end;

finalization
    FreeAndNil(Timetable);

end.

