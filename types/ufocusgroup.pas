unit UFocusGroup;

{$mode ObjFPC}{$H+}

interface

uses
    Classes, SysUtils, Controls, Generics.Collections;

type

    { TFocusGroup }
    TFocusGroup = class(specialize TList<TWinControl>)
    public
        procedure AddWithChildrenRecursive(WinControl: TWinControl);
        function HasFocus(ActiveControl: TWinControl): Boolean;
    end;

implementation

{ TFocusGroup }

procedure TFocusGroup.AddWithChildrenRecursive(WinControl: TWinControl);
var ChildControl: TControl;
begin
    Add(WinControl);
    for ChildControl in WinControl.GetEnumeratorControls do
        if ChildControl is TWinControl then
            AddWithChildrenRecursive(ChildControl as TWinControl);
end;

function TFocusGroup.HasFocus(ActiveControl: TWinControl): Boolean;
begin
    Result := Contains(ActiveControl);
end;

end.

