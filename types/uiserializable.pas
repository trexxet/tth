unit UISerializable;

{$mode ObjFPC}{$H+}
{$modeswitch typehelpers}

interface

type

    ISerializable = interface(IInterface)
        function Serialize(): String;
        procedure Deserialize(var S: String); // throws EFormatError
    end;

implementation

end.

