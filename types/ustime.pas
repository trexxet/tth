unit USTime;

{$mode ObjFPC}{$H+}
{$modeswitch advancedrecords}
{$modeswitch typehelpers}

interface

uses
    SysUtils, UISerializable;

type

    TTimeMode = (GameTime, RealTime);

    { TTimeInt }
    TTimeInt = Word;
    TSmallTimeInt = Byte;

    { TTimeIntHelper }

    TTimeIntHelper = type helper(TWordHelper) for TTimeInt
    public
        const Wrap: TTimeInt = 3600;
        const RealWrap: TTimeInt = 1440;
        const Mins: TSmallTimeInt = 60;
        function GameToReal(): TTimeInt; inline;
        function ModeWrap(): TTimeInt; inline;
    end;

    { THMTime }
    THMTime = record
        H, M: TSmallTimeInt;
        constructor Create(_H, _M: TSmallTimeInt);
    end;

    { TSchTime }
    TSchTime = class(TInterfacedObject, ISerializable)
    private
        FT: TTimeInt;
        GT: THMTime;   // Game time
        RT: THMTime;   // Real time
        function GetH(): TSmallTimeInt;
        function GetM(): TSmallTimeInt;
        function GetHM(): THMTime;
        procedure SetT(_t: TTimeInt);
        function GetModeT(): TTimeInt;
    public
        AutoCalculated, ACInvalid: Boolean;
        procedure SetTime(const _T: THMTime; AutoCalc: Boolean = False);
        property T: TTimeInt read FT write SetT;
        property MT: TTimeInt read GetModeT;
        property H: TSmallTimeInt read GetH;
        property M: TSmallTimeInt read GetM;
        property HM: THMTime read GetHM;

        procedure Unused();
        function IsUnused(): Boolean;

        function ToStr(): String;
        function Serialize(): String;
        procedure Deserialize(var S: String);

        // Returns how much the time need to be adjusted to make an integer
        // number of loops (or HM0 if it fits perfectly into a game day)
        function GetOffTime(out Negative: Boolean): THMTime;
        // Inverse if loop takes multiple game days
        function GetLoopCount(out Inverse: Boolean): SizeUInt;

        constructor FromHMT(_HMT: THMTime; AutoCalc: Boolean = False);
        constructor CreateCopy(const Other: TSchTime);
    end;

    procedure ReplaceSchTime(var OldSch: TSchTime; const NewSch: TSchTime);

    function HMT(_T: TTimeInt): THMTime; overload;
    function HMT(_H, _M: TSmallTimeInt): THMTime; overload;
    operator Explicit(a: THMTime) b: TTimeInt;
    operator=(L, R: THMTime): Boolean;

    operator+(a, b: TSchTime): TSchTime; overload;
    operator+(a: TSchTime; b: TTimeInt): TSchTime; overload;
    operator-(a, b: TSchTime): TSchTime; overload;
    operator-(a: TSchTime; b: TTimeInt): TSchTime; overload;
    operator-(a: TTimeInt; b: TSchTime): TSchTime; overload;
    operator:=(I: THMTime) O: TSchTime; overload;

    procedure ToggleTimeMode();

const
    HM0: THMTime = (H: 0; M: 0);
    HMUnused: THMTime = (H: 255; M: 255);

var
    TimeMode: TTimeMode;

implementation

uses USTimeBounds, ULogger;

procedure ReplaceSchTime(var OldSch: TSchTime; const NewSch: TSchTime);
begin
    FreeAndNil(OldSch);
    OldSch := NewSch;
end;

function HMT(_T: TTimeInt): THMTime;
begin
    Result := THMTime.Create(_T div TTimeInt.Mins, _T mod TTimeInt.Mins);
end;

function HMT(_H, _M: TSmallTimeInt): THMTime;
begin
    Result := THMTime.Create(_H, _M);
end;

operator Explicit(a: THMTime) b: TTimeInt;
begin
    b := a.H * TTimeInt.Mins + a.M;
end;

operator = (L, R: THMTime): Boolean;
begin
    Result := (L.H = R.H) and (L.M = R.M);
end;

operator + (a, b: TSchTime): TSchTime;
begin
    Result := TSchTime.Create();
    Result.T := (a.T + b.T) mod TTimeInt.Wrap;
    Result.AutoCalculated := True;
    Result.ACInvalid := False;
end;

operator + (a: TSchTime; b: TTimeInt): TSchTime;
begin
    Result := TSchTime.Create();
    Result.T := (a.T + b) mod TTimeInt.Wrap;
    Result.AutoCalculated := True;
    Result.ACInvalid := False;
end;

operator - (a, b: TSchTime): TSchTime;
begin
    Result := TSchTime.Create();
    if a.T >= b.T then
        Result.T := a.T - b.T
    else
        Result.T := (a.T  + TTimeInt.Wrap) - b.T;
    Result.AutoCalculated := True;
    Result.ACInvalid := False;
end;

operator - (a: TSchTime; b: TTimeInt): TSchTime;
begin
    Result := TSchTime.Create();
    if a.T >= b then
        Result.T := a.T - b
    else
        Result.T := (a.T  + TTimeInt.Wrap) - b;
    Result.AutoCalculated := True;
    Result.ACInvalid := False;
end;

operator - (a: TTimeInt; b: TSchTime): TSchTime;
begin
    Result := TSchTime.Create();
    Result.T := abs(a - b.T) mod TTimeInt.Wrap;
    Result.AutoCalculated := True;
    Result.ACInvalid := False;
end;

operator := (I: THMTime)O: TSchTime;
begin
    O := TSchTime.FromHMT(I, False);
end;

procedure ToggleTimeMode();
begin
    case TimeMode of
        GameTime: begin
            TimeMode := RealTime;
            Logger.Log(LINFO, 'Set real time mode (24H)');
        end;
        RealTime: begin
            TimeMode := GameTime;
            Logger.Log(LINFO, 'Set game time mode (60H)');
        end;
    end;
end;

{ TTimeIntHelper }

function TTimeIntHelper.GameToReal(): TTimeInt;
begin
    Result := (Self * 2) div 5;  // * 24 / 60
end;

function TTimeIntHelper.ModeWrap(): TTimeInt;
begin
    case TimeMode of
        GameTime: Result := Wrap;
        RealTime: Result := RealWrap;
    end;
end;

{ THMTime }

constructor THMTime.Create(_H, _M: TSmallTimeInt);
begin
    H := _H;
    M := _M;
end;

{ TSchTime }

function TSchTime.GetH(): TSmallTimeInt;
begin
    Result := specialize IfThen<TSmallTimeInt>(TimeMode = GameTime,
                                               GT.H, RT.H);
end;

function TSchTime.GetM(): TSmallTimeInt;
begin
    Result := specialize IfThen<TSmallTimeInt>(TimeMode = GameTime,
                                               GT.M, RT.M);
end;

function TSchTime.GetHM(): THMTime;
begin
    Result := specialize IfThen<THMTime>(TimeMode = GameTime,
                                         GT, RT);
end;

procedure TSchTime.SetT(_t: TTimeInt);
begin
    FT := _t;
    GT := HMT(_t);
    RT := HMT(FT.GameToReal());
end;

function TSchTime.GetModeT(): TTimeInt;
begin
    case TimeMode of
        GameTime: Result := FT;
        RealTime: Result := FT.GameToReal();
    end;
end;

procedure TSchTime.SetTime(const _T: THMTime; AutoCalc: Boolean);
begin
    GT := _T;
    T := TTimeInt(GT);
    AutoCalculated := AutoCalc;
    ACInvalid := False;
end;

procedure TSchTime.Unused();
begin
    FT := 0;
    GT := HMUnused;
    RT := HMUnused;
    AutoCalculated := True;
    ACInvalid := False;
end;

function TSchTime.IsUnused(): Boolean;
begin
   Result := (FT = 0) and (GT = HMUnused);
end;

function TSchTime.ToStr(): String;
begin
    Result := Format('%.2u:%.2u', [H, M]);
end;

function TSchTime.Serialize(): String;
begin
    Result := T.ToString();
    if ACInvalid then
        Result := 'i' + Result;
    if AutoCalculated then
        Result := 'a' + Result;
end;

procedure TSchTime.Deserialize(var S: String);
begin
    AutoCalculated := (S[1] = 'a');
    if AutoCalculated then
        S := S.Remove(0, 1);
    ACInvalid := (S[1] = 'i');
    if ACInvalid then
        S := S.Remove(0, 1);
    T := S.ToInt64;
end;

function TSchTime.GetOffTime(out Negative: Boolean): THMTime;
var
    ClosestRoundTime: TTimeInt;
    Diff: Integer;
begin
    Result := HMUnused;
    if FT = 0 then exit;

    if not GetClosestRoundTime(FT, ClosestRoundTime) then exit;
    Diff := FT - ClosestRoundTime;
    Negative := ExtractSignAndAbs(Diff);
    if TimeMode = GameTime then
        Result := HMT(TTimeInt(Diff))
    else
        Result := HMT(TTimeInt(Diff).GameToReal());
end;

function TSchTime.GetLoopCount(out Inverse: Boolean): SizeUInt;
begin
    if FT = 0 then exit(0);

    Inverse := FT > TTimeInt.Wrap;
    if Inverse then Result := FT div TTimeInt.Wrap
    else Result := TTimeInt.Wrap div FT;
end;

constructor TSchTime.FromHMT(_HMT: THMTime; AutoCalc: Boolean);
begin
    if _HMT = HMUnused then
        Unused()
    else
        SetTime(_HMT, AutoCalc);
end;

constructor TSchTime.CreateCopy(const Other: TSchTime);
begin
    FromHMT(Other.GT, Other.AutoCalculated);
end;

end.

