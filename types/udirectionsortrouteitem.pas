unit UDirectionSortRouteItem;

{$mode ObjFPC}{$H+}

interface

uses
    SysUtils, Generics.Collections, UEntity, USchList, USchIntersect;

type

    { TDirectionSortRoutesItem }
    TDirectionSortRoutesItem = class
    public
        Entity: TEntity;
        SchList: TSchList;
        Intersect: TSchIntersect;
    public
        function AddIfNoIntersect(const SchListItem: TSchListItem): Boolean;
    public
        constructor Create(_Entity: TEntity);
        destructor Destroy(); override;
    end;

    { TDirectionSortRoutesList }
    TDirectionSortRoutesList = class(specialize TObjectList<TDirectionSortRoutesItem>)
        procedure AddWithEntity(const Entity: TEntity);
        procedure AddSchListItem(const SchListItem: TSchListItem;
                                 const Entity: TEntity);
    end;

implementation

{ TDirectionSortRoutesItem }

function TDirectionSortRoutesItem.AddIfNoIntersect(const SchListItem: TSchListItem): Boolean;
begin
    Result := Intersect.AddIfNoIntersect(SchListItem.Sch.Intersect);
    if Result then
        SchList.Add(SchListItem);
end;

constructor TDirectionSortRoutesItem.Create(_Entity: TEntity);
begin
    Entity := _Entity;
    SchList := TSchList.Create();
    Intersect := TSchIntersect.Create();
end;

destructor TDirectionSortRoutesItem.Destroy();
begin
    FreeAndNil(SchList);
    FreeAndNil(Intersect);
    inherited Destroy();
end;

{ TDirectionSortRoutesList }

procedure TDirectionSortRoutesList.AddWithEntity(const Entity: TEntity);
begin
    Add(TDirectionSortRoutesItem.Create(Entity));
end;

procedure TDirectionSortRoutesList.AddSchListItem(const SchListItem: TSchListItem;
    const Entity: TEntity);
var
    SortItem: TDirectionSortRoutesItem;
    SchSorted: Boolean;
begin
    SchSorted := False;
    for SortItem in Self do begin
        if SortItem.AddIfNoIntersect(SchListItem) then begin
            SchSorted := True;
            break;
        end;
    end;

    if not SchSorted then begin
        AddWithEntity(Entity);
        Last.AddIfNoIntersect(SchListItem);
    end;
end;

end.

