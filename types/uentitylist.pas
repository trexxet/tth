unit UEntityList;

{$mode ObjFPC}{$H+}

interface

uses
    Classes, SysUtils, ULogger,
    UHashTableFreeList, UEntity, UID, UISerializable;

type

    { TEntityList }
    generic TEntityList<T: TEntity> = class(TInterfacedObject, ISerializable)
    private
        type _THTFL = specialize THashTableFreeList<T>;
    private
        HTFL: _THTFL;
        EType: TEntity.TEntityType;
    private
        procedure CheckNameID(var Name: String; var ID: TID);
        function At(ID: TID): T;
        function GetCount(): SizeInt; inline;
    public
        function Append(Name: String = ''; ID: TID = 0): TID;
        function Insert(Pos: SizeUInt; Name: String = ''; ID: TID = 0): TID;
        function Exchange(Pos1, Pos2: SizeUInt): Boolean;
        function Move(PosS, PosD: SizeUInt): Boolean;
        function Exists(ID: TID): Boolean;
        function Remove(ID: TID): Boolean;
        function Rename(ID: TID; NewName: String): Boolean;
        function OrderOf(ID: TID): SizeInt;
        function IDOf(Pos: SizeUInt): TID;

        procedure FillIDList(IDL: TIDList);
        property Entities[ID: TID]: T read At; default;
        property Count: SizeInt read GetCount;

        function Serialize(): String;
        procedure Deserialize(var S: String);

        constructor Create(_EType: TEntity.TEntityType);
        destructor Destroy(); override;
    public
        StringList: TStringList; // holds both the name and ID
    {%REGION Enumerator } private
        { TELEnumerator }
        type TELEnumerator = class
        private
            FTEL: TEntityList;
            FPosition: SizeInt;
        public
            constructor Create(TEL: TEntityList);
            function GetCurrent(): T; inline;
            function MoveNext(): Boolean; inline;
            procedure Reset(); inline;
            property Current: T read GetCurrent;
        end;
    {%ENDREGION}
    public
        function GetEnumerator(): TELEnumerator; inline;
    end;

implementation

uses UDeserializeUtils;

{ TEntityList }

function TEntityList.At(ID: TID): T;
begin
    Result := HTFL.At(ID);
    if Result = nil then
        Logger.Log(LERROR, 'ERROR: retrieving %s ID %u: not exist',
                   [TEntity.ETypeToStr(EType), ID]);
end;

function TEntityList.GetCount: SizeInt;
begin
    Result := HTFL.Count();
end;

procedure TEntityList.CheckNameID(var Name: String; var ID: TID);
begin
    if Name = '' then Name := TEntity.ETypeToNewName(EType);
    if ID = 0 then ID := HTFL.ReserveID(ID);
end;

function TEntityList.Append(Name: String; ID: TID): TID;
begin
    CheckNameID(Name, ID);
    Result := HTFL.Add(ID, T.Create(ID, EType, Name));
    StringList.AddObject(Name, TObject(ID));
end;

function TEntityList.Insert(Pos: SizeUInt; Name: String; ID: TID): TID;
begin
    CheckNameID(Name, ID);
    Result := HTFL.AddWithOrder(Pos, ID, T.Create(ID, EType, Name));
    if Result > 0 then
        StringList.InsertObject(Pos, Name, TObject(ID))
    else
        Logger.Log(LERROR, 'ERROR: inserting %s ID %u at position %u: out of bounds',
                   [TEntity.ETypeToStr(EType), ID, Pos]);
end;

function TEntityList.Exchange(Pos1, Pos2: SizeUInt): Boolean;
begin
    Result := HTFL.ExchangeOrder(Pos1, Pos2);
    if Result then StringList.Exchange(Pos1, Pos2);
end;

function TEntityList.Move(PosS, PosD: SizeUInt): Boolean;
begin
    Result := HTFL.MoveOrder(PosS, PosD);
    if Result then StringList.Move(PosS, PosD);
end;

function TEntityList.Exists(ID: TID): Boolean;
begin
    Result := HTFL.Exists(ID);
end;

function TEntityList.Remove(ID: TID): Boolean;
var Order: SizeInt;
begin
    Order := HTFL.OrderOf(ID);
    Result := HTFL.Remove(ID);
    if not Result then
    begin
        Logger.Log(LERROR, 'ERROR: removing %s ID %u: not exist',
                   [TEntity.ETypeToStr(EType), ID]);
        exit;
    end;
    StringList.Delete(Order);
end;

function TEntityList.Rename(ID: TID; NewName: String): Boolean;
var OldName: String;
begin
    if not Exists(ID) then
    begin
        Result := False;
        Logger.Log(LERROR, 'ERROR: renaming %s ID %u: not exist',
           [TEntity.ETypeToStr(EType), ID]);
        exit;
    end;

    OldName := HTFL.At(ID).Name;
    HTFL.At(ID).Name := NewName;
    StringList[HTFL.OrderOf(ID)] := NewName;

    Logger.Log(LINFO, 'Renaming %s ID %u : "%s" -> "%s"',
        [TEntity.ETypeToStr(EType), ID, OldName, NewName]);
    Result := True;
end;

function TEntityList.OrderOf(ID: TID): SizeInt;
begin
    Result := HTFL.OrderOf(ID);
end;

function TEntityList.IDOf(Pos: SizeUInt): TID;
begin
    Result := HTFL.IDByOrder(Pos);
end;

procedure TEntityList.FillIDList(IDL: TIDList);
var Index: SizeUInt;
begin
    if IDL = nil then exit;
    IDL.Clear();
    for Index := 0 to Count do
        IDL.Add(IDOf(Index));
end;

function TEntityList.Serialize(): String;
begin
    Result := Format('{ EList = %s; HTFL = %s; }',
                     [TEntity.ETypeToStr(EType), HTFL.Serialize()]);
end;

procedure TEntityList.Deserialize(var S: String);
var
    EListStr, HTFLStr: String;
    ID: TID;
begin
    TrimBrackets(S);
    ExtractField('EList', S, EListStr, efValue);
    if not TEntity.ETypeFromStr(EListStr, EType) then
        raise EFormatError.Create({$I %CURRENTROUTINE%});
    ExtractField('HTFL', S, HTFLStr, efBlock);
    HTFL.Deserialize(HTFLStr);

    for ID in HTFL.Order do
        StringList.AddObject(HTFL.At(ID).Name, TObject(ID));
end;

constructor TEntityList.Create(_EType: TEntity.TEntityType);
begin
    HTFL := _THTFL.Create();
    EType := _EType;
    StringList := TStringList.Create();
    StringList.OwnsObjects := False;
end;

destructor TEntityList.Destroy();
begin
    FreeAndNil(StringList);
    FreeAndNil(HTFL);
    inherited Destroy();
end;

function TEntityList.GetEnumerator: TELEnumerator;
begin
    Result := TELEnumerator.Create(Self);
end;

{%REGION Enumerator }
{ TEntityList.TELEnumerator }

constructor TEntityList.TELEnumerator.Create(TEL: TEntityList);
begin
    FTEL := TEL;
    FPosition := -1;
end;

function TEntityList.TELEnumerator.GetCurrent(): T;
var HTFL_ID: TID;
begin
    HTFL_ID := FTEL.HTFL.HashTable.Keys.ToArray[FPosition];
    Result := FTEL.HTFL.At(HTFL_ID);
end;

function TEntityList.TELEnumerator.MoveNext(): Boolean;
begin
    FPosition += 1;
    Result := FPosition < FTEL.HTFL.Count;
end;

procedure TEntityList.TELEnumerator.Reset();
begin
    FPosition := -1;
end;

{%ENDREGION}

end.

