unit UTrainSchedule;

{$mode ObjFPC}{$H+}

interface

uses
    SysUtils, Generics.Collections, UStopSchedule, USTime, UID;

type

    TArrDep_CanCalcStatus =
        (ADCC_OK, ADCC_NeedUR, ADCC_Need1Loop, ADCC_FirstArrAC, ADCC_LoopToShort, ADCC_Empty);
    TCanToggleAC = (CTAC_OK, CTAC_IsRD, CTAC_IsFirstArr);

    { TTotalTime }
    TTotalTime = record
        SchTime: TSchTime;
        OffTime: THMTime;
        OffTimeNegative: Boolean;
        LoopCount: SizeUInt;
        LoopCountInverse: Boolean;
    end;

    { TTrainSchedule }
    TTrainSchedule = class(specialize TObjectList<TStopSchedule>)
    // ordered, not unique
    public
        // Looping functions
        function Prev(const StopSch: TStopSchedule): TStopSchedule;
        function Next(const StopSch: TStopSchedule): TStopSchedule;

        // Total time & auto calculate
        procedure TotalTime(var TT: TTotalTime);
        function StopTrav_CanAutoCalc(): Boolean;
        procedure StopTrav_DoAutoCalc();
        function ArrDep_CanAutoCalc(constref _TotalTime: TTotalTime):
            TArrDep_CanCalcStatus;
        procedure ArrDep_DoAutoCalc();
        function CanToggleAC(const StopSchedule: TStopSchedule;
            SchType: TSchType): TCanToggleAC;

        function Serialize(): String;
        procedure Deserialize(var S: String; RouteID, TrainID: TID);
    end;

implementation

uses UDeserializeUtils, ULogger;

{ TTrainSchedule }

function TTrainSchedule.Prev(const StopSch: TStopSchedule): TStopSchedule;
begin
    Result := StopSch;
    if StopSch = nil then exit;

    if StopSch = First then
        Result := Last
    else
        Result := Items[IndexOf(StopSch) - 1];
end;

function TTrainSchedule.Next(const StopSch: TStopSchedule): TStopSchedule;
begin
    Result := StopSch;
    if Result = nil then exit;

    if StopSch = Last then
        Result := First
    else
        Result := Items[IndexOf(StopSch) + 1];
end;

procedure TTrainSchedule.TotalTime(var TT: TTotalTime);
var
    SchT: TSchTime;
    StopSchedule: TStopSchedule;
begin
    SchT := TSchTime.FromHMT(HM0);
    for StopSchedule in Self do
        SchT.T := SchT.T + StopSchedule[Total].T;
    ReplaceSchTime(TT.SchTime, SchT);

    with TT do
    begin
        OffTime := SchTime.GetOffTime(OffTimeNegative);
        if OffTime = HM0 then
            LoopCount := SchTime.GetLoopCount(LoopCountInverse);
    end;
end;

function TTrainSchedule.StopTrav_CanAutoCalc(): Boolean;
var StopSchedule: TStopSchedule;
begin
    Result := Count > 0;
    for StopSchedule in Self do
        with StopSchedule do
        begin
            Result := Result and (not AutoCalculate[Arrival]);
            Result := Result and (not IsRouteTime[Stopping]);
            Result := Result and (not AutoCalculate[Departure]);
            Result := Result and (not IsRouteTime[Travel]);
            if not Result then exit;
        end;
end;

procedure TTrainSchedule.StopTrav_DoAutoCalc();
var StopSchedule: TStopSchedule;
begin
    Logger.PushSeverity(LERROR);
    for StopSchedule in Self do
        with StopSchedule do
        begin
            AutoCalculate[Stopping] := True;
            AutoCalculate[Travel] := True;
        end;
    Logger.PopSeverity();
end;

function TTrainSchedule.ArrDep_CanAutoCalc(constref _TotalTime: TTotalTime):
    TArrDep_CanCalcStatus;
var
    StopSchedule: TStopSchedule;
    OK, StoppingOK, TravelOK: Boolean;
begin
    if Count = 0 then exit(ADCC_Empty);
    Result := ADCC_OK;
    OK := True;

    // Check if first arrival time is not AC
    if First.AutoCalculate[Arrival] then exit(ADCC_FirstArrAC);

    // Check if all Stopping and Travel time are RD or at least not AC
    for StopSchedule in Self do
        with StopSchedule do
        begin
            StoppingOK := IsRouteTime[Stopping] or (not AutoCalculate[Stopping]);
            TravelOK := IsRouteTime[Travel] or (not AutoCalculate[Travel]);
            OK := OK and StoppingOK and TravelOK;
            if not OK then exit(ADCC_NeedUR);
        end;

    with _TotalTime do
    begin
        // Check if offtime is 0
        if (OffTime <> HM0) or LoopCountInverse then
            exit(ADCC_Need1Loop);
        // Check if loop is too short
        if SchTime.T < TTimeInt.Mins then
            exit(ADCC_LoopToShort);
    end;
end;

procedure TTrainSchedule.ArrDep_DoAutoCalc();
var
    StopSchedule: TStopSchedule;
begin
    Logger.PushSeverity(LERROR);
    for StopSchedule in Self do
        with StopSchedule do
        begin
            if StopSchedule <> First then
                AutoCalculate[Arrival] := True;
            AutoCalculate[Departure] := True;
        end;
    Logger.PopSeverity();
end;

function TTrainSchedule.CanToggleAC(const StopSchedule: TStopSchedule;
    SchType: TSchType): TCanToggleAC;
begin
    if StopSchedule.IsRouteTime[SchType] then
        Result := CTAC_IsRD
    else if (StopSchedule.StopIndex = 0) and (SchType = Arrival) then
        Result := CTAC_IsFirstArr
    else
        Result := CTAC_OK;
end;

function TTrainSchedule.Serialize(): String;
var Item: TStopSchedule;
begin
    Result := '{ ';

    if Count > 0 then
    begin
        for Item in Self do
            Result += Item.Serialize() + '; ';
        SetLength(Result, Length(Result) - 1);
    end;

    Result += ' }';
end;

procedure TTrainSchedule.Deserialize(var S: String;
    RouteID, TrainID: TID);
var
    ItemStr: String;
    Item: TStopSchedule;
    StopIndex: SizeUInt = 0;
begin
    TrimBrackets(S);
    while S.Length > 0 do
    begin
        ExtractBracketBlock(S, ItemStr, 0);
        S := S.Remove(0, 1); // cut ;
        Item := TStopSchedule.CreateAndDeserialize(ItemStr,
                                                   RouteID, TrainID, StopIndex);
        Add(Item);
        StopIndex += 1;
    end;
end;

end.

