unit URouteStopTime;

{$mode ObjFPC}{$H+}

interface

uses
    Classes, SysUtils, Generics.Collections,
    UStopSchedule, USTime, UISerializable;

type

    { TRouteStopTime }
    TRouteStopTime = class(TInterfacedObject, ISerializable)
    private
        FStopping, FTravel: TSchTime;
        function GetSch(SchType: TSchType): TSchTime;
        procedure SetSch(SchType: TSchType; const AValue: TSchTime);
    public
        NonStop: Boolean;
        function Serialize(): String;
        procedure Deserialize(var S: String);
        property Tm[SchType: TSchType]: TSchTime
            read GetSch write SetSch; default;

        procedure CopyTimesFrom(const Other: TRouteStopTime);

        constructor CreateAndDeserialize(var S: String);
        constructor Create0();
        constructor Create();
        destructor Destroy(); override;
    end;

    TRouteStopTimes = specialize TObjectList<TRouteStopTime>;

    { TStopTimesSerializer }
    TStopTimesSerializer = class helper for TRouteStopTimes
        function Serialize(): String;
        procedure Deserialize(var S: String);
    end;

implementation

uses UDeserializeUtils;

{ TRouteStopTime }

function TRouteStopTime.GetSch(SchType: TSchType): TSchTime;
begin
    case SchType of
        Arrival, Departure, Total:
            raise EArgumentOutOfRangeException.Create(
                'Route stop time can be retrieved for Stop and Trav only');
        Stopping: Result := FStopping;
        Travel:   Result := FTravel;
    end;
end;

procedure TRouteStopTime.SetSch(SchType: TSchType; const AValue: TSchTime);
begin
    case SchType of
        Arrival, Departure, Total: exit;
        Stopping: ReplaceSchTime(FStopping, AValue);
        Travel:   ReplaceSchTime(FTravel,   AValue);
    end;
end;

function TRouteStopTime.Serialize(): String;
begin
    Result := Format('{ Stop = %s; Trav = %s; NonStop = %s; }',
                      [FStopping.Serialize(), FTravel.Serialize(),
                       BoolToStr(NonStop, True)]);
end;

procedure TRouteStopTime.Deserialize(var S: String);
var FieldStr: String;
begin
    TrimBrackets(S);

    ExtractField('Stop', S, FieldStr, efValue);
    FStopping.Deserialize(FieldStr);

    ExtractField('Trav', S, FieldStr, efValue);
    FTravel.Deserialize(FieldStr);

    ExtractField('NonStop', S, FieldStr, efValue);
    try
        NonStop := StrToBool(FieldStr);
    except
        on EConvertError do
            raise EFormatError.Create({$I %CURRENTROUTINE%});
    end;
end;

procedure TRouteStopTime.CopyTimesFrom(const Other: TRouteStopTime);
begin
    Tm[Stopping] := TSchTime.CreateCopy(Other.FStopping);
    Tm[Travel]   := TSchTime.CreateCopy(Other.FTravel);
    NonStop      := Other.NonStop;
end;

constructor TRouteStopTime.CreateAndDeserialize(var S: String);
begin
    Create();
    Deserialize(S);
end;

constructor TRouteStopTime.Create0();
begin
    inherited Create();
    FStopping := TSchTime.FromHMT(HM0);
    FTravel   := TSchTime.FromHMT(HM0);
    NonStop := False;
end;

constructor TRouteStopTime.Create();
begin
    inherited Create();
    FStopping := TSchTime.Create();
    FTravel   := TSchTime.Create();
    NonStop := False;
end;

destructor TRouteStopTime.Destroy();
begin
    FreeAndNil(FStopping);
    FreeAndNil(FTravel);
    inherited Destroy();
end;

{ TStopTimesSerializer }

function TStopTimesSerializer.Serialize(): String;
var Item: TRouteStopTime;
begin
    Result := '{ ';

    if Count > 0 then
    begin
        for Item in Self do
            Result += Item.Serialize() + '; ';
        SetLength(Result, Length(Result) - 1);
    end;

    Result += ' }';
end;

procedure TStopTimesSerializer.Deserialize(var S: String);
var
    ItemStr: String;
    Item: TRouteStopTime;
begin
    TrimBrackets(S);
    while S.Length > 0 do
    begin
        ExtractBracketBlock(S, ItemStr, 0);
        S := S.Remove(0, 1); // cut ;
        Item := TRouteStopTime.CreateAndDeserialize(ItemStr);
        Add(Item);
    end;
end;

end.

