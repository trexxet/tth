unit UHashTableFreeList;

{$mode ObjFPC}{$H+}

interface

uses
    Classes, SysUtils, Generics.Collections, gpriorityqueue, gutil,
    UEntity, UID, UISerializable;

type

    { THashTableFreeList }
    generic THashTableFreeList<T: TEntity> = class(TInterfacedObject, ISerializable)
    private
        type _THashTable = specialize TObjectDictionary<TID, T>;
        type _TFreeList = specialize TPriorityQueue<TID, specialize TGreater<TID>>;
    private
        HashTable: _THashTable;
        FreeList: _TFreeList;
        Order: TIDList;
    public
        function ReserveID(ID: TID): TID;
        function Add(ID: TID; constref Item: T): TID;
        function Exists(ID: TID): Boolean;
        function Remove(ID: TID): Boolean;
        function At(ID: TID): T;
        function Count(): SizeInt;

        function AddWithOrder(Pos: SizeUInt; ID: TID; constref Item: T): TID;
        function ExchangeOrder(Pos1, Pos2: SizeUInt): Boolean;
        function MoveOrder(PosS, PosD: SizeUInt): Boolean;
        function IDByOrder(Pos: SizeUInt): TID;
        function AtByOrder(Pos: SizeUInt): T;
        function OrderOf(ID: TID): SizeInt;

        function Serialize(): String;
        procedure Deserialize(var S: String);

        constructor Create();
        destructor Destroy(); override;
    end;

implementation

uses UDeserializeUtils;

{ THashTableFreeList }

function THashTableFreeList.ReserveID(ID: TID): TID;
begin
    if ID = 0 then
    begin
        if FreeList.IsEmpty() then
            ID := HashTable.Count + 1
        else
        begin
            ID := FreeList.Top();
            FreeList.Pop();
        end;
    end;
    Result := ID;
end;

function THashTableFreeList.Add(ID: TID; constref Item: T): TID;
begin
    HashTable.Add(ID, Item);
    Order.Add(ID);
    Result := ID;
end;

function THashTableFreeList.Exists(ID: TID): Boolean;
begin
    Result := HashTable.ContainsKey(ID);
end;

function THashTableFreeList.Remove(ID: TID): Boolean;
begin
    Result := False;
    if Exists(ID) then begin
        FreeList.Push(ID);
        HashTable.Remove(ID);
        Order.Remove(ID);
        Result := True;
    end;
end;

function THashTableFreeList.At(ID: TID): T;
begin
    Result := nil;
    if Exists(ID) then Result := HashTable.Items[ID];
end;

function THashTableFreeList.Count: SizeInt;
begin
    Result := HashTable.Count;
end;

function THashTableFreeList.AddWithOrder(Pos: SizeUInt; ID: TID; constref
    Item: T): TID;
begin
    if (Pos > Order.Count) then
    begin
        Result := 0;
        exit;
    end;
    HashTable.Add(ID, Item);
    Order.Insert(Pos, ID);
    Result := ID;
end;

function THashTableFreeList.ExchangeOrder(Pos1, Pos2: SizeUInt): Boolean;
begin
    if (Pos1 >= Order.Count) or (Pos2 >= Order.Count) then
    begin
        Result := False;
        exit;
    end;
    Order.Exchange(Pos1, Pos2);
    Result := True;
end;

function THashTableFreeList.MoveOrder(PosS, PosD: SizeUInt): Boolean;
begin
    if (PosS >= Order.Count) or (PosD >= Order.Count) then
    begin
        Result := False;
        exit;
    end;
    Order.Move(PosS, PosD);
    Result := True;
end;

function THashTableFreeList.IDByOrder(Pos: SizeUInt): TID;
begin
    if Pos < Order.Count then
        Result := Order[Pos]
    else
        Result := 0;
end;

function THashTableFreeList.AtByOrder(Pos: SizeUInt): T;
begin
    Result := At(IDByOrder(Pos));
end;

function THashTableFreeList.OrderOf(ID: TID): SizeInt;
begin
    Result := Order.IndexOf(ID);
end;

function THashTableFreeList.Serialize(): String;
var
    Item: T;
    ID: TID;
begin
    Result := '{ HashTable = { ';

    if HashTable.Count > 0 then
    begin
        for Item in HashTable.Values do
            Result += Item.Serialize() + '; ';
        SetLength(Result, Length(Result) - 1);
    end;

    Result += ' }; FreeList = { ';

    if not FreeList.IsEmpty() then
    begin
        for ID in FreeList.FData do
            Result += ID.ToString() + ', ';
        SetLength(Result, Length(Result) - 2);
    end;

    Result += ' }; Order = ';
    Result += Order.Serialize();
    Result += '; }';
end;

procedure THashTableFreeList.Deserialize(var S: String);
var
    FieldStr, HTItemStr: String;
    Item: T;
    IDList: TIDList;
    ID: TID;
begin
    TrimBrackets(S);
    ExtractField('HashTable', S, FieldStr, efBlock);
    TrimBrackets(FieldStr);
    while FieldStr.Length > 0 do
    begin
        ExtractBracketBlock(FieldStr, HTItemStr, 0);
        FieldStr := FieldStr.Remove(0, 1); // cut ;
        Item := T.CreateAndDeserialize(HTItemStr);
        Add(Item.ID, Item);
    end;

    ExtractField('FreeList', S, FieldStr, efBlock);
    IDList := TIDList.Create();
    try
        IDList.Deserialize(FieldStr);
        for ID in IDList do
            FreeList.Push(ID);
    finally
        FreeAndNil(IDList);
    end;

    ExtractField('Order', S, FieldStr, efBlock);
    Order.Deserialize(FieldStr);
end;

constructor THashTableFreeList.Create();
begin
    inherited Create();
    HashTable := _THashTable.Create([doOwnsValues]);
    FreeList := _TFreeList.Create();
    Order := TIDList.Create();
end;

destructor THashTableFreeList.Destroy();
begin
    FreeAndNil(HashTable);
    FreeAndNil(FreeList);
    FreeAndNil(Order);
    inherited Destroy();
end;

end.

