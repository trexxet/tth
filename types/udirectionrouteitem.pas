unit UDirectionRouteItem;

{$mode ObjFPC}{$H+}
{$modeswitch advancedrecords}

interface

uses
    Classes, SysUtils, Generics.Collections,
    UEntity;

type
    { TDirectionRouteItem }
    TDirectionRouteItem = record
        Entity: TEntity;
        StopIndex: SizeUInt;
        HasStop, HasNonStop: Boolean;
        class function Make(_Entity: TEntity; _StopIndex: SizeUInt;
            _HasStop, _HasNonStop: Boolean): TDirectionRouteItem; static;
    end;
    TDirectionRouteList = specialize TList<TDirectionRouteItem>;

    TDRIndexList = specialize TList<SizeUInt>;

implementation

{ TDirectionRouteItem }

class function TDirectionRouteItem.Make(_Entity: TEntity; _StopIndex: SizeUInt;
    _HasStop, _HasNonStop: Boolean): TDirectionRouteItem;
begin
    with Result do begin
        Entity := _Entity;
        StopIndex := _StopIndex;
        HasStop := _HasStop;
        HasNonStop := _HasNonStop;
    end;
end;

end.

