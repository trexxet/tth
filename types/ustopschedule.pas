unit UStopSchedule;

{$mode ObjFPC}{$H+}

interface

uses
    SysUtils, USchIntersect, USTime, UID, UISerializable;

type
    // Arrival and Departure are times of day
    // Stopping, Travel and Total are intervals
    // Total = Stopping + Travel
    TSchType = (Arrival, Stopping, Departure, Travel, Total);

    { TSchTimes }
    TSchTimes = class(TObject)
    private
       Arr, Stop, Dep, Trav, Tot: TSchTime;
       function GetSch(SchType: TSchType): TSchTime;
       procedure SetSch(SchType: TSchType; const AValue: TSchTime);
    public
       property SchTimes[SchType: TSchType]: TSchTime
           read GetSch write SetSch; default;
       constructor Create();
       destructor Destroy(); override;
    end;

    { TStopSchedule }
    TStopSchedule = class(TInterfacedObject, ISerializable)
    private
        Initialized: Boolean;
        FGhost: SizeInt;
        FSchTimes: TSchTimes;
        FNonStop, FUseRouteNS: Boolean;
        FAutoCalc: Array[TSchType] of Boolean;
    private
        function GetIsGhost(): Boolean;
        function GetNS(): Boolean;
        procedure SetNS(const AValue: Boolean);
        procedure SetUseRouteNS(const AValue: Boolean);
        function GetTotal(): TSchTime;
        function GetSch(SchType: TSchType): TSchTime;
        procedure SetSch(SchType: TSchType; const AValue: TSchTime);
        function GetAutoCalc(SchType: TSchType): Boolean;
        procedure SetAutoCalc(SchType: TSchType; const AValue: Boolean);
        function GetACInvalid(SchType: TSchType): Boolean;
        procedure SetACInvalid(SchType: TSchType; const AValue: Boolean);
        function GetIsRouteTime(SchType: TSchType): Boolean;
        procedure SetIsRouteTime(SchType: TSchType; const AValue: Boolean);
    public
        Station, Train, Route: TID;
        StopIndex: SizeUInt;
        Intersect: TSchIntersect;
    public
        property IsGhost: Boolean read GetIsGhost;
        property SchTimes[SchType: TSchType]: TSchTime
            read GetSch write SetSch; default;
        property AutoCalculate[SchType: TSchType]: Boolean
            read GetAutoCalc write SetAutoCalc;
        property AutoCalcInvalid[SchType: TSchType]: Boolean
            read GetACInvalid write SetACInvalid;
        property IsRouteTime[SchType: TSchType]: Boolean
            read GetIsRouteTime write SetIsRouteTime;
        property NonStop: Boolean read GetNS write SetNS;
        property UseRouteNS: Boolean read FUseRouteNS write SetUseRouteNS;
    private
        FNeighborsInvalid: Boolean;
        FNeighborsRecalculated: Boolean;
        function GetNeighborsInvalid(): Boolean;
        function GetNeighborsRecalculated(): Boolean;
        function GetCanInvalidate(SchType: TSchType): Boolean; inline;
        function GetCanRecalculate(SchType: TSchType): Boolean; inline;
        property CanInvalidate[SchType: TSchType]: Boolean
            read GetCanInvalidate;
        property CanRecalculate[SchType: TSchType]: Boolean
            read GetCanRecalculate;
    public
        // Returns True if any neighbor stop schedule is invalidated
        property NeighborsInvalid: Boolean read GetNeighborsInvalid;
        property NeighborsRecalculated: Boolean read GetNeighborsRecalculated;
        function TryInvalidate(SchType: TSchType): Boolean; inline;
        function InvalidateDependencies(SchType: TSchType): Boolean;
        function TryRecalculate(SchType: TSchType): Boolean; inline;
        function RecalculateDependencies(SchType: TSchType): Boolean;
        function Calc(SchType: TSchType; DoInvalidate: Boolean): Boolean;
        procedure UpdateIntersect();
    public
        function ToStr(): String;
        function Serialize(): String;
        procedure Deserialize(var S: String);
        constructor CreateAndDeserialize(var S: String; RouteID, TrainID: TID;
                           _StopIndex: SizeUInt);
        constructor Create(StationID, RouteID, TrainID: TID;
                           _StopIndex: SizeUInt);
        constructor CreateGhost(Other: TStopSchedule);
        destructor Destroy(); override;
    private
        procedure Init();
    end;

    function TSchTypeToStr(SchType: TSchType): String;

const SchType_CanAutoCalc = [Arrival..Travel];
const SchType_RouteTimes = [Stopping, Travel];
const SchType_TrainOnly = [Arrival, Departure, Total];

implementation

uses UTimetable, UTrain, UDeserializeUtils, ULogger;

function TSchTypeToStr(SchType: TSchType): String;
var
    SchTypeStr: String;
begin
    Str(SchType, SchTypeStr);
    Result := SchTypeStr;
end;

{ TSchTimes }

function TSchTimes.GetSch(SchType: TSchType): TSchTime;
begin
    case SchType of
        Arrival:   Result := Arr;
        Stopping:  Result := Stop;
        Departure: Result := Dep;
        Travel:    Result := Trav;
        Total:     Result := Tot;
    end;
end;

procedure TSchTimes.SetSch(SchType: TSchType; const AValue: TSchTime);
begin
    case SchType of
        Arrival:   ReplaceSchTime(Arr,  AValue);
        Stopping:  ReplaceSchTime(Stop, AValue);
        Departure: ReplaceSchTime(Dep,  AValue);
        Travel:    ReplaceSchTime(Trav, AValue);
        Total:     raise ERangeError.Create({$I %CURRENTROUTINE%});
    end;
end;

constructor TSchTimes.Create();
begin
    inherited Create();
    Arr  := TSchTime.FromHMT(HM0);
    Stop := TSchTime.FromHMT(HMUnused);
    Dep  := TSchTime.FromHMT(HM0);
    Trav := TSchTime.FromHMT(HMUnused);
    Tot  := TSchTime.FromHMT(HM0);
end;

destructor TSchTimes.Destroy();
begin
    FreeAndNil(Arr);
    FreeAndNil(Stop);
    FreeAndNil(Dep);
    FreeAndNil(Trav);
    FreeAndNil(Tot);
    inherited Destroy();
end;

{ TStopSchedule }

function TStopSchedule.GetSch(SchType: TSchType): TSchTime;
begin
    if IsRouteTime[SchType] then
        Result := Timetable.Routes[Route].GetStopTime(StopIndex, SchType)
    else if SchType = Total then
        Result := GetTotal()
    else
        Result := FSchTimes[SchType];
end;

function TStopSchedule.GetIsGhost(): Boolean;
begin
    Result := FGhost >= 0;
end;

function TStopSchedule.GetNS(): Boolean;
begin
    if UseRouteNS then
        Result := Timetable.Routes[Route].GetNS(StopIndex)
    else
        Result := FNonStop;
end;

function TStopSchedule.GetAutoCalc(SchType: TSchType): Boolean;
begin
    if SchType in SchType_CanAutoCalc then
        Result := FAutoCalc[SchType]
    else
        raise ERangeError.Create({$I %CURRENTROUTINE%});
end;

procedure TStopSchedule.SetAutoCalc(SchType: TSchType; const AValue: Boolean);
begin
    if SchType in SchType_CanAutoCalc then
        FAutoCalc[SchType] := AValue
    else
        raise ERangeError.Create({$I %CURRENTROUTINE%});
    if AValue then Calc(SchType, True)
    else begin
        FSchTimes[SchType].AutoCalculated := False;
        FSchTimes[SchType].ACInvalid := False;
    end;
    if not IsGhost then Logger.Log(LINFO,
        'Set auto calculate = %s for %s of stop %u ''%s'' of train %s of route %s',
        [BoolToStr(AValue, True), TSchTypeToStr(SchType), StopIndex,
         Timetable.Stations[Station].Name,
         Timetable.Routes[Route].Trains[Train].IdNameStr(),
         Timetable.Routes[Route].IDNameStr()]);
end;

function TStopSchedule.GetACInvalid(SchType: TSchType): Boolean;
begin
    if SchType in SchType_CanAutoCalc then
        Result := FSchTimes[SchType].ACInvalid
    else
        raise ERangeError.Create({$I %CURRENTROUTINE%});
end;

procedure TStopSchedule.SetACInvalid(SchType: TSchType; const AValue: Boolean);
begin
    if SchType in SchType_CanAutoCalc then
        FSchTimes[SchType].ACInvalid := AValue
    else
        raise ERangeError.Create({$I %CURRENTROUTINE%});
end;

function TStopSchedule.GetIsRouteTime(SchType: TSchType): Boolean;
begin
    Result := (SchType in SchType_RouteTimes) and FSchTimes[SchType].IsUnused();
end;

procedure TStopSchedule.SetIsRouteTime(SchType: TSchType;
    const AValue: Boolean);
var OldT: TTimeInt;
begin
    if SchType in SchType_TrainOnly then exit;
    if AValue = FSchTimes[SchType].IsUnused() then exit;
    if AValue then
    begin
        OldT := FSchTimes[SchType].T;
        // set unused = set use route default
        FSchTimes[SchType] := TSchTime.FromHMT(HMUnused);
        if OldT <> GetSch(SchType).T then
            InvalidateDependencies(SchType);
    end else
    begin
        FSchTimes[SchType] := TSchTime.CreateCopy(GetSch(SchType));
        FSchTimes[SchType].AutoCalculated := False;
    end;

    if not Initialized then exit;
    Logger.Log(LINFO,
               'Set use route time = %s for %s of stop %u ''%s'' of train %s of route %s',
               [BoolToStr(AValue, True), TSchTypeToStr(SchType), StopIndex,
                Timetable.Stations[Station].Name,
                Timetable.Routes[Route].Trains[Train].IdNameStr(),
                Timetable.Routes[Route].IDNameStr()]);
end;

procedure TStopSchedule.SetNS(const AValue: Boolean);
begin
    FNonStop := AValue;
    FUseRouteNS := False;
    Timetable.Stations[Station].UpdateDirectionsRoutes();
end;

procedure TStopSchedule.SetUseRouteNS(const AValue: Boolean);
begin
    FUseRouteNS := AValue;
    if FUseRouteNS then
        FNonStop := False
    else if Initialized then
        FNonStop := Timetable.Routes[Route].GetNS(StopIndex);

    if not Initialized then exit;
    Logger.Log(LINFO,
               'Set use route NS = %s of stop %u ''%s'' of train %s of route %s',
               [BoolToStr(AValue, True), StopIndex,
                Timetable.Stations[Station].Name,
                Timetable.Routes[Route].Trains[Train].IdNameStr(),
                Timetable.Routes[Route].IDNameStr()]);
end;

function TStopSchedule.GetTotal(): TSchTime;
begin
    FSchTimes[Total].T := GetSch(Stopping).T + GetSch(Travel).T;
    Result := FSchTimes[Total];
end;

procedure TStopSchedule.SetSch(SchType: TSchType; const AValue: TSchTime);
var
    OwnerTrain: TTrain;
    NeedInvalidate: Boolean;
begin
    if SchType = Total then
        raise ERangeError.Create({$I %CURRENTROUTINE%});
    NeedInvalidate := FSchTimes[SchType].T <> AValue.T;
    FSchTimes[SchType] := AValue;

    if SchType in SchType_TrainOnly then
        UpdateIntersect();

    if not IsGhost then Logger.Log(LINFO,
        'Set time [%s] for %s of stop %u ''%s'' of train %s of route %s',
        [AValue.ToStr(), TSchTypeToStr(SchType), StopIndex,
         Timetable.Stations[Station].Name,
         Timetable.Routes[Route].Trains[Train].IdNameStr(),
         Timetable.Routes[Route].IDNameStr()]);

    // Invalidate & recalculate
    if NeedInvalidate then InvalidateDependencies(SchType);
    if IsGhost then exit;
    OwnerTrain := Timetable.Routes[Route].Trains[Train];
    if OwnerTrain = nil then exit;
    if OwnerTrain.AutoRecalc then
    begin
        RecalculateDependencies(SchType);
        if OwnerTrain.HasGhosts() then OwnerTrain.RecalcGhosts();
    end;
end;

function TStopSchedule.GetCanInvalidate(SchType: TSchType): Boolean; inline;
begin
    Result := (SchType in SchType_CanAutoCalc)
          and (AutoCalculate[SchType])
          and (not AutoCalcInvalid[SchType])
          and (not IsRouteTime[SchType]);
end;

function TStopSchedule.GetCanRecalculate(SchType: TSchType): Boolean;
begin
    Result := (SchType in SchType_CanAutoCalc)
          and (AutoCalculate[SchType])
          and AutoCalcInvalid[SchType]
          and (not IsRouteTime[SchType]);;
end;

function TStopSchedule.GetNeighborsInvalid(): Boolean;
begin
    Result := FNeighborsInvalid;
    FNeighborsInvalid := False;
end;

function TStopSchedule.GetNeighborsRecalculated: Boolean;
begin
    Result := FNeighborsRecalculated;
    FNeighborsRecalculated := False;
end;

function TStopSchedule.TryInvalidate(SchType: TSchType): Boolean;
begin
    Result := CanInvalidate[SchType];
    if Result then
    begin
        AutoCalcInvalid[SchType] := True;

        Logger.Log(LINFO,
                   'Invalidated auto calculated time for %s of stop %u ''%s'' of train %s of route %s',
                   [TSchTypeToStr(SchType), StopIndex,
                    Timetable.Stations[Station].Name,
                    Timetable.Routes[Route].Trains[Train].IdNameStr(),
                    Timetable.Routes[Route].IDNameStr()]);

        // Recursively screw the whole timetable
        InvalidateDependencies(SchType);
    end;
end;

function TStopSchedule.InvalidateDependencies(SchType: TSchType): Boolean;
var
    OwnerTrain: TTrain;
    NeighborSchedule: TStopSchedule;
begin
    Result := False;
    OwnerTrain := Timetable.Routes[Route].Trains[Train];
    if OwnerTrain = nil then exit;

    case SchType of
        Arrival: begin
            // Invalidate self Stopping and Departure
            TryInvalidate(Stopping);
            TryInvalidate(Departure);
            // Invalidate prev Travel
            NeighborSchedule := OwnerTrain.SchedulePrev(Self, IsGhost);
            Result := NeighborSchedule.TryInvalidate(Travel);
        end;
        Stopping: begin
            // Invalidate self Departure
            TryInvalidate(Departure);
        end;
        Departure: begin
            // Invalidate self Stopping and Travel
            TryInvalidate(Stopping);
            TryInvalidate(Travel);
            // Invalidate next Arrival
            NeighborSchedule := OwnerTrain.ScheduleNext(Self, IsGhost);
            Result := NeighborSchedule.TryInvalidate(Arrival);
        end;
        Travel: begin
            // Invalidate next Arrival
            NeighborSchedule := OwnerTrain.ScheduleNext(Self, IsGhost);
            Result := NeighborSchedule.TryInvalidate(Arrival);
        end;
        Total: begin
            // Invalidate self and neighbours
            Result := InvalidateDependencies(Arrival);
            Result := InvalidateDependencies(Travel) or Result;
        end;
    end;
    FNeighborsInvalid := FNeighborsInvalid or Result;
end;

function TStopSchedule.TryRecalculate(SchType: TSchType): Boolean;
begin
    Result := CanRecalculate[SchType];
    if Result then
    begin
        Calc(SchType, False);
        // Recursively recalculate the whole timetable
        RecalculateDependencies(SchType);
    end;
end;

function TStopSchedule.RecalculateDependencies(SchType: TSchType): Boolean;
var
    OwnerTrain: TTrain;
    NeighborSchedule: TStopSchedule;
begin
    Result := False;
    OwnerTrain := Timetable.Routes[Route].Trains[Train];
    if OwnerTrain = nil then exit;

    case SchType of
        Arrival: begin
            // Recalculate self Stopping and Departure
            TryRecalculate(Stopping);
            TryRecalculate(Departure);
            // Recalculate prev Travel
            NeighborSchedule := OwnerTrain.SchedulePrev(Self, IsGhost);
            Result := NeighborSchedule.TryRecalculate(Travel);
        end;
        Stopping: begin
            // Recalculate self Departure
            TryRecalculate(Departure);
        end;
        Departure: begin
            // Recalculate self Stopping and Travel
            TryRecalculate(Stopping);
            TryRecalculate(Travel);
            // Recalculate next Arrival
            NeighborSchedule := OwnerTrain.ScheduleNext(Self, IsGhost);
            Result := NeighborSchedule.TryRecalculate(Arrival);
        end;
        Travel: begin
            // Recalculate next Arrival
            NeighborSchedule := OwnerTrain.ScheduleNext(Self, IsGhost);
            Result := NeighborSchedule.TryRecalculate(Arrival);
        end;
        Total: begin
            // Recalculate self and neighbours
            Result := TryRecalculate(Arrival);
            Result := TryRecalculate(Stopping) or Result;
            Result := TryRecalculate(Departure) or Result;
            Result := TryRecalculate(Travel) or Result;
        end;
    end;
    FNeighborsRecalculated := FNeighborsRecalculated or Result;
end;

function TStopSchedule.Calc(SchType: TSchType; DoInvalidate: Boolean): Boolean;
var
    OwnerTrain: TTrain;
    NeighborSchedule: TStopSchedule;
    OldT: TTimeInt;
begin
    Result := False;
    OwnerTrain := Timetable.Routes[Route].Trains[Train];
    if OwnerTrain = nil then exit;
    if SchType = Total then raise ERangeError.Create({$I %CURRENTROUTINE%});
    OldT := FSchTimes[SchType].T;

    case SchType of
        Arrival: begin
            NeighborSchedule := OwnerTrain.SchedulePrev(Self, IsGhost);
            FSchTimes[Arrival] :=
                NeighborSchedule[Departure] + NeighborSchedule[Travel];
            UpdateIntersect();
        end;
        Stopping: begin
            if IsRouteTime[SchType] then exit;
            FSchTimes[Stopping] := FSchTimes[Departure] - FSchTimes[Arrival];
        end;
        Departure: begin
            FSchTimes[Departure] := FSchTimes[Arrival] + GetSch(Stopping);
            UpdateIntersect();
        end;
        Travel: begin
            if IsRouteTime[SchType] then exit;
            NeighborSchedule := OwnerTrain.ScheduleNext(Self, IsGhost);
            FSchTimes[Travel] :=
                NeighborSchedule[Arrival] - FSchTimes[Departure];
        end;
    end;

    if not FAutoCalc[SchType] then FAutoCalc[SchType] := True;
    AutoCalcInvalid[SchType] := False;
    // Do not invalidate if nothing has changed
    if DoInvalidate and (OldT <> FSchTimes[SchType].T) then
        Result := InvalidateDependencies(SchType);
end;

procedure TStopSchedule.UpdateIntersect();
begin
    Intersect.SetFromArrDep(FSchTimes[Arrival], FSchTimes[Departure]);
end;

function TStopSchedule.ToStr(): String;
begin
    Result := Format('Station=%u Train=%u NonStop=%s Arr=[%s] Stop=[%s] Dep=[%s] Trav=[%s]',
                     [Station, Train, BoolToStr(NonStop, True),
                      FSchTimes[Arrival].ToStr(), FSchTimes[Stopping].ToStr(),
                      FSchTimes[Departure].ToStr(), FSchTimes[Travel].ToStr()]);
end;

function TStopSchedule.Serialize(): String;
var NSStr, StopStr, TravStr: String;
begin
    if UseRouteNS then NSStr := 'route'
    else NSStr := BoolToStr(FNonStop, True);
    Result := Format('{ Station = %u; NonStop = %s; ', [Station, NSStr]);

    if FSchTimes[Stopping].IsUnused() then StopStr := 'route'
    else StopStr := FSchTimes[Stopping].Serialize();
    if FSchTimes[Travel].IsUnused() then TravStr := 'route'
    else TravStr := FSchTimes[Travel].Serialize();

    Result += Format('Arr = %s; Stop = %s; Dep = %s; Trav = %s; }',
                     [FSchTimes[Arrival].Serialize(), StopStr,
                      FSchTimes[Departure].Serialize(), TravStr]);
end;

procedure TStopSchedule.Deserialize(var S: String);
var
    FieldStr: String;
    UseRouteTime: Boolean;
begin
    TrimBrackets(S);
    ExtractField('Station', S, FieldStr, efValue);
    Station := FieldStr.ToInt64;

    ExtractField('NonStop', S, FieldStr, efValue);
    UseRouteNS := (FieldStr = 'route');
    if not UseRouteNS then
        try
            FNonStop := StrToBool(FieldStr);
        except
            on EConvertError do
                raise EFormatError.Create({$I %CURRENTROUTINE%});
        end;

    ExtractField('Arr', S, FieldStr, efValue);
    FSchTimes[Arrival].Deserialize(FieldStr);
    FAutoCalc[Arrival] := FSchTimes[Arrival].AutoCalculated;

    ExtractField('Stop', S, FieldStr, efValue);
    UseRouteTime := (FieldStr = 'route');
    if UseRouteTime then FSchTimes[Stopping].Unused()
    else FSchTimes[Stopping].Deserialize(FieldStr);
    FAutoCalc[Stopping] := (not UseRouteTime)
                       and FSchTimes[Stopping].AutoCalculated;

    ExtractField('Dep', S, FieldStr, efValue);
    FSchTimes[Departure].Deserialize(FieldStr);
    FAutoCalc[Departure] := FSchTimes[Departure].AutoCalculated;

    ExtractField('Trav', S, FieldStr, efValue);
    UseRouteTime := (FieldStr = 'route');
    if UseRouteTime then FSchTimes[Travel].Unused()
    else FSchTimes[Travel].Deserialize(FieldStr);
    FAutoCalc[Travel] := (not UseRouteTime)
                     and FSchTimes[Travel].AutoCalculated;

    UpdateIntersect();
end;

constructor TStopSchedule.CreateAndDeserialize(var S: String; RouteID,
    TrainID: TID; _StopIndex: SizeUInt);
begin
    Initialized := False;
    FGhost := -1;
    Route := RouteID;
    Train := TrainID;
    StopIndex := _StopIndex;

    Init();
    Deserialize(S);
    Initialized := True;
end;

constructor TStopSchedule.Create(StationID, RouteID, TrainID: TID;
    _StopIndex: SizeUInt);
begin
    Initialized := False;
    FGhost := -1;
    Station := StationID;
    Route := RouteID;
    Train := TrainID;
    StopIndex := _StopIndex;

    Init();
    UseRouteNS := True;
    Initialized := True;
end;

constructor TStopSchedule.CreateGhost(Other: TStopSchedule);
var SchType: TSchType;
begin
    Initialized := False;

    Station := Other.Station;
    Route := Other.Route;
    Train := Other.Train;
    StopIndex := Other.StopIndex;

    Init();

    for SchType in SchType_RouteTimes do
    begin
        FSchTimes[SchType] := TSchTime.CreateCopy(Other.FSchTimes[SchType]);
        FAutoCalc[SchType] := True;
    end;
    UpdateIntersect();
    FNonStop := Other.FNonStop;
    FUseRouteNS := Other.FUseRouteNS;
    FGhost := Other.StopIndex;

    Initialized := True;
end;

destructor TStopSchedule.Destroy();
begin
    FreeAndNil(FSchTimes);
    FreeAndNil(Intersect);
    inherited Destroy();
end;

procedure TStopSchedule.Init();
begin
    Intersect := TSchIntersect.Create();
    FSchTimes := TSchTimes.Create();
    FNeighborsInvalid := False;
    FNeighborsRecalculated := False;
end;

end.

