unit UIDPrefixTree;

{$mode ObjFPC}{$H+}
{$modeswitch advancedrecords}

interface

uses
    SysUtils, gtree, UEntity, UEntityList, UID;

type

    { TID_PD_Data }
    TID_PD_Data = record
        Key, Value: TID;
        class function Make(KeyID, ValueID: TID): TID_PD_Data; static;
    end;

    { TIDPrefixTree }
    generic TIDPrefixTree<T: TEntity> = class(TObject)
    public
        type TIDPTree = specialize TTree<TID_PD_Data>;
        type TIDPTreeNode = specialize TTreeNode<TID_PD_Data>;
        type TIDPTree_EL = specialize TEntityList<T>;
    private
        Tree: TIDPTree;
        procedure InitTree();
        procedure ResetTree();
    public
        function CreateNode(Parent: TIDPTreeNode; const Data: TID_PD_Data):
            TIDPTreeNode;
        function Root(): TIDPTreeNode;

        function FindKey(Key: TID; const Start: TIDPTreeNode;
            out Node: TIDPTreeNode): Boolean;
        function FindChildKey(Key: TID; const Parent: TIDPTreeNode;
            out Node: TIDPTreeNode): Boolean;
        // True = found, False = created
        function FindChildKeyOrCreate(Key: TID; Parent: TIDPTreeNode;
            out Node: TIDPTreeNode): Boolean;

        procedure Build(const EntityList: TIDPTree_EL);
    public
        constructor Create();
        destructor Destroy(); override;
    end;

const NoValue = 0;

implementation

{ TID_PD_Data }

class function TID_PD_Data.Make(KeyID, ValueID: TID): TID_PD_Data;
begin
    Result.Key := KeyID;
    Result.Value := ValueID;
end;

{ TIDPrefixTree }

procedure TIDPrefixTree.InitTree();
begin
    Tree := TIDPTree.Create();
    Tree.Root := TIDPTreeNode.Create(TID_PD_Data.Make(NoValue, NoValue));
end;

procedure TIDPrefixTree.ResetTree();
begin
    FreeAndNil(Tree);
    InitTree();
end;

function TIDPrefixTree.CreateNode(Parent: TIDPTreeNode;
    const Data: TID_PD_Data): TIDPTreeNode;
begin
    Result := TIDPTreeNode.Create(Data);
    Parent.Children.PushBack(Result);
end;

function TIDPrefixTree.Root(): TIDPTreeNode;
begin
    Result := Tree.Root;
end;

function TIDPrefixTree.FindKey(Key: TID; const Start: TIDPTreeNode;
    out Node: TIDPTreeNode): Boolean;
var Child: TIDPTreeNode;
begin
    if Start.Data.Key = Key then begin
        Node := Start;
        Result := True;
    end
    else if Start.Children.IsEmpty() then begin
        Node := nil;
        Result := False;
    end
    else begin
        for Child in Start.Children do begin
            Result := FindKey(Key, Child, Node);
            if Result then break;
        end;
    end;
end;

function TIDPrefixTree.FindChildKey(Key: TID; const Parent: TIDPTreeNode;
    out Node: TIDPTreeNode): Boolean;
var Child: TIDPTreeNode;
begin
    Node := nil;
    Result := False;
    for Child in Parent.Children do begin
        Result := Child.Data.Key = Key;
        if Result then begin
            Node := Child;
            break;
        end;
    end;
end;

function TIDPrefixTree.FindChildKeyOrCreate(Key: TID; Parent: TIDPTreeNode;
    out Node: TIDPTreeNode): Boolean;
begin
    Result := FindChildKey(Key, Parent, Node);
    if not Result then
        Node := CreateNode(Parent, TID_PD_Data.Make(Key, NoValue));
end;

procedure TIDPrefixTree.Build(const EntityList: TIDPTree_EL);
var
    Keys: TIDList;
    Key: TID;
    Entity: TEntity;
    Node: TIDPTreeNode;
begin
    ResetTree();
    for Entity in EntityList do
    begin
        Node := Tree.Root;
        Keys := Entity.GetTreeKeys();
        if Keys.Count = 0 then continue;
        for Key in Keys do
            FindChildKeyOrCreate(Key, Node, Node);
        Node.Data := TID_PD_Data.Make(Key, Entity.ID);
    end;
end;

constructor TIDPrefixTree.Create();
begin
    inherited Create();
    InitTree();
end;

destructor TIDPrefixTree.Destroy();
begin
    FreeAndNil(Tree);
    inherited Destroy();
end;

end.

