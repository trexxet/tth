unit USchIntersect;

{$mode ObjFPC}{$H+}

interface

uses
    Classes, SysUtils, USTime;

type

    { TSchIntersect }
    TSchIntersect = class(TBits)
    public
        procedure SetFromArrDep(const Arr, Dep: TSchTime);

        function DoesIntersect(const Other: TSchIntersect): Boolean;
        function AddIfNoIntersect(const Other: TSchIntersect): Boolean;

        constructor Create(); reintroduce;
    end;

implementation

{ TSchIntersect }

procedure TSchIntersect.SetFromArrDep(const Arr, Dep: TSchTime);
var
    tArr, tDep: TTimeInt;
    i: TTimeInt;
begin
    ClearAll();
    tArr := Arr.T;
    tDep := Dep.T;

    if tDep >= tArr then begin
        for i := tArr to tDep do
            Bits[i] := True;
    end else begin
        for i := tArr to TTimeInt.Wrap - 1 do
            Bits[i] := True;
        for i := 0 to tDep do
            Bits[i] := True;
    end;
end;

function TSchIntersect.DoesIntersect(const Other: TSchIntersect): Boolean;
var tmp: TSchIntersect;
begin
    tmp := TSchIntersect.Create();
    tmp.CopyBits(Self);
    tmp.AndBits(Other);
    Result := tmp.FindFirstBit(True) >= 0;
    FreeAndNil(tmp);
end;

function TSchIntersect.AddIfNoIntersect(const Other: TSchIntersect): Boolean;
begin
    Result := not DoesIntersect(Other);
    if Result then
        OrBits(Other);
end;

constructor TSchIntersect.Create();
begin
    inherited Create(TTimeInt.Wrap);
end;

end.

