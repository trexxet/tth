unit URunningControl;

{$mode ObjFPC}{$H+}
{$modeswitch advancedrecords}

interface

uses
    SysUtils;

type

    { TRunningControl }
    TRunningControl = record
    private
        FRunning: Boolean;
        FLoading: Boolean;
        FFileChanged: Boolean;
        procedure DoneRerun(willRun: Boolean);
    public
        procedure SetRunning(const AValue: Boolean);
        procedure InitRerun();
        property Running: Boolean read FRunning write SetRunning;
    public
        function AcquireLoading(): Boolean;
        procedure ReleaseLoading();
        property Loading: Boolean read FLoading;
    public
        procedure SetFileChanged(const AValue: Boolean);
        property FileChanged: Boolean read FFileChanged write SetFileChanged;
    end;

implementation

uses Forms, Controls, Main, Editor, ULogger;

{ TRunningControl }

procedure TRunningControl.SetRunning(const AValue: Boolean);
begin
    Logger.Log(LINFO, 'Set running to ', BoolToStr(AValue, True));
    DoneRerun(AValue);
    FRunning := AValue;

    with MainForm do
    begin
        MMSave.Enabled := Running;
        MMSaveAs.Enabled := Running;
        TBSave.Enabled := Running;
        TBSaveAs.Enabled := Running;

        ActionFSave.Enabled := Running;
        ActionFSaveAs.Enabled := Running;
        Action24H.Enabled := Running;

        with MainEditorFrame do
        begin
            PGC.Visible := Running;
            LabelRunning.Visible := not Running;

            SwitchTabStations.Enabled := Running;
            SwitchTabRoutes.Enabled := Running;

            if Running then AssignData();
        end;
    end;
end;

procedure TRunningControl.InitRerun();
begin
    Screen.Cursor := crHourGlass;
    if Running then
        with MainForm do
        begin
            MainEditorFrame.Visible := False;
            MainEditorFrameScrollBox.Repaint();
        end;
end;

function TRunningControl.AcquireLoading(): Boolean;
begin
    Result := not FLoading;
    if Result then FLoading := True;
end;

procedure TRunningControl.ReleaseLoading;
begin
    FLoading := False;
end;

procedure TRunningControl.SetFileChanged(const AValue: Boolean);
begin
    if FFileChanged and AValue then exit;
    FFileChanged := AValue;
    MainForm.SetFilenameCaption(AValue);
end;

procedure TRunningControl.DoneRerun(willRun: Boolean);
begin
    Screen.Cursor := crDefault;
    if Running and willRun then
        with MainForm do
        begin
            FreeAndNil(MainEditorFrame);
            MainEditorFrame := TEditorFrame.Create(MainEditorFrameScrollBox);
            MainEditorFrame.Parent := MainEditorFrameScrollBox;
            AssignViewActions();
        end
    else
    begin
        // Restore hidden in InitRerun()
        MainForm.MainEditorFrame.Visible := True;
    end;
end;

end.

