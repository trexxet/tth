unit UID;

{$mode ObjFPC}{$H+}

interface

uses SysUtils, Generics.Collections;

type
    TID = SizeUInt;
    PTID = ^TID;
    TIDList = specialize TList<TID>;

    { TIDListSerializer }
    TIDListSerializer = class helper for TIDList
        function Serialize(): String;
        procedure Deserialize(var S: String);
    end;

implementation

uses UDeserializeUtils;

{ TIDListSerializer }

function TIDListSerializer.Serialize(): String;
var i: TID;
begin
    Result := '{ ';
    if Count > 0 then
    begin
        for i in Self do
            Result += i.ToString() + ', ';
        SetLength(Result, Length(Result) - 2);
    end;
    Result += ' }';
end;

procedure TIDListSerializer.Deserialize(var S: String);
begin
    ExtractIDList(S, Self);
end;

end.

