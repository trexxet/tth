unit UEntity;

{$mode ObjFPC}{$H+}

interface

uses
    Classes, SysUtils, Graphics,
    ULogger, UISerializable, UID,
    UEntity_RS;

type

    { TEntity }
    TEntity = class(TInterfacedObject, ISerializable)
        type TEntityType = (station, direction, route, train, fake, err);
    public
        ID: TID;
        EType: TEntityType;
        Name: String;

        function ETypeToStr(): String; overload;
        class function ETypeToStr(_EType: TEntityType): String; overload;
        class function ETypeFromStr(const S: String; out V: TEntityType): Boolean;
        class function ETypeToNewName(_EType: TEntityType): String;
        function IdNameStr(): String;
    public
        function GetTreeKeys(): TIDList; virtual;
    public
        function Serialize(): String;
        procedure Deserialize(var S: String); virtual;

        constructor CreateAndDeserialize(var S: String); virtual;
        constructor Create(_ID: Word; _EType: TEntityType; _Name: String);
        destructor Destroy(); override;
    end;

    { TEntityColored }
    TEntityColored = class(TEntity)
    protected
        FFGColor, FBGColor: TColor;
        procedure SetColor(const AValue: TColor);
    public
        property FGColor: TColor read FFGColor;
        property Color: TColor read FBGColor write SetColor;
        procedure GetColor(out BG, FG: TColor);

        function Serialize(): String;
        procedure Deserialize(var S: String); override;
        constructor Create(_ID: Word; _EType: TEntityType; _Name: String);
    end;

const ecl_Default = TColor($D0D0D0);

implementation

uses UDeserializeUtils, UColorUtils;

{ TEntity }

{%REGION Str }
function TEntity.ETypeToStr(): String;
var
    ETypeStr: String;
begin
    Str(EType, ETypeStr);
    Result := ETypeStr;
end;

class function TEntity.ETypeToStr(_EType: TEntityType): String;
var
    ETypeStr: String;
begin
    Str(_EType, ETypeStr);
    Result := ETypeStr;
end;

class function TEntity.ETypeFromStr(const S: String;
    out V: TEntityType): Boolean;
var Code: Word;
begin
    Val(S, V, Code);
    Result := Code = 0;
    if not Result then V := err;
end;

class function TEntity.ETypeToNewName(_EType: TEntityType): String;
const Names: array[TEntityType] of String = (
    UEntityRS_NewStation,
    UEntityRS_NewDirection,
    UEntityRS_NewRoute,
    UEntityRS_NewTrain,
    UEntityRS_NewFake,
    UEntityRS_NewError);
begin
    Result := Names[_EType];
end;

function TEntity.IdNameStr(): String;
begin
    Result := Format('%u ''%s''', [ID, Name]);
end;

function TEntity.GetTreeKeys(): TIDList;
begin
    Result := nil;
end;

function TEntity.Serialize(): String;
begin
    Result := Format('{ ID = %u; EType = %s; Name = "%s"; }',
                     [ID, ETypeToStr(), Name]);
end;

procedure TEntity.Deserialize(var S: String);
var FieldStr: String;
begin
    TrimBrackets(S);
    ExtractField('ID', S, FieldStr, efValue);
    ID := FieldStr.ToInt64;
    ExtractField('EType', S, FieldStr, efValue);
    if not ETypeFromStr(FieldStr, EType) then
        raise EFormatError.Create({$I %CURRENTROUTINE%});
    ExtractField('Name', S, FieldStr, efValue);
    Name := FieldStr.DeQuotedString('"');
    Logger.Log(LINFO, 'New %s %s', [ETypeToStr(), IDNameStr()]);
end;

constructor TEntity.CreateAndDeserialize(var S: String);
begin
    Deserialize(S);
end;

{%ENDREGION}

constructor TEntity.Create(_ID: Word; _EType: TEntityType; _Name: String);
begin
    ID := _ID;
    Name := _Name;
    EType := _EType;
    Logger.Log(LINFO, 'New %s %s', [ETypeToStr(), IDNameStr()]);
end;

destructor TEntity.Destroy();
begin
    Logger.Log(LINFO, 'Delete %s %s', [ETypeToStr(), IDNameStr()]);
    inherited Destroy();
end;

{ TEntityColored }

procedure TEntityColored.SetColor(const AValue: TColor);
begin
    FBGColor := AValue;
    FFGColor := GetContrastFGColor(AValue);
end;

procedure TEntityColored.GetColor(out BG, FG: TColor);
begin
    BG := Color;
    FG := FGColor;
end;

function TEntityColored.Serialize(): String;
var EndPos: SizeInt;
begin
    Result := inherited Serialize();
    EndPos := Result.LastIndexOf('}') + 1;
    if EndPos > 0 then
        Insert(Format('Color = %s; ', [ColorToString(FBGColor)]),
               Result, EndPos);
end;

procedure TEntityColored.Deserialize(var S: String);
var FieldStr: String;
begin
    inherited Deserialize(S);
    ExtractField('Color', S, FieldStr, efValue);
    Color := StringToColor(FieldStr);
end;

constructor TEntityColored.Create(_ID: Word; _EType: TEntityType;
    _Name: String);
begin
    inherited Create(_ID, _EType, _Name);
    Color := ecl_Default;
end;

end.

