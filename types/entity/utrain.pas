unit UTrain;

{$mode ObjFPC}{$H+}

interface

uses
    SysUtils,
    UEntity, UTrainSchedule, UStopSchedule, USchList, USTime, UID;

type

    TTotalTime     = UTrainSchedule.TTotalTime;
    TCanCalcArrDep = UTrainSchedule.TArrDep_CanCalcStatus;
    TCanToggleAC   = UTrainSchedule.TCanToggleAC;

    { TTrain }
    TTrain = class(TEntity)
    private
        Schedule: TTrainSchedule;   // ordered
        GhostSchedule: TTrainSchedule;   // ordered
    public
        RouteID: TID;
        procedure InitSchedule();
        procedure InsertSchedule(StopIndex: SizeUInt);
        procedure DeleteSchedule(StopIndex: SizeUInt);
        procedure MoveSchedule(OldIndex, NewIndex: SizeUInt);
        function ScheduleAt(StopIndex: SizeUInt): TStopSchedule;
        function SchedulePrev(StopSch: TStopSchedule; Ghost: Boolean = False):
            TStopSchedule;
        function ScheduleNext(StopSch: TStopSchedule; Ghost: Boolean = False):
            TStopSchedule;
        function SchedulesFor(StopIndex: SizeUInt): TSchList;
    public // Used for auto calculating the schedule
        AutoRecalc: Boolean;
        function ValidateTotalTime(var TT: TTotalTime): Boolean;
        procedure GetTotalTime(var TT: TTotalTime); inline;
        function ScheduleStopTrav_CanAutoCalc(): Boolean; inline;
        procedure ScheduleStopTrav_DoAutoCalc(); inline;
        function ScheduleArrDep_CanAutoCalc(constref TotalTime: TTotalTime):
            TArrDep_CanCalcStatus; inline;
        procedure ScheduleArrDep_DoAutoCalc(); inline;
        function ScheduleCanToggleAC(const StopSchedule: TStopSchedule;
            SchType: TSchType): TCanToggleAC; inline;
        procedure RecalculateAll();
    private // Ghost schedule for loops
        FNeedGhostReset: Boolean;
        FGhostLoops: SizeUInt;
        function GetNeedGhostReset(): Boolean; inline;
    public
        procedure MakeGhosts(Loops: SizeUInt = 0);
        procedure ClearGhosts(); inline;
        procedure RecalcGhosts(const LastStop: TStopSchedule = nil);
        function HasGhosts(): Boolean; inline;
        function GhostScheduleAt(StopIndex: SizeUInt): TStopSchedule;
        function CountWithGhosts(): SizeUInt;
        function CountWithoutGhosts(): SizeUInt;
        property NeedGhostReset: Boolean read GetNeedGhostReset;
    public
        function Serialize(): String;
        constructor CreateAndDeserialize(var S: String); override;
        constructor Create(_ID: TID; _EType: TEntityType; _Name: String);
        destructor Destroy(); override;
    private
        procedure Init();
    end;

implementation

uses
    Math, UTimetable,
    UDeserializeUtils, ULogger;

{ TTrain }

procedure TTrain.InitSchedule();
var
    Stops: TIDList;
    StopIndex: SizeUInt;
begin
    Stops := Timetable.Routes[RouteID].Stops;
    if Stops.Count = 0 then exit;
    Schedule.Clear();
    for StopIndex := 0 to Stops.Count - 1 do
        Schedule.Add(TStopSchedule.Create(
                         Stops[StopIndex], RouteID, ID, StopIndex));
end;

procedure TTrain.InsertSchedule(StopIndex: SizeUInt);
var
    Stops: TIDList;
    StationID: TID;
    StopIndexSave: SizeUInt;
begin
    Stops := Timetable.Routes[RouteID].Stops;
    if StopIndex >= Stops.Count then
    begin
        Logger.Log(LERROR,
                   'ERROR: Train %s: adding schedule with index %u: not exist',
                   [IDNameStr(), StopIndex]);
        exit;
    end;
    StationID := Stops[StopIndex];

    ClearGhosts();
    Schedule.Insert(StopIndex,
                    TStopSchedule.Create(StationID, RouteID, ID, StopIndex));
    Logger.Log(LINFO, 'Train %s: Added schedule:' + LineEnding + #9'%s',
               [IDNameStr(), Schedule[StopIndex].ToStr()]);

    // Update StopIndex's
    StopIndexSave := StopIndex;
    while StopIndex < Schedule.Count - 1 do
    begin
        StopIndex += 1;
        Schedule[StopIndex].StopIndex := StopIndex;
    end;

    Schedule[StopIndexSave].InvalidateDependencies(Total);
end;

procedure TTrain.DeleteSchedule(StopIndex: SizeUInt);
begin
    if StopIndex >= Schedule.Count then
        Logger.Log(LERROR,
                   'ERROR: Train %s: removing schedule with index %u: not exist',
                   [IDNameStr(), StopIndex])
    else
    begin
        ClearGhosts();
        Logger.Log(LINFO, 'Train %s: Removing schedule:' + LineEnding + #9'%s',
                   [IDNameStr(), Schedule[StopIndex].ToStr()]);
        Schedule[StopIndex].InvalidateDependencies(Total);
        Schedule.Delete(StopIndex);
    end;
    // Update StopIndex's
    while StopIndex < Schedule.Count do
    begin
        Schedule[StopIndex].StopIndex := StopIndex;
        StopIndex += 1;
    end;
end;

procedure TTrain.MoveSchedule(OldIndex, NewIndex: SizeUInt);
var StartIndex, EndIndex, StopIndex: SizeUInt;
begin
    ClearGhosts();

    Schedule.Move(OldIndex, NewIndex);

    // Update StopIndex's
    StartIndex := Min(OldIndex, NewIndex);
    EndIndex := Max(OldIndex, NewIndex);
    for StopIndex := StartIndex to EndIndex do
        Schedule[StopIndex].StopIndex := StopIndex;
    // Invalidate
    for StopIndex := StartIndex to EndIndex do
    begin
        Schedule[StopIndex].TryInvalidate(Arrival);
        Schedule[StopIndex].TryInvalidate(Travel);
    end;
end;

function TTrain.ScheduleAt(StopIndex: SizeUInt): TStopSchedule;
begin
    if StopIndex >= Schedule.Count then
        Result := GhostScheduleAt(StopIndex - Schedule.Count)
    else
        Result := Schedule.Items[StopIndex];
end;

function TTrain.SchedulePrev(StopSch: TStopSchedule; Ghost: Boolean):
    TStopSchedule;
begin
    if not Ghost then Result := Schedule.Prev(StopSch)
    else Result := GhostSchedule.Prev(StopSch);
end;

function TTrain.ScheduleNext(StopSch: TStopSchedule; Ghost: Boolean):
    TStopSchedule;
begin
    if not Ghost then Result := Schedule.Next(StopSch)
    else Result := GhostSchedule.Next(StopSch);
end;

function TTrain.SchedulesFor(StopIndex: SizeUInt): TSchList;
begin
    Result := TSchList.Create();
    if StopIndex >= Schedule.Count then exit;

    Result.Add(TSchListItem.Make(Schedule[StopIndex]));
    if HasGhosts() then
        while StopIndex < GhostSchedule.Count do begin
            Result.Add(TSchListItem.Make(GhostSchedule[StopIndex]));
            StopIndex += Schedule.Count;
        end;
end;

function TTrain.ValidateTotalTime(var TT: TTotalTime): Boolean;
begin
    GetTotalTime(TT);
    Result := TT.OffTime = HM0;
    if not Result then ClearGhosts();
end;

procedure TTrain.GetTotalTime(var TT: TTotalTime);
begin
    Schedule.TotalTime(TT);
end;

function TTrain.ScheduleStopTrav_CanAutoCalc(): Boolean;
begin
    Result := Schedule.StopTrav_CanAutoCalc()
end;

procedure TTrain.ScheduleStopTrav_DoAutoCalc();
begin
    Schedule.StopTrav_DoAutoCalc()
end;

function TTrain.ScheduleArrDep_CanAutoCalc(constref TotalTime: TTotalTime):
    TArrDep_CanCalcStatus;
begin
    Result := Schedule.ArrDep_CanAutoCalc(TotalTime);
end;

procedure TTrain.ScheduleArrDep_DoAutoCalc();
begin
    Schedule.ArrDep_DoAutoCalc();
end;

function TTrain.ScheduleCanToggleAC(const StopSchedule: TStopSchedule;
    SchType: TSchType): TCanToggleAC;
begin
    Result := Schedule.CanToggleAC(StopSchedule, SchType);
end;

procedure TTrain.RecalculateAll();
var StopSchedule: TStopSchedule;
begin
    Logger.PushSeverity(LERROR);

    for StopSchedule in Schedule do
        StopSchedule.RecalculateDependencies(Total);
    if HasGhosts() then
        RecalcGhosts(StopSchedule);

    Logger.PopSeverity();
end;

function TTrain.GetNeedGhostReset(): Boolean;
begin
    Result := FNeedGhostReset;
    FNeedGhostReset := False;
end;

procedure TTrain.MakeGhosts(Loops: SizeUInt);
var
    StopSchedule: TStopSchedule;
begin
    Logger.PushSeverity(LERROR);
    ClearGhosts();
    if Loops = 0 then Loops := FGhostLoops
    else FGhostLoops := Loops;

    while Loops > 1 do
    begin
        for StopSchedule in Schedule do
            GhostSchedule.Add(TStopSchedule.CreateGhost(StopSchedule));
        Loops -= 1;
    end;

    if HasGhosts() then
        RecalcGhosts(StopSchedule);
    Logger.PopSeverity();
end;

procedure TTrain.ClearGhosts();
begin
    if not HasGhosts() then exit;
    Logger.PushSeverity(LERROR);

    GhostSchedule.Clear();
    FGhostLoops := 0;
    FNeedGhostReset := True;

    Logger.PopSeverity();
end;

procedure TTrain.RecalcGhosts(const LastStop: TStopSchedule);
var
    StopSchedule: TStopSchedule;
begin
    if LastStop = nil then StopSchedule := Schedule.Last
    else StopSchedule := LastStop;
    GhostScheduleAt(0)[Arrival] := StopSchedule[Departure] + StopSchedule[Travel];
    GhostSchedule.ArrDep_DoAutoCalc();
end;

function TTrain.HasGhosts(): Boolean;
begin
    Result := GhostSchedule.Count > 0;
end;

function TTrain.GhostScheduleAt(StopIndex: SizeUInt): TStopSchedule;
begin
    if StopIndex >= GhostSchedule.Count then
    begin
        Result := nil;
        Logger.Log(LERROR,
                   'ERROR: Train %s: getting schedule with index %u: not exist',
                   [IDNameStr(), StopIndex]);
    end else
        Result := GhostSchedule.Items[StopIndex];
end;

function TTrain.CountWithGhosts(): SizeUInt;
begin
    Result := Schedule.Count + GhostSchedule.Count;
end;

function TTrain.CountWithoutGhosts(): SizeUInt;
begin
    Result := Schedule.Count;
end;

function TTrain.Serialize(): String;
var EndPos: SizeInt;
begin
    Result := inherited Serialize();
    EndPos := Result.LastIndexOf('}') + 1;
    if EndPos > 0 then
        Insert(Format('Route = %u; AutoRecalc = %s; Schedule = %s; GhostLoops = %u; ',
                      [RouteID, BoolToStr(AutoRecalc, True), Schedule.Serialize(),
                       FGhostLoops]),
               Result, EndPos);
end;

constructor TTrain.CreateAndDeserialize(var S: String);
var
    FieldStr: String;
    StopSchedule: TStopSchedule;
    StopIndex: SizeUInt = 0;
begin
    inherited CreateAndDeserialize(S);
    Init();

    ExtractField('Route', S, FieldStr, efValue);
    RouteID := FieldStr.ToInt64;

    ExtractField('AutoRecalc', S, FieldStr, efValue);
    AutoRecalc := StrToBool(FieldStr);

    ExtractField('Schedule', S, FieldStr, efBlock);
    Schedule.Deserialize(FieldStr, RouteID, ID);
    for StopSchedule in Schedule do
    begin
        StopSchedule.Train := ID;
        StopSchedule.Route := RouteID;
        StopSchedule.StopIndex := StopIndex;
        StopIndex += 1;
    end;

    ExtractField('GhostLoops', S, FieldStr, efValue);
    FGhostLoops := FieldStr.ToInt64;
end;

constructor TTrain.Create(_ID: TID; _EType: TEntityType; _Name: String);
begin
    inherited Create(_ID, _EType, _Name);
    Init();
    FGhostLoops := 0;
    AutoRecalc := False;
end;

destructor TTrain.Destroy();
begin
    FreeAndNil(GhostSchedule);
    FreeAndNil(Schedule);
    inherited Destroy();
end;

procedure TTrain.Init();
begin
    Schedule := TTrainSchedule.Create();
    GhostSchedule := TTrainSchedule.Create();
    FNeedGhostReset := False;
end;

end.

