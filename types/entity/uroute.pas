unit URoute;

{$mode ObjFPC}{$H+}

interface

uses
    Classes, SysUtils,
    UEntity, UEntityList, UTrain, UStation, UDirection,
    URouteStopTime, UStopSchedule, USchList, UDirectionRouteItem, USTime, UID;

type

    { TRoute }
    TRoute = class(TEntityColored)
    private
        DirectionCalcIDL: TIDList;
        function DoAddStop(Station: TStation; StationID: TID;
                           Order: SizeInt): SizeUInt;
        procedure DoRemoveStop(Station: TStation; Order: SizeUInt);
        procedure DoInvalidateSchedules(StopIndex: SizeUInt; SchType: TSchType);
    public
        Stops: TIDList;  // ordered, non-unique
        StopTimes: TRouteStopTimes;  // ordered
        Trains: specialize TEntityList<TTrain>; // ordered, unique
    public
        function AddStop(StationID: TID; Order: SizeInt = -1): SizeInt;
        function RemoveStop(StationID: TID; Order: SizeInt = -1): Boolean;
        function PrevStopIndex(StopIndex: SizeUInt): SizeInt;
        function NextStopIndex(StopIndex: SizeUInt): SizeInt;
        procedure AddReverseTrip();
    public
        function GetStopTime(StopIndex: SizeUInt; SchType: TSchType): TSchTime;
        procedure SetStopTime(StopIndex: SizeUInt; SchType: TSchType;
            const _Time: THMTime);
        function GetNS(StopIndex: SizeUInt): Boolean;
        procedure SetNS(StopIndex: SizeUInt; NS: Boolean);
        procedure MoveStopTime(OldIndex, NewIndex: SizeUInt);
    public
        procedure NSForStop(StopIndex: SizeUInt; out HasStop, HasNonStop: Boolean);
        function DirectionForStop(StopIndex: SizeUInt): TDirection;
        procedure DirectionsForStations(StationID: TID;
            DirectionRouteList: TDirectionRouteList);
        function SchedulesForStop(StopIndex: SizeUInt): TSchList;
        procedure ValidateTotalTimes();
    public
        function AddTrain(_Name: String = ''; Order: SizeInt = -1): TID;
        function RemoveTrain(TrainID: TID): Boolean;
    public
        procedure Purge();
        function Serialize(): String;

        constructor CreateAndDeserialize(var S: String); override;
        constructor Create(_ID: TID; _EType: TEntityType; _Name: String);
        destructor Destroy(); override;
    end;

implementation

uses UTimetable, UDeserializeUtils, ULogger;

{ TRoute }

function TRoute.DoAddStop(Station: TStation; StationID: TID;
    Order: SizeInt): SizeUInt;
var Train: TTrain;
begin
    if Order = - 1 then
        Order := Stops.Count;
    Stops.Insert(Order, StationID);
    StopTimes.Insert(Order, TRouteStopTime.Create0());
    for Train in Trains do
        Train.InsertSchedule(Order);
    Station.AddRoute(Self.ID);
    Result := Order;
end;

procedure TRoute.DoRemoveStop(Station: TStation; Order: SizeUInt);
var Train: TTrain;
begin
    Station.RemoveRoute(Self.ID);
    Stops.Delete(Order);
    StopTimes.Delete(Order);
    for Train in Trains do
        Train.DeleteSchedule(Order);
end;

procedure TRoute.DoInvalidateSchedules(StopIndex: SizeUInt; SchType: TSchType);
var ItemTrain: TTrain;
begin
    for ItemTrain in Trains do
        with ItemTrain.ScheduleAt(StopIndex) do
            if IsRouteTime[SchType] then
            begin
                ItemTrain.ClearGhosts();
                InvalidateDependencies(SchType);
                if ItemTrain.AutoRecalc then
                    RecalculateDependencies(SchType);
            end;
end;

function TRoute.AddStop(StationID: TID; Order: SizeInt): SizeInt;
var Station: TStation;
begin
    Result := -1;
    if not Timetable.Stations.Exists(StationID) then
    begin
        Logger.Log(LERROR, 'ERROR: Route %s: adding station %u: station does not exist',
                   [IDNameStr(), StationID]);
        exit;
    end;

    Station := Timetable.Stations[StationID];
    Result := DoAddStop(Station, StationID, Order);

    Logger.Log(LINFO, 'Route %s: Added station %s',
               [IDNameStr(), Station.IDNameStr()]);
end;

function TRoute.RemoveStop(StationID: TID; Order: SizeInt): Boolean;
var
    IDToDelete: TID;
    Station: TStation;
begin
    Result := False;
    if Order > -1 then
    begin
        IDToDelete := Stops[Order];
        if IDToDelete <> StationID then
        begin
            Logger.Log(LERROR,
                'ERROR: Route %s: removing station %u: station at position %d has ID %u',
                [IDNameStr(), StationID, Order, IDToDelete]);
            exit;
        end;
    end else
    begin
        IDToDelete := StationID;
        Order := Stops.IndexOf(IDToDelete);
    end;

    if not Timetable.Stations.Exists(IDToDelete) then
    begin
        Logger.Log(LERROR,
            'ERROR: Route %s: removing station %u: station does not exist',
            [IDNameStr(), IDToDelete]);
        exit;
    end;

    Station := Timetable.Stations[IDToDelete];
    DoRemoveStop(Station, Order);

    Logger.Log(LINFO, 'Route %s: Removed station %s',
               [IDNameStr(), Station.IDNameStr()]);
    Result := True;
end;

function TRoute.PrevStopIndex(StopIndex: SizeUInt): SizeInt;
begin
    if StopIndex >= Stops.Count then exit(-1);
    if StopIndex = 0 then
        Result := Stops.Count - 1
    else
        Result := StopIndex - 1;
end;

function TRoute.NextStopIndex(StopIndex: SizeUInt): SizeInt;
begin
    if StopIndex >= Stops.Count then exit(-1);
    if StopIndex = Stops.Count - 1 then
        Result := 0
    else
        Result := StopIndex + 1;
end;

procedure TRoute.AddReverseTrip();
var
    i: SizeUInt;
    NewStopIndex: SizeInt;
begin
    if Stops.Count > 2 then
        for i := Stops.Count - 2 downto 1 do
        begin
            NewStopIndex := AddStop(Stops[i]);
            if NewStopIndex < 0 then exit;
            StopTimes[NewStopIndex].CopyTimesFrom(StopTimes[i]);
        end;
end;

function TRoute.GetStopTime(StopIndex: SizeUInt; SchType: TSchType): TSchTime;
var StationID: TID;
begin
    Result := nil;
    if StopIndex >= Stops.Count then
    begin
        Logger.Log(LERROR,
                   'ERROR: Route %s: getting station stop time: wrong station position %u',
                   [IDNameStr(), StopIndex]);
        exit;
    end;

    StationID := Stops[StopIndex];
    if not Timetable.Stations.Exists(StationID) then
    begin
        Logger.Log(LERROR,
                   'ERROR: Route %s: getting station stop time station %u: station does not exist',
                   [IDNameStr(), StationID]);
        exit;
    end;

    if SchType in SchType_TrainOnly then
    begin
        Logger.Log(LERROR,
                   'ERROR: Route %s: getting station stop time %u: wrong schedule type',
                   [IDNameStr(), StationID]);
        exit;
    end;

    Result := StopTimes[StopIndex][SchType];
end;

procedure TRoute.SetStopTime(StopIndex: SizeUInt; SchType: TSchType;
    const _Time: THMTime);
var
    StationID: TID;
    OldT: TTimeInt;
begin
    if StopIndex >= Stops.Count then
    begin
        Logger.Log(LERROR,
                   'ERROR: Route %s: setting station stop time: wrong station position %u',
                   [IDNameStr(), StopIndex]);
        exit;
    end;

    StationID := Stops[StopIndex];
    if not Timetable.Stations.Exists(StationID) then
    begin
        Logger.Log(LERROR,
                   'ERROR: Route %s: setting station stop time %u: station does not exist',
                   [IDNameStr(), StationID]);
        exit;
    end;

    if SchType in SchType_TrainOnly then
    begin
        Logger.Log(LERROR,
                   'ERROR: Route %s: setting station stop time %u: wrong schedule type',
                   [IDNameStr(), StationID]);
        exit;
    end;

    OldT := StopTimes[StopIndex][SchType].T;
    StopTimes[StopIndex][SchType] := TSchTime.FromHMT(_Time);

    Logger.Log(LINFO, 'Set default route time [%s] for %s of stop %u ''%s'' of route %s',
               [StopTimes[StopIndex][SchType].ToStr(), TSchTypeToStr(SchType),
                StopIndex, Timetable.Stations[Stops[StopIndex]].Name,
                IDNameStr()]);

    // Do not invalidate if nothing has changed
    if OldT <> StopTimes[StopIndex][SchType].T then
        DoInvalidateSchedules(StopIndex, SchType);
end;

function TRoute.GetNS(StopIndex: SizeUInt): Boolean;
begin
    Result := False;
    if StopIndex >= Stops.Count then exit;
    Result := StopTimes[StopIndex].NonStop;
end;

procedure TRoute.SetNS(StopIndex: SizeUInt; NS: Boolean);
begin
    if StopIndex >= Stops.Count then
    begin
        Logger.Log(LERROR,
                   'ERROR: Route %s: setting station NS: wrong station position %u',
                   [IDNameStr(), StopIndex]);
        exit;
    end;

    Logger.Log(LINFO, 'Set default route NS = %s of stop %u ''%s'' of route %s',
               [BoolToStr(NS, True), StopIndex,
                Timetable.Stations[Stops[StopIndex]].Name, IDNameStr()]);

    StopTimes[StopIndex].NonStop := NS;
end;

procedure TRoute.MoveStopTime(OldIndex, NewIndex: SizeUInt);
var Train: TTrain;
begin
    if (OldIndex >= Stops.Count) or (NewIndex >= Stops.Count) then
    begin
        Logger.Log(LERROR,
                   'ERROR: Route %s: moving station stop time: wrong station positions %u and %u',
                   [IDNameStr(), OldIndex, NewIndex]);
        exit;
    end;

    StopTimes.Move(OldIndex, NewIndex);
    for Train in Trains do
        Train.MoveSchedule(OldIndex, NewIndex);
end;

procedure TRoute.NSForStop(StopIndex: SizeUInt; out HasStop, HasNonStop: Boolean);
var
    Train: TTrain;
    NS: Boolean;
begin
    HasStop := False;
    HasNonStop := False;
    for Train in Trains do begin
        NS := Train.ScheduleAt(StopIndex).NonStop;
        HasStop := HasStop or (not NS);
        HasNonStop := HasNonStop or NS;
    end;
end;

function TRoute.DirectionForStop(StopIndex: SizeUInt): TDirection;
var
    StopStation: TStation;
    StopDirTree: TStation.TDirTree;
    StopIter: SizeUInt;

    Node: TStation.TDirTree.TIDPTreeNode;
    PotentialDirection: TDirection;
    PotentialDirectionID: TID;
begin
    Result := NoDirection;
    if StopIndex >= Stops.Count then exit;
    StopStation := Timetable.Stations[Stops[StopIndex]];
    if StopStation = nil then exit;
    StopDirTree := StopStation.DirectionsTree;
    if StopDirTree = nil then exit;

    StopIter := StopIndex;
    Node := StopDirTree.Root();
    PotentialDirection := NoDirection;

    while (True) do begin
        StopIter := NextStopIndex(StopIter);
        if StopDirTree.FindChildKey(Stops[StopIter], Node, Node) then begin
            PotentialDirectionID := Node.Data.Value;
            if PotentialDirectionID > 0 then
                PotentialDirection := StopStation.Directions[PotentialDirectionID];
        end
        else break;
    end;

    Result := PotentialDirection;
end;

procedure TRoute.DirectionsForStations(StationID: TID;
    DirectionRouteList: TDirectionRouteList);
var
    StopIndex: SizeUInt;
    StopStationID: TID;
    DirectionRouteItem: TDirectionRouteItem;
    HasStop, HasNonStop: Boolean;
begin
    if DirectionRouteList = nil then exit;
    DirectionRouteList.Clear();

    for StopIndex := 0 to Stops.Count - 1 do
    begin
        StopStationID := Stops[StopIndex];
        if Timetable.Stations.Exists(StopStationID) and (StopStationID = StationID) then
        begin
            NSForStop(StopIndex, HasStop, HasNonStop);
            DirectionRouteItem :=
                TDirectionRouteItem.Make(DirectionForStop(StopIndex),
                                         StopIndex, HasStop, HasNonStop);
            DirectionRouteList.Add(DirectionRouteItem);
        end;
    end;
end;

function TRoute.SchedulesForStop(StopIndex: SizeUInt): TSchList;
var
    Train: TTrain;
    TrainSchList: TSchList;
begin
    Result := TSchList.Create();
    if StopIndex >= Stops.Count then exit;

    for Train in Trains do begin
        TrainSchList := Train.SchedulesFor(StopIndex);
        Result.AddRange(TrainSchList);
        FreeAndNil(TrainSchList);
    end;
end;

procedure TRoute.ValidateTotalTimes();
var
    Train: TTrain;
    TT: TTotalTime;
begin
    TT.SchTime := TSchTime.FromHMT(HM0);
    for Train in Trains do
        Train.ValidateTotalTime(TT);
    FreeAndNil(TT.SchTime);
end;

function TRoute.AddTrain(_Name: String; Order: SizeInt): TID;
var NewTrain: TTrain;
begin
    if Order = - 1 then
        Result := Trains.Append(_Name)
    else
        Result := Trains.Insert(Order, _Name);
    if Result = 0 then exit;

    NewTrain := Trains.At(Result);
    NewTrain.RouteID := ID;
    Logger.Log(LINFO, 'Route %s: Added train %s',
               [IDNameStr(), NewTrain.IDNameStr()]);
end;

function TRoute.RemoveTrain(TrainID: TID): Boolean;
begin
    Result := Trains.Remove(TrainID);
    Logger.Log(LINFO, 'Route %s: Removed train %u', [IDNameStr(), TrainID]);
end;

procedure TRoute.Purge();
var
    StopStationID: TID;
begin
    for StopStationID in Stops do
    begin
        if Timetable.Stations.Exists(StopStationID) then
            Timetable.Stations[StopStationID].RemoveRoute(Self.ID);
    end;
end;

function TRoute.Serialize(): String;
var EndPos: SizeInt;
begin
    Result := inherited Serialize();
    EndPos := Result.LastIndexOf('}') + 1;
    if EndPos > 0 then
        Insert(Format('Stops = %s; StopTimes = %s; Trains = %s; ',
                      [Stops.Serialize(), StopTimes.Serialize(),
                       Trains.Serialize()]),
               Result, EndPos);
end;

constructor TRoute.CreateAndDeserialize(var S: String);
var
    FieldStr: String;
    ItemTrain: TTrain;
    StationID: TID;
begin
    inherited CreateAndDeserialize(S);
    Stops := TIDList.Create();
    StopTimes := TRouteStopTimes.Create();
    Trains := specialize TEntityList<TTrain>.Create(train);
    DirectionCalcIDL := TIDList.Create();

    ExtractField('Stops', S, FieldStr, efBlock);
    Stops.Deserialize(FieldStr);
    for StationID in Stops do
        Logger.Log(LINFO, 'Route %s: Added station %s',
                   [IDNameStr(), Timetable.Stations[StationID].IDNameStr()]);

    ExtractField('StopTimes', S, FieldStr, efBlock);
    StopTimes.Deserialize(FieldStr);

    ExtractField('Trains', S, FieldStr, efBlock);
    Trains.Deserialize(FieldStr);
    for ItemTrain in Trains do
        Logger.Log(LINFO, 'Route %s: Added train %s',
                   [IDNameStr(), ItemTrain.IDNameStr()]);
end;

constructor TRoute.Create(_ID: TID; _EType: TEntityType; _Name: String);
begin
    inherited Create(_ID, _EType, _Name);
    Stops := TIDList.Create();
    StopTimes := TRouteStopTimes.Create();
    Trains := specialize TEntityList<TTrain>.Create(train);
    DirectionCalcIDL := TIDList.Create();
end;

destructor TRoute.Destroy();
begin
    FreeAndNil(DirectionCalcIDL);
    FreeAndNil(Trains);
    FreeAndNil(StopTimes);
    FreeAndNil(Stops);
    inherited Destroy();
end;

end.

