unit UStation;

{$mode ObjFPC}{$H+}

interface

uses
    SysUtils, EntityListView,
    UEntity, UEntityList, UDirection, UDirectionRouteItem, UIDPrefixTree, UID;

type

    { TStation }
    TStation = class(TEntity)
    private
        RoutesAll: TIDList; // unordered, non-unique, used for purging and GetViewList
        Routes: TIDList; // ordered, unique
        UpdatingDirections: Boolean;
        UpdatingRoutes: Boolean;
    public
        type TDirTree = specialize TIDPrefixTree<TDirection>;
    public
        Directions: specialize TEntityList<TDirection>;
        NoDirectionLocal: TDirection;
        DirectionsTree: TDirTree;
        procedure BeginDirectionsUpdate();
        procedure RebuildDirectionsTree();
        procedure OnDirectionStationsChange(Sender: TObject);
        procedure EndDirectionsUpdate();
        procedure BeginRoutesUpdate();
        procedure EndRoutesUpdate();
    public
        function AddDirection(_Name: String = ''; Order: SizeInt = -1): TID;
        function RemoveDirection(DirectionID: TID): Boolean;
        procedure AddRoute(RouteID: TID);
        procedure RemoveRoute(RouteID: TID);
        procedure UpdateRoutes(); // fills ordered and unique list of routes
        procedure UpdateDirectionsRoutes();
        procedure UpdateDirectionsRoutesIfHasRoute(RouteID: TID);
        procedure GetRouteList(ViewList: TEntityViewList);
    private
        procedure GetViewListByDirection(ViewList: TEntityViewList;
            Direction: TDirection);
    public
        procedure GetViewList(ViewList: TEntityViewList; NeedDirections: Boolean);
    public
        procedure Purge();
        function Serialize(): String;

        constructor CreateAndDeserialize(var S: String); override;
        constructor Create(_ID: TID; _EType: TEntityType; _Name: String);
        destructor Destroy(); override;
    private
        procedure Init();
    end;

implementation

uses UTimetable, URoute, UDeserializeUtils, ULogger;

{ TStation }

procedure TStation.OnDirectionStationsChange(Sender: TObject);
begin
    RebuildDirectionsTree();
    UpdateDirectionsRoutes();
end;

procedure TStation.BeginDirectionsUpdate();
begin
    UpdatingDirections := True;
end;

procedure TStation.RebuildDirectionsTree;
begin
    if UpdatingDirections then exit;
    DirectionsTree.Build(Directions);
end;

procedure TStation.EndDirectionsUpdate();
begin
    UpdatingDirections := False;
    RebuildDirectionsTree();
end;

procedure TStation.BeginRoutesUpdate();
begin
    UpdatingRoutes := True;
end;

procedure TStation.EndRoutesUpdate();
begin
    UpdatingRoutes := False;
    UpdateRoutes();
end;

function TStation.AddDirection(_Name: String; Order: SizeInt): TID;
begin
    if Order = - 1 then
        Result := Directions.Append(_Name)
    else
        Result := Directions.Insert(Order, _Name);
    if Result > 0 then
        Directions[Result].OnStationsChange := @OnDirectionStationsChange;
end;

function TStation.RemoveDirection(DirectionID: TID): Boolean;
begin
    Result := Directions.Remove(DirectionID);
    DirectionsTree.Build(Directions);
end;

procedure TStation.AddRoute(RouteID: TID);
begin
    if not Timetable.Routes.Exists(RouteID) then
    begin
        Logger.Log(LERROR,
                   'ERROR: Station %s: adding route %u: route does not exist',
                   [IDNameStr(), RouteID]);
        exit;
    end;

    RoutesAll.Add(RouteID);
    UpdateRoutes();
end;

procedure TStation.RemoveRoute(RouteID: TID);
begin
    if RoutesAll.Remove(RouteID) = -1 then
        Logger.Log(LERROR,
                   'ERROR: Station %s: removing route %u: route not in station',
                   [IDNameStr(), RouteID])
    else
        UpdateRoutes();
end;

procedure TStation.UpdateRoutes();
var
    RouteID: TID;
    Index: Integer;
begin
    if UpdatingRoutes then exit;
    Routes.Clear();
    for Index := 0 to Timetable.Routes.Count - 1 do
    begin
        RouteID := Timetable.Routes.IDOf(Index);
        if RoutesAll.Contains(RouteID) then
            Routes.Add(RouteID);
    end;
    UpdateDirectionsRoutes();
end;

procedure TStation.UpdateDirectionsRoutes();
var
    RouteID: TID;
    Direction: TDirection;
    RouteDirectionsList: TDirectionRouteList;
    RouteDirectionItem, DirectionRouteItem: TDirectionRouteItem;
begin
    RouteDirectionsList := TDirectionRouteList.Create();
    for Direction in Directions do
        Direction.ClearRoutes();
    NoDirectionLocal.ClearRoutes();

    for RouteID in Routes do
    begin
        Timetable.Routes[RouteID].DirectionsForStations(Self.ID, RouteDirectionsList);
        for RouteDirectionItem in RouteDirectionsList do
        begin
            Direction := RouteDirectionItem.Entity as TDirection;
            DirectionRouteItem :=
                TDirectionRouteItem.Make(Timetable.Routes[RouteID],
                                         RouteDirectionItem.StopIndex,
                                         RouteDirectionItem.HasStop,
                                         RouteDirectionItem.HasNonStop);
            if Direction <> NoDirection then
                Direction.AddRoute(DirectionRouteItem)
            else
                NoDirectionLocal.AddRoute(DirectionRouteItem);
        end;
    end;

    FreeAndNil(RouteDirectionsList);
end;

procedure TStation.UpdateDirectionsRoutesIfHasRoute(RouteID: TID);
begin
    if Routes.Contains(RouteID) then UpdateDirectionsRoutes();
end;

procedure TStation.GetRouteList(ViewList: TEntityViewList);
var RouteID: TID;
begin
    ViewList.Clear();
    for RouteID in Routes do
        ViewList.Add(TEntityViewListItem.Make(Timetable.Routes[RouteID], True));
end;

procedure TStation.GetViewListByDirection(ViewList: TEntityViewList;
    Direction: TDirection);
var
    DirectionRouteItem: TDirectionRouteItem;
    Route: TRoute;
begin
    if (Direction = nil) or (Direction.Routes.Count = 0) then exit;

    ViewList.Add(TEntityViewListItem.Make(Direction, False));
    for DirectionRouteItem in Direction.Routes do
    begin
        Route := Timetable.Routes[DirectionRouteItem.Entity.ID];
        ViewList.Add(TEntityViewListItem.Make(Route, True));
    end;
end;

procedure TStation.GetViewList(ViewList: TEntityViewList;
    NeedDirections: Boolean);
var
    Index: SizeInt;
    DirectionID: TID;
begin
    if NeedDirections then
    begin
        ViewList.Clear();
        for Index := 0 to Directions.Count - 1 do begin
            DirectionID := Directions.IDOf(Index);
            GetViewListByDirection(ViewList, Directions[DirectionID]);
        end;
        GetViewListByDirection(ViewList, NoDirectionLocal);
    end else
        GetRouteList(ViewList);
end;

procedure TStation.Purge();
var
    OtherStation: TStation;
    Direction: TDirection;
begin
    // Purge from Directions
    for OtherStation in Timetable.Stations do
        if OtherStation.ID <> Self.ID then begin
            for Direction in OtherStation.Directions do
                Direction.PurgeStation(Self.ID);
            OtherStation.NoDirectionLocal.PurgeStation(Self.ID);
        end;
    // Purge from Routes
    while RoutesAll.Count > 0 do
        if not Timetable.Routes[RoutesAll[0]].RemoveStop(Self.ID) then
        begin
            Logger.Log(LERROR, 'ERROR: Station %s: purge failed', [IDNameStr()]);
            break;
        end;
end;

{$H+} // WTF? https://forum.lazarus.freepascal.org/index.php?topic=67914

function TStation.Serialize(): String;
var EndPos: SizeInt;
begin
    Result := inherited Serialize();
    EndPos := Result.LastIndexOf('}') + 1;
    if EndPos > 0 then
        Insert(Format('Directions = %s; Routes = %s; ',
                      [Directions.Serialize(), RoutesAll.Serialize()]),
               Result, EndPos);
end;

constructor TStation.CreateAndDeserialize(var S: String);
var FieldStr: String;
begin
    inherited CreateAndDeserialize(S);
    Init();

    ExtractField('Directions', S, FieldStr, efBlock);
    Directions.Deserialize(FieldStr);
    ExtractField('Routes', S, FieldStr, efBlock);
    RoutesAll.Deserialize(FieldStr);
end;

constructor TStation.Create(_ID: TID; _EType: TEntityType; _Name: String);
begin
    inherited Create(_ID, _EType, _Name);
    Init();
end;

destructor TStation.Destroy();
begin
    FreeAndNil(DirectionsTree);
    FreeAndNil(RoutesAll);
    FreeAndNil(Routes);
    FreeAndNil(NoDirectionLocal);
    FreeAndNil(Directions);
    inherited Destroy();
end;

procedure TStation.Init();
begin
    Directions := specialize TEntityList<TDirection>.Create(direction);
    NoDirectionLocal := TDirection.Create(NoDirection.ID, NoDirection.EType, NoDirection.Name);
    RoutesAll := TIDList.Create();
    Routes := TIDList.Create();
    DirectionsTree := specialize TIDPrefixTree<TDirection>.Create();
    UpdatingDirections := False;
    UpdatingRoutes := False;
end;

end.

