unit UDirection;

{$mode ObjFPC}{$H+}

interface

uses
    Classes, SysUtils, EntityListView,
    UEntity, UEntityRecord,
    UDirectionRouteItem, UDirectionSortRouteItem, USchList,
    UID, UEntity_RS;

type

    { TDirection }
    TDirection = class(TEntity)
    private
        Stations: TIDList;  // ordered, unique
    public
        Routes: TDirectionRouteList;
        RoutesDS, RoutesNS: TDRIndexList; // ordered, unique
        function SchedulesOf(constref DirectionRouteItem: TDirectionRouteItem):
            TSchList;
    public
        procedure AddStation(StationID: TID; Order: SizeInt = -1);
        procedure RemoveStation(StationID: TID; Order: SizeInt = -1);
        procedure PurgeStation(StationID: TID);
    public
        procedure ClearRoutes();
        procedure AddRoute(constref RouteItem: TDirectionRouteItem);
    public
        function GetTreeKeys(): TIDList; override;
        function GetStations(): TIDList; inline;
        procedure GetViewList(ViewList: TEntityViewList);
    public
        OnStationsChange: TNotifyEvent;
        function SchedulesSorted(EntityRecList: TEntityRecList; SchLists: TSchListList):
            SizeUInt; // returns number of lists
    private
        procedure DoSortSchedules(SortList: TDirectionSortRoutesList;
            const Indexes: TDRIndexList; IsNS: Boolean);
        procedure MoveSortedSchedules(SortList: TDirectionSortRoutesList;
            EntityRecList: TEntityRecList; SchLists: TSchListList);
    public
        function Serialize(): String;

        constructor CreateAndDeserialize(var S: String); override;
        constructor Create(_ID: TID; _EType: TEntityType; _Name: String);
        destructor Destroy(); override;
    private
        procedure Init();
    end;

var
    NoDirection: TDirection;
    FakeStop, FakeNonStop: TEntity;

implementation

uses
    Math,
    UTimetable, URoute, UStopSchedule,
    UDeserializeUtils, ULogger;

{ TDirection }

function TDirection.SchedulesOf(constref DirectionRouteItem: TDirectionRouteItem):
    TSchList;
begin
    with DirectionRouteItem do
        Result := (Entity as TRoute).SchedulesForStop(StopIndex);
end;

procedure TDirection.AddStation(StationID: TID; Order: SizeInt);
begin
    if not Timetable.Stations.Exists(StationID) then
    begin
        Logger.Log(LERROR,
                   'ERROR: Direction %s: adding station %u: station does not exist',
                   [IDNameStr(), StationID]);
        exit;
    end;

    if Order = - 1 then
        Stations.Add(StationID)
    else
        Stations.Insert(Order, StationID);

    Logger.Log(LINFO, 'Direction %s: Added station %s',
               [IDNameStr(), Timetable.Stations[StationID].IDNameStr()]);

    if (OnStationsChange <> nil) then OnStationsChange(Self);
end;

procedure TDirection.RemoveStation(StationID: TID; Order: SizeInt);
var IDToDelete: TID;
begin
    if Order > -1 then
    begin
        IDToDelete := Stations[Order];
        if IDToDelete <> StationID then
        begin
            Logger.Log(LERROR,
                'ERROR: Direction %s: removing station %u: station at position %d has ID %u',
                [IDNameStr(), StationID, Order, IDToDelete]);
            exit;
        end;
    end else
        IDToDelete := StationID;

    if not Timetable.Stations.Exists(IDToDelete) then
    begin
        Logger.Log(LERROR,
            'ERROR: Direction %s: removing station %u: station does not exist',
            [IDNameStr(), IDToDelete]);
        exit;
    end;

    if Order > -1 then
        Stations.Delete(Order)
    else
        Stations.Remove(StationID);

    Logger.Log(LINFO, 'Direction %s: Removed station %s',
               [IDNameStr(), Timetable.Stations[IDToDelete].IDNameStr()]);

    if (OnStationsChange <> nil) then OnStationsChange(Self);
end;

procedure TDirection.PurgeStation(StationID: TID);
begin
    while Stations.Remove(StationID) >= 0 do;
end;

procedure TDirection.ClearRoutes();
begin
    Routes.Clear();
    RoutesDS.Clear();
    RoutesNS.Clear();
end;

procedure TDirection.AddRoute(constref RouteItem: TDirectionRouteItem);
var Index: SizeUInt;
begin
    Index := Routes.Add(RouteItem);
    if RouteItem.HasStop then
        RoutesDS.Add(Index);
    if RouteItem.HasNonStop then
        RoutesNS.Add(Index);
end;

function TDirection.GetTreeKeys(): TIDList;
begin
    Result := Stations;
end;

function TDirection.GetStations(): TIDList;
begin
    Result := Stations;
end;

procedure TDirection.GetViewList(ViewList: TEntityViewList);
var Index: SizeUInt;
begin
    ViewList.Clear();
    // First, routes that do stop at station
    ViewList.Add(TEntityViewListItem.Make(FakeStop, False));
    for Index in RoutesDS do
        ViewList.Add(TEntityViewListItem.Make(Routes[Index].Entity, True));
    // Last, non stop routes
    ViewList.Add(TEntityViewListItem.Make(FakeNonStop, False));
    for Index in RoutesNS do
        ViewList.Add(TEntityViewListItem.Make(Routes[Index].Entity, True));
end;

function TDirection.SchedulesSorted(EntityRecList: TEntityRecList;
    SchLists: TSchListList): SizeUInt;
var SortListDS, SortListNS: TDirectionSortRoutesList;
begin
    if (EntityRecList = nil) or (SchLists = nil) then exit(0);
    SortListDS := TDirectionSortRoutesList.Create();
    SortListNS := TDirectionSortRoutesList.Create();

    DoSortSchedules(SortListDS, RoutesDS, False);
    DoSortSchedules(SortListNS, RoutesNS, True);
    Result := SortListDS.Count + SortListNS.Count;

    MoveSortedSchedules(SortListDS, EntityRecList, SchLists);
    MoveSortedSchedules(SortListNS, EntityRecList, SchLists);

    FreeAndNil(SortListDS);
    FreeAndNil(SortListNS);
end;

procedure TDirection.DoSortSchedules(SortList: TDirectionSortRoutesList;
    const Indexes: TDRIndexList; IsNS: Boolean);
var
    Index: SizeUInt;
    RouteSchList: TSchList;
    RouteSchItem: TSchListItem;
    SortItemEntity: TEntity;
begin
    for Index in Indexes do
    begin
        RouteSchList := SchedulesOf(Routes[Index]);
        while RouteSchList.Count > 0 do begin
            RouteSchItem := RouteSchList[0];
            if RouteSchItem.Sch.NonStop <> IsNS then begin
                RouteSchList.Delete(0);
                continue;
            end;

            SortItemEntity := specialize IfThen<TEntity>(IsNS, FakeNonStop, FakeStop);
            if SortList.Count = 0 then
                SortList.AddWithEntity(SortItemEntity);

            SortList.AddSchListItem(RouteSchItem, SortItemEntity);
            RouteSchList.Delete(0);
        end;
        FreeAndNil(RouteSchList);
    end;
end;

procedure TDirection.MoveSortedSchedules(SortList: TDirectionSortRoutesList;
    EntityRecList: TEntityRecList; SchLists: TSchListList);
var SortItem: TDirectionSortRoutesItem;
begin
    for SortItem in SortList do begin
        EntityRecList.Add(SortItem.Entity);
        SchLists.Add(SortItem.SchList);
        SortItem.SchList := nil;
    end;
end;

function TDirection.Serialize(): String;
var EndPos: SizeInt;
begin
    Result := inherited Serialize();
    EndPos := Result.LastIndexOf('}') + 1;
    if EndPos > 0 then
        Insert(Format('Stations = %s; ', [Stations.Serialize()]), Result, EndPos);
end;

constructor TDirection.CreateAndDeserialize(var S: String);
var StationsStr: String;
begin
    inherited CreateAndDeserialize(S);
    Init();

    ExtractField('Stations', S, StationsStr, efBlock);
    Stations.Deserialize(StationsStr);
end;

constructor TDirection.Create(_ID: TID; _EType: TEntityType; _Name: String);
begin
    inherited Create(_ID, _EType, _Name);
    Init();
end;

destructor TDirection.Destroy();
begin
    FreeAndNil(Stations);
    FreeAndNil(Routes);
    FreeAndNil(RoutesDS);
    FreeAndNil(RoutesNS);
    inherited Destroy();
end;

procedure TDirection.Init();
begin
    Stations := TIDList.Create();
    Routes := TDirectionRouteList.Create();
    RoutesDS := TDRIndexList.Create();
    RoutesNS := TDRIndexList.Create();
end;

initialization
    NoDirection := TDirection.Create(0, direction, UEntityRS_NoDirection);
    FakeStop    := TEntity.Create(0, fake, UEntityRS_DoesStop);
    FakeNonStop := TEntity.Create(0, fake, UEntityRS_NonStop);

finalization
    FreeAndNil(NoDirection);
    FreeAndNil(FakeStop);
    FreeAndNil(FakeNonStop);

end.

