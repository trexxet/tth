unit PickerStation;

{$mode ObjFPC}{$H+}

interface

uses
    Classes, SysUtils, Forms, Controls, Graphics, ButtonPanel,
    ExtCtrls,
    ListControl, ListControl_Conn, ListControl_EntityList,
    UEntity, UStation, UID;

type

    { TPickerStationForm }
    TPickerStationForm = class(TForm)
        ButtonPanel: TButtonPanel;
        LCPanel: TPanel;
        procedure ListBoxDblClick(Sender: TObject);
    public
        LC: specialize TLC_EL_Connector<TStation>;
        SelectedID: TID;
    private
        function PickerLCOnSelect(var ID: TID; Index: SizeInt): Boolean;
    public
        constructor Create(TheOwner: TComponent); override;
        destructor Destroy(); override;
    end;

var
    PickerStationForm: TPickerStationForm;

implementation

{$R *.lfm}

{ TPickerStationForm }

procedure TPickerStationForm.ListBoxDblClick(Sender: TObject);
begin
    ButtonPanel.OKButton.Click();
end;

function TPickerStationForm.PickerLCOnSelect(var ID: TID;
    Index: SizeInt): Boolean;
begin
    SelectedID := ID;
    Result := True;
end;

constructor TPickerStationForm.Create(TheOwner: TComponent);
var
    LCCActions: TLCCActions;
begin
    inherited Create(TheOwner);

    LCPanel.Color := clDefault;
    LCCActions := Default(TLCCActions);
    LCCActions[lcOnSelect] := @PickerLCOnSelect;
    LC := specialize TLC_EL_Connector<TStation>.Create(LCPanel, LCCActions);
    LC.LCFrame.RenameEnabled := False;
    LC.LCFrame.ButtonPanelEnabled := False;
    LC.LCFrame.ListBox.OnDblClick := @ListBoxDblClick;
end;

destructor TPickerStationForm.Destroy();
begin
    FreeAndNil(LC);
    inherited Destroy();
end;

end.

