unit StopScheduleView;

{$mode ObjFPC}{$H+}

interface

uses
    Classes, SysUtils, laz.VirtualTrees, ScheduleView,
    UEntityRecord, URoute, UTrain, USchList, UID;

type

    { TStopScheduleViewFrame }

    TStopScheduleViewFrame = class(TScheduleViewFrame)
        procedure VTEInitNode(Sender: TBaseVirtualTree; ParentNode,
            Node: PVirtualNode; var InitialStates: TVirtualNodeInitStates);
        procedure VTSInitNode(Sender: TBaseVirtualTree; ParentNode,
            Node: PVirtualNode; var InitialStates: TVirtualNodeInitStates);
    private
        Route: TRoute;
        StopIndex: SizeUInt;
    protected
        function GetSyncFocusIndex(Data: PEntityRec): Integer; override;
    public
        procedure AssignData(RouteID: TID; _StopIndex: SizeUInt);
        procedure Reassign();
    public
        constructor Create(TheOwner: TComponent); override;
    end;

implementation

uses
    Graphics, LCLIntf, LCLType,
    UTimetable;

{$R *.lfm}

{ TStopScheduleViewFrame }

procedure TStopScheduleViewFrame.VTEInitNode(Sender: TBaseVirtualTree;
    ParentNode, Node: PVirtualNode; var InitialStates: TVirtualNodeInitStates);
var
    Data: PEntityRec;
    TrainID: TID;
begin
    if (Node = nil) or (Route = nil) then exit;
    Data := VTE.GetNodeData(Node);
    if Data = nil then exit;

    TrainID := Route.Trains.IDOf(Node^.Index);
    Data^ := Route.Trains[TrainID];
end;

procedure TStopScheduleViewFrame.VTSInitNode(Sender: TBaseVirtualTree;
    ParentNode, Node: PVirtualNode; var InitialStates: TVirtualNodeInitStates);
var
    Data: PSchList;
    TrainID: TID;
begin
    if (Node = nil) or (Route = nil) then exit;
    Data := VTS.GetNodeData(Node);
    if Data = nil then exit;

    TrainID := Route.Trains.IDOf(Node^.Index);
    Data^ := Route.Trains[TrainID].SchedulesFor(StopIndex);
    CalcRects(Data^);
    MakeHintTexts(Data^);
end;

function TStopScheduleViewFrame.GetSyncFocusIndex(Data: PEntityRec): Integer;
begin
    // Sync focused node to selected stop in TrainLC
    Result := Route.Trains.OrderOf(Data^.ID);
end;

procedure TStopScheduleViewFrame.AssignData(RouteID: TID; _StopIndex: SizeUInt);
begin
    VTE.Clear();
    VTS.Clear();
    if RouteID > 0 then
    begin
        if not Timetable.Routes.Exists(RouteID) then exit;
        Route := Timetable.Routes[RouteID];
        StopIndex := _StopIndex;
        VTE.RootNodeCount := Route.Trains.Count;
        VTS.RootNodeCount := Route.Trains.Count;
    end;
end;

procedure TStopScheduleViewFrame.Reassign();
begin
    if Route = nil then exit;
    AssignData(Route.ID, StopIndex);
    VTE.Invalidate();
    VTS.Invalidate();
end;

constructor TStopScheduleViewFrame.Create(TheOwner: TComponent);
begin
    VTE_NoFakeBG := False;
    VTE_AlignFakeRight := False;
    VTS_OwnsData := True;
    VTS_UseRouteColors := False;
    VTS_DrawTrainName := False;

    inherited Create(TheOwner);
end;

end.

