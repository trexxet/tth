unit StationScheduleView;

{$mode ObjFPC}{$H+}

interface

uses
    Classes, SysUtils, laz.VirtualTrees, ScheduleView,
    UEntityRecord, USchList, UStation, UDirection, UID;

type

    { TStationScheduleViewFrame }
    TStationScheduleViewFrame = class(TScheduleViewFrame)
        procedure VTEInitNode(Sender: TBaseVirtualTree; ParentNode,
            Node: PVirtualNode; var InitialStates: TVirtualNodeInitStates);
        procedure VTSInitNode(Sender: TBaseVirtualTree; ParentNode,
            Node: PVirtualNode; var InitialStates: TVirtualNodeInitStates);
    private
        Station: TStation;
        EntityRecList: TEntityRecList;
        SchLists: TSchListList;
    private
        function AssignDirection(Direction: TDirection): SizeUInt;
    public
        procedure AssignData(StationID: TID);
        procedure Reassign();
    public
        constructor Create(TheOwner: TComponent); override;
        destructor Destroy(); override;
    end;

implementation

uses
    Graphics, LCLIntf,
    UTimetable;

{$R *.lfm}

{ TStationScheduleViewFrame }

procedure TStationScheduleViewFrame.VTEInitNode(Sender: TBaseVirtualTree;
    ParentNode, Node: PVirtualNode; var InitialStates: TVirtualNodeInitStates);
var
    Data: PEntityRec;
begin
    if (Node = nil) or (Station = nil) then exit;
    Data := VTE.GetNodeData(Node);
    if Data = nil then exit;

    Data^ := EntityRecList[Node^.Index];
end;

procedure TStationScheduleViewFrame.VTSInitNode(Sender: TBaseVirtualTree;
    ParentNode, Node: PVirtualNode; var InitialStates: TVirtualNodeInitStates);
var
    Data: PSchList;
begin
    if (Node = nil) or (Station = nil) then exit;
    Data := VTS.GetNodeData(Node);
    if Data = nil then exit;

    Data^ := SchLists[Node^.Index];
    CalcRects(Data^);
    MakeHintTexts(Data^);
end;

function TStationScheduleViewFrame.AssignDirection(Direction: TDirection): SizeUInt;
begin
    if (Direction = nil) or (Direction.Routes.Count = 0) then exit(0);
    EntityRecList.Add(Direction);
    SchLists.Add(TSchList.Create());
    Result := Direction.SchedulesSorted(EntityRecList, SchLists) + 1;
end;

procedure TStationScheduleViewFrame.AssignData(StationID: TID);
var
    Index: SizeInt;
    DirectionID: TID;
    Direction: TDirection;
    NodeCount: SizeUInt = 0;
begin
    EntityRecList.Clear();
    SchLists.Clear();
    VTE.Clear();
    VTS.Clear();
    if StationID > 0 then begin
        if not Timetable.Stations.Exists(StationID) then exit;
        Station := Timetable.Stations[StationID];
        for Index := 0 to Station.Directions.Count - 1 do begin
            DirectionID := Station.Directions.IDOf(Index);
            Direction := Station.Directions[DirectionID];
            NodeCount += AssignDirection(Direction);
        end;
        NodeCount += AssignDirection(Station.NoDirectionLocal);
        VTE.RootNodeCount := NodeCount;
        VTS.RootNodeCount := NodeCount;
    end;
end;

procedure TStationScheduleViewFrame.Reassign();
begin
    if Station = nil then exit;
    AssignData(Station.ID);
    VTE.Invalidate();
    VTS.Invalidate();
end;

constructor TStationScheduleViewFrame.Create(TheOwner: TComponent);
begin
    EntityRecList := TEntityRecList.Create();
    SchLists := TSchListList.Create();

    VTE_NoFakeBG := True;
    VTE_AlignFakeRight := True;
    VTS_OwnsData := False;
    VTS_UseRouteColors := True;
    VTS_DrawTrainName := True;

    inherited Create(TheOwner);
end;

destructor TStationScheduleViewFrame.Destroy();
begin
    FreeAndNil(EntityRecList);
    FreeAndNil(SchLists);
    inherited Destroy();
end;

end.

