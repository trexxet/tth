unit ScheduleView_ColumnTypes;

{$mode ObjFPC}{$H+}

interface

uses
    SysUtils, Graphics, laz.VirtualTrees;

type

    TScheduleViewColumnType = (colIndicator, colEntity);

    operator:=(I: TColumnIndex) O: TScheduleViewColumnType;
    operator:=(I: TScheduleViewColumnType) O: TColumnIndex;
    operator=(L: TColumnIndex; R: TScheduleViewColumnType): Boolean;
    operator<(L: TColumnIndex; R: TScheduleViewColumnType): Boolean;
    operator>(L: TColumnIndex; R: TScheduleViewColumnType): Boolean;

implementation

operator := (I: TColumnIndex)O: TScheduleViewColumnType;
begin
    O := TScheduleViewColumnType(I);
end;

operator := (I: TScheduleViewColumnType)O: TColumnIndex;
begin
    O := ord(I);
end;

operator = (L: TColumnIndex; R: TScheduleViewColumnType): Boolean;
begin
    Result := L = ord(R);
end;

operator<(L: TColumnIndex; R: TScheduleViewColumnType): Boolean;
begin
    Result := L < ord(R);
end;

operator>(L: TColumnIndex; R: TScheduleViewColumnType): Boolean;
begin
    Result := L > ord(R);
end;

end.

