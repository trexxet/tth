unit ScheduleView;

{$mode ObjFPC}{$H+}

interface

uses
    Classes, SysUtils, Graphics, Forms, Controls, ExtCtrls, StdCtrls, Buttons,
    ActnList, laz.VirtualTrees, ListControl, UEntityRecord, USchList,
    UStopSchedule, UEntity, URoute, UTrain, UZoom, ScheduleView_Scale,
    ScheduleView_ColumnTypes;

type

    { TScheduleViewFrame }
    TScheduleViewFrame = class(TFrame)
        Icons: TImageList;
        {%REGION VT Entity} published
        VTE: TLazVirtualDrawTree;
        procedure VTEDrawNode(Sender: TBaseVirtualTree;
            const PaintInfo: TVTPaintInfo);
        procedure VTEResetNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
        procedure VTEFreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
        procedure VTEExit(Sender: TObject);
        {%ENDREGION}
        {%REGION VT Schedule} published
        VTS: TLazVirtualDrawTree;
        procedure VTSDrawNode(Sender: TBaseVirtualTree;
            const PaintInfo: TVTPaintInfo);
        procedure VTSResetNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
        procedure VTSFreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
        procedure VTSHeaderDrawQueryElements(Sender: TVTHeader;
            var PaintInfo: THeaderPaintInfo;
            var Elements: THeaderPaintElements);
        procedure VTSAdvancedHeaderDraw(Sender: TVTHeader;
            var PaintInfo: THeaderPaintInfo;
            const Elements: THeaderPaintElements);
        procedure VTSGetHintKind(Sender: TBaseVirtualTree; Node: PVirtualNode;
            Column: TColumnIndex; var Kind: TVTHintKind);
        procedure VTSGetHint(Sender: TBaseVirtualTree; Node: PVirtualNode;
            Column: TColumnIndex; var LineBreakStyle: TVTTooltipLineBreakStyle;
            var HintText: String);
        procedure VTSMouseMove(Sender: TObject; Shift: TShiftState; X,
            Y: Integer);
        procedure VTSFocusChanged(Sender: TBaseVirtualTree; Node: PVirtualNode;
            Column: TColumnIndex);
        {%ENDREGION}
        {%REGION Button Panel} published
        ButtonPanel: TPanel;
        PointerTimeLabel: TLabel;
        ZoomInBtn: TBitBtn;
        ZoomResetBtn: TBitBtn;
        ZoomOutBtn: TBitBtn;
        {%ENDREGION}
        {%REGION Zoom Actions} published
        ZoomActions: TActionList;
        ZoomInAction: TAction;
        ZoomResetAction: TAction;
        ZoomOutAction: TAction;
        procedure ZoomInActionExecute(Sender: TObject);
        procedure ZoomResetActionExecute(Sender: TObject);
        procedure ZoomOutActionExecute(Sender: TObject);
        {%ENDREGION}
    private
        Zoom: TZoom;
        procedure ZoomChange(Change: TZoomChange);
        function WithOffsetX(X: Integer): Integer; inline;
    private
        function CheckVTEFakeBG(Data: PEntityRec): Boolean; inline;
        function CheckVTEFakeAlign(Data: PEntityRec): Boolean; inline;
        function GetRoute(const Sch: TStopSchedule): TRoute; inline;
        function GetRouteBGColor(const Sch: TStopSchedule): TColor; inline;
        function GetRouteFGColor(const Sch: TStopSchedule): TColor; inline;
        function GetSchColor(const Sch: TStopSchedule): TColor; inline;
        function GetTrainName(const Sch: TStopSchedule): String; inline;
    private
        VTS_Scale: TScheduleView_Scale;
        procedure SchItemToScale(var SchItem: TSchListItem; W, H: Integer);
        procedure DrawSch(PaintInfo: TVTPaintInfo; const SchItem: TSchListItem);
        procedure DrawTrainNamesIfFit(PaintInfo: TVTPaintInfo;
            const SchItem: TSchListItem);
        procedure DrawTrainNameIfFit(PaintInfo: TVTPaintInfo; R: TRect;
            TrainName: String);
    protected
        procedure CalcRects(Data: TSchList);
        procedure MakeHintTexts(Data: TSchList);
        function GetSyncFocusIndex(Data: PEntityRec): Integer; virtual; abstract;
    public
        function ResetNode(VT: TLazVirtualDrawTree; Node: PVirtualNode): Boolean;
        function ResetNodesAt(Index: Integer): Boolean;
    public
        SyncFocus: TLCAction;
    public
        procedure TimeModeChanged(IsGameTime: Boolean);
        constructor Create(TheOwner: TComponent); override;
        destructor Destroy(); override;
    protected // Params
        VTE_NoFakeBG: Boolean;
        VTE_AlignFakeRight: Boolean;
        VTS_OwnsData: Boolean;
        VTS_UseRouteColors: Boolean;
        VTS_DrawTrainName: Boolean;
    end;

implementation

uses
    Math, LCLIntf, LCLType, VTUtils,
    UTimetable, USTime;

procedure SetFontColor(const Ed: TScheduleViewFrame; const Node: PVirtualNode;
    SchColumn: TScheduleViewColumnType; CellCanvas: TCanvas); inline;
begin
    with Ed.VTE, CellCanvas.Font do
    begin
        if (SchColumn = TScheduleViewColumnType(FocusedColumn)) and (Node = FocusedNode) then
            Color := clHighlightText
        else
            Color := clWindowText;
    end;
end;

procedure FillBG(SchColumn: TScheduleViewColumnType; CellCanvas: TCanvas;
    CellRect: TRect); inline;
begin
    with CellCanvas.Brush do
    begin
        if SchColumn = colEntity then
            Color := sch_clEntityName
        else exit;
    end;
    CellCanvas.FillRect(CellRect);
end;

{$R *.lfm}

{ TScheduleViewFrame }

procedure TScheduleViewFrame.VTEDrawNode(Sender: TBaseVirtualTree;
    const PaintInfo: TVTPaintInfo);
var
    Data: PEntityRec;
    PI: TVTPaintInfo;
    SchColumn: TScheduleViewColumnType;
begin
    PI := PaintInfo;  // ObjFPC can't put const arg into with..do
    with VTE, PI do
    begin
        Data := GetNodeData(Node);
        if not CheckData(Data) then exit;
        SchColumn := TScheduleViewColumnType(Column);

        SetBKMode(Canvas.Handle, TRANSPARENT);
        SetFontColor(Self, Node, SchColumn, Canvas);
        if (Column <> FocusedColumn) or (Node <> FocusedNode) then
            if CheckVTEFakeBG(Data) then
                FillBG(SchColumn, Canvas, CellRect);

        // Draw the contents
        case SchColumn of
        colIndicator: DrawIndicator(Canvas, CellRect, Icons, Node = FocusedNode);
        colEntity: DrawCaption(Canvas, ContentRect, Data^.Name, CheckVTEFakeAlign(Data));
        end;
    end;
end;

procedure TScheduleViewFrame.VTEResetNode(Sender: TBaseVirtualTree;
    Node: PVirtualNode);
begin
    VTEFreeNode(Sender, Node);
end;

procedure TScheduleViewFrame.VTEFreeNode(Sender: TBaseVirtualTree;
    Node: PVirtualNode);
var Data: PEntityRec;
begin
    Data := VTE.GetNodeData(Node);
    if Data = nil then exit;
    Finalize(Data^);
end;

procedure TScheduleViewFrame.VTEExit(Sender: TObject);
var Data: PEntityRec;
begin
    if (VTE.FocusedNode <> nil) and (SyncFocus <> nil) then
    begin
        Data := VTE.GetNodeData(VTE.FocusedNode);
        if not CheckData(Data) then exit;
        SyncFocus(GetSyncFocusIndex(Data));
    end;
end;

procedure TScheduleViewFrame.VTSDrawNode(Sender: TBaseVirtualTree;
    const PaintInfo: TVTPaintInfo);
var
    Data: PSchList;
    PI: TVTPaintInfo;
    SchItem: TSchListItem;
    LinePos: Integer;
begin
    PI := PaintInfo;  // ObjFPC can't put const arg into with..do
    with VTS, PI do
    begin
        Data := GetNodeData(Node);
        if not CheckData(Data) then exit;
        SetBKMode(Canvas.Handle, TRANSPARENT);

        Canvas.Font.Color := clWindowText;
        if VTS_DrawTrainName then Canvas.Font.Height := Node^.NodeHeight - 2;

        for SchItem in Data^ do
            DrawSch(PI, SchItem);

        LinePos := WithOffsetX(VTS_Scale.PointerX);
        Canvas.Line(LinePos, 0, LinePos, Node^.NodeHeight);
    end;
end;

procedure TScheduleViewFrame.VTSResetNode(Sender: TBaseVirtualTree;
    Node: PVirtualNode);
begin
    VTSFreeNode(Sender, Node);
end;

procedure TScheduleViewFrame.VTSFreeNode(Sender: TBaseVirtualTree;
    Node: PVirtualNode);
var Data: PSchList;
begin
    Data := VTS.GetNodeData(Node);
    if Data = nil then exit;
    if VTS_OwnsData then FreeAndNil(Data^)
    else Finalize(Data^);
end;

procedure TScheduleViewFrame.VTSHeaderDrawQueryElements(Sender: TVTHeader;
    var PaintInfo: THeaderPaintInfo; var Elements: THeaderPaintElements);
begin
    if PaintInfo.Column = nil then exit;
    Include(Elements, hpeText);
end;

procedure TScheduleViewFrame.VTSAdvancedHeaderDraw(Sender: TVTHeader;
    var PaintInfo: THeaderPaintInfo; const Elements: THeaderPaintElements);
begin
    with PaintInfo do
    begin
        if Column = nil then exit;
        VTS_Scale.Draw(TargetCanvas, VTS.OffsetX);
    end;
end;

procedure TScheduleViewFrame.VTSGetHintKind(Sender: TBaseVirtualTree;
    Node: PVirtualNode; Column: TColumnIndex; var Kind: TVTHintKind);
begin
    if Column = 0 then Kind := vhkText;
end;

procedure TScheduleViewFrame.VTSGetHint(Sender: TBaseVirtualTree;
    Node: PVirtualNode; Column: TColumnIndex;
    var LineBreakStyle: TVTTooltipLineBreakStyle; var HintText: String);
var
    Data: PSchList;
    SchItem: TSchListItem;
    HitPos: TPoint;
begin
    Data := VTS.GetNodeData(Node);
    if not CheckData(Data) then exit;

    HintText := '';
    HitPos := Point(WithOffsetX(VTS.ScreenToControl(Mouse.CursorPos).X), 1);
    for SchItem in Data^ do
        if SchItem.R1.Contains(HitPos)
        or (SchItem.HasR2 and SchItem.R2.Contains(HitPos)) then begin
            HintText := SchItem.HintText;
            break;
        end;
end;

procedure TScheduleViewFrame.VTSMouseMove(Sender: TObject; Shift: TShiftState;
    X, Y: Integer);
begin
    if InRange(X, 1,
               Min(VTS.Width - 1, VTS.Header.Columns[0].Width - 1)) then
    begin
        VTS_Scale.PointerX := X;
        VTS.Invalidate();
        PointerTimeLabel.Caption := VTS_Scale.PointerPosToStr(VTS.OffsetX);
    end;
end;

procedure TScheduleViewFrame.VTSFocusChanged(Sender: TBaseVirtualTree;
    Node: PVirtualNode; Column: TColumnIndex);
var OldFocusedNode: PVirtualNode;
begin
    if Node = nil then exit;
    OldFocusedNode := VTE.FocusedNode;

    VTE.FocusedNode := NodeAt(VTE, Node^.Index);
    VTE.FocusedColumn := colEntity;

    if OldFocusedNode <> nil then
        VTE.Selected[OldFocusedNode] := False;
    VTE.Selected[VTE.FocusedNode] := True;
    VTE.SetFocus();
end;

procedure TScheduleViewFrame.ZoomInActionExecute(Sender: TObject);
begin
    ZoomChange(zcIn);
end;

procedure TScheduleViewFrame.ZoomResetActionExecute(Sender: TObject);
begin
    ZoomChange(zcReset);
end;

procedure TScheduleViewFrame.ZoomOutActionExecute(Sender: TObject);
begin
    ZoomChange(zcOut);
end;

procedure TScheduleViewFrame.ZoomChange(Change: TZoomChange);
var BaseWidth: Integer;
begin
    BaseWidth := VTS.Header.Columns[0].Width div Zoom.Value;

    Zoom.Change(Change);
    ZoomInAction.Enabled := Zoom.CanZoomIn;
    ZoomResetAction.Enabled := Zoom.CanResetZoom;
    ZoomOutAction.Enabled := Zoom.CanZoomOut;

    VTS.Header.Columns[0].Width := BaseWidth * Zoom.Value;
    VTS_Scale.Width := VTS.Header.Columns[0].Width;
    ResetAllNodes(VTS);
end;

function TScheduleViewFrame.WithOffsetX(X: Integer): Integer;
begin
    Result := X - VTS.OffsetX;
end;

function TScheduleViewFrame.CheckVTEFakeBG(Data: PEntityRec): Boolean;
begin
    Result := (not VTE_NoFakeBG) or (Data^.EType <> fake);
end;

function TScheduleViewFrame.CheckVTEFakeAlign(Data: PEntityRec): Boolean;
begin
    Result := VTE_AlignFakeRight and (Data^.EType = fake);
end;

function TScheduleViewFrame.GetRoute(const Sch: TStopSchedule): TRoute;
begin
    if Sch = nil then exit(nil);
    Result := Timetable.Routes[Sch.Route];
end;

function TScheduleViewFrame.GetRouteBGColor(const Sch: TStopSchedule): TColor;
var Route: TRoute;
begin
    Route := GetRoute(Sch);
    if Route = nil then exit(clBlack);
    Result := Route.Color;
end;

function TScheduleViewFrame.GetRouteFGColor(const Sch: TStopSchedule): TColor;
var Route: TRoute;
begin
    Route := GetRoute(Sch);
    if Route = nil then exit(clWhite);
    Result := Route.FGColor;
end;

function TScheduleViewFrame.GetSchColor(const Sch: TStopSchedule): TColor;
begin
    if Sch = nil then exit;
    with Sch do begin
        if IsGhost then
            Result := sch_clGhost
        else if AutoCalcInvalid[Arrival]
             or AutoCalcInvalid[Stopping]
             or AutoCalcInvalid[Departure] then
            Result := sch_clInvalid
        else if IsRouteTime[Stopping] then
            Result := sch_clRouteTime
        else
            Result := sch_clUneditable;
    end;
end;

function TScheduleViewFrame.GetTrainName(const Sch: TStopSchedule): String;
var
    Route: TRoute;
    Train: TTrain;
begin
    Result := '';
    if Sch = nil then exit;
    Route := GetRoute(Sch);
    if Route = nil then exit;
    Train := Route.Trains[Sch.Train];
    if Train = nil then exit;
    Result := Train.Name;
end;

procedure TScheduleViewFrame.CalcRects(Data: TSchList);
var
    i: Integer;
    SchItem: TSchListItem;
begin
    for i := 0 to Data.Count - 1 do
        with VTS do begin
            // I have no words left to describe how fucking retarded the Pascal is
            // that I can't change a fucking record right in the list
            SchItem := Data.Items[i];
            SchItemToScale(SchItem, Header.Columns[0].Width,
                           DefaultNodeHeight - 1);
            Data.Items[i] := SchItem;
        end;
end;

procedure TScheduleViewFrame.MakeHintTexts(Data: TSchList);
var
    SchItem: TSchListItem;
    i: Integer;
begin
    // A normal person would write 'for SchItem in Data do'
    // but this fucking degenerate language would whine
    // ILLEGAL ASSIGNMENT TO FOR LOOP VARIABLE
    for i := 0 to Data.Count - 1 do
    begin
        SchItem := Data[i];
        with SchItem do
            HintText := Format('%s: %s-%s', [GetTrainName(Sch),
                               Sch[Arrival].ToStr(), Sch[Departure].ToStr()]);
        // absolutely disgusting
        Data[i] := SchItem;
    end;
end;

procedure TScheduleViewFrame.SchItemToScale(var SchItem: TSchListItem;
    W, H: Integer);
var
    tArr, tDep: TTimeInt;
    posArr, posDep: Integer;
begin
    with SchItem do
    begin
        tArr := Sch[Arrival].MT;
        tDep := Sch[Departure].MT;

        HasR2 := tArr > tDep;
        if tArr = tDep then
        begin
            R1 := Rect(0, 0, 0, 0);
            exit;
        end;

        posArr := VTS_Scale.ValueToPos(tArr, 0);
        posDep := VTS_Scale.ValueToPos(tDep, 0);

        if HasR2 then
        begin;
            R1.TopLeft     := Point(0, 0);
            R1.BottomRight := Point(posDep + 1, H);
            R2.TopLeft     := Point(posArr, 0);
            R2.BottomRight := Point(W, H);
        end else begin
            R1.TopLeft     := Point(posArr, 0);
            R1.BottomRight := Point(posDep + 1, H);
        end;
    end;
end;

procedure TScheduleViewFrame.DrawSch(PaintInfo: TVTPaintInfo;
    const SchItem: TSchListItem);
begin
    with VTS, PaintInfo, SchItem do begin
        with PaintInfo.Canvas.Brush do
            if VTS_UseRouteColors then
                Color := GetRouteBGColor(Sch)
            else
                Color := GetSchColor(Sch);

        Canvas.Rectangle(R1);
        if HasR2 then Canvas.Rectangle(R2);

        if VTS_DrawTrainName then DrawTrainNamesIfFit(PaintInfo, SchItem);
    end;
end;

procedure TScheduleViewFrame.DrawTrainNamesIfFit(PaintInfo: TVTPaintInfo;
    const SchItem: TSchListItem);
var
    TrainName: String;
    FontColorSave: TColor;
begin
    with PaintInfo.Canvas.Font, SchItem do begin
        TrainName := GetTrainName(SchItem.Sch);
        if TrainName = '' then exit;

        FontColorSave := Color;
        Color := GetRouteFGColor(Sch);

        DrawTrainNameIfFit(PaintInfo, R1, TrainName);
        if HasR2 then DrawTrainNameIfFit(PaintInfo, R2, TrainName);

        Color := FontColorSave;
    end;
end;

procedure TScheduleViewFrame.DrawTrainNameIfFit(PaintInfo: TVTPaintInfo;
    R: TRect; TrainName: String);
var TextWidth: Integer;
begin
    with PaintInfo do begin
        TextWidth := Canvas.TextWidth(TrainName);
        if R.Width >= TextWidth + 2 then
            DrawText(Canvas.Handle, PChar(TrainName), Length(TrainName), R,
                     DT_CENTER or DT_VCENTER or DT_SINGLELINE);
    end;
end;

function TScheduleViewFrame.ResetNode(VT: TLazVirtualDrawTree;
    Node: PVirtualNode): Boolean;
var
    DataVTE: PEntityRec;
    DataVTS: PSchList;
begin
    Result := False;

    if VT = VTE then
    begin
        DataVTE := VTE.GetNodeData(Node);
        if not CheckData(DataVTE) then exit;
    end;
    if VT = VTS then
    begin
        DataVTS := VTS.GetNodeData(Node);
        if not CheckData(DataVTS) then exit;
    end;

    VT.ResetNode(Node);
    Result := True;
end;

function TScheduleViewFrame.ResetNodesAt(Index: Integer): Boolean;
begin
    Result := ResetNode(VTE, NodeAt(VTE, Index));
    Result := ResetNode(VTS, NodeAt(VTS, Index)) and Result;
end;

procedure TScheduleViewFrame.TimeModeChanged(IsGameTime: Boolean);
begin
    VTS_Scale.TimeModeChanged(IsGameTime);
    VTS.Header.Invalidate(VTS.Header.Columns[0]);
    ResetAllNodes(VTS);
end;

constructor TScheduleViewFrame.Create(TheOwner: TComponent);
begin
    inherited Create(TheOwner);
    VTE.NodeDataSize := SizeOf(TEntityRec);
    VTS.NodeDataSize := SizeOf(TSchList);

    VTS.Header.Columns[0].Width := TTimeInt.Wrap div 6;
    Zoom.Default();
    VTS_Scale := TScheduleView_Scale.Create(VTS, @Zoom);
    ZoomChange(zcReset);
end;

destructor TScheduleViewFrame.Destroy();
begin
    FreeAndNil(VTS_Scale);
    inherited Destroy();
end;

end.

