unit ScheduleView_Scale;

{$mode ObjFPC}{$H+}

interface

uses
    Classes, SysUtils, Controls, Graphics, ECScale, laz.VirtualTrees,
    UZoom;

type

    { TScheduleView_Scale }
    TScheduleView_Scale = class(TObject)
    private
        Scale: TECScale;
        FWidth: Integer;
        Zoom: PZoom;
        procedure ConvertHM(Value: Double; out H, M: Word);
        procedure EncodeHM(Sender: TObject; var AValue: Double);
        procedure SetWidth(const AValue: Integer);
    public
        PointerX: Integer;
        property Width: Integer read FWidth write SetWidth;

        procedure TimeModeChanged(IsGameTime: Boolean);
        procedure Rezoom();
        procedure Draw(_Canvas: TCanvas; Offset: Integer);
        // The value may be a bit off because of how TECScale rounds tick position
        function PointerPosToStr(Offset: Integer): String;
        function ValueToPos(Value, Offset: Integer): Integer;

        constructor Create(Parent: TLazVirtualDrawTree; _Zoom: PZoom);
        destructor Destroy(); override;
    end;


implementation

uses Math, ECTypes, USTime;

{ TScheduleView_Scale }

procedure TScheduleView_Scale.ConvertHM(Value: Double; out H, M: Word);
begin
    //why the fuck DivMod doesn't declare outputs as out and require init
    H := 0;
    M := 0;
    DivMod(Round(Value), TTimeInt.Mins, H, M);
    if H = TTimeInt.Mins then H := 0;
end;

procedure TScheduleView_Scale.EncodeHM(Sender: TObject; var AValue: Double);
var H, M: Word;
begin
    ConvertHM(AValue, H, M);
    AValue := EncodeTime(0, H, M, 0);
end;

procedure TScheduleView_Scale.SetWidth(const AValue: Integer);
begin
    if FWidth = AValue then Exit;
    FWidth := AValue;
    Rezoom();
    Scale.CalcTickPosAndValues(AValue, False);
end;

procedure TScheduleView_Scale.TimeModeChanged(IsGameTime: Boolean);
begin
    Scale.Max := TTimeInt.ModeWrap() - 1;
    Rezoom();
    Scale.CalcTickPosAndValues(FWidth, False);
end;

procedure TScheduleView_Scale.Rezoom();
begin
    if Zoom = nil then exit;
    with Scale, Zoom^ do
        case Level of
            ZoomMinLevel: begin
                TickLongValue := TTimeInt.ModeWrap() div 6;
                TickShortValue := TTimeInt.ModeWrap() div 12;
            end;
            ZoomMaxLevel: begin
                TickLongValue := TTimeInt.ModeWrap() div 30;
                TickShortValue := TTimeInt.ModeWrap() div 60;
            end;
            else begin
                TickLongValue := TTimeInt.ModeWrap() div 12;
                TickShortValue := TTimeInt.ModeWrap() div 24;
            end;
        end;
end;

procedure TScheduleView_Scale.Draw(_Canvas: TCanvas; Offset: Integer);
begin
    with _Canvas, Scale do
    begin
        ValueIndent := GetFontData(Font.Handle).Height - 4;
        Draw(_Canvas, True, True, eopBottom, Point(Offset + 1, 0), []);
        Line(PointerX, 0, PointerX, ClipRect.Bottom);
    end;
end;

function TScheduleView_Scale.PointerPosToStr(Offset: Integer): String;
var
    Pos: Integer;
    Value: Double;
    H: Word = 0;
    M: Word = 0;
begin
    Pos := PointerX - Offset - 1;
    Value := Scale.Max * Pos / (FWidth - 0.99);
    ConvertHM(Value, H, M);
    Result := Format('%.2u:%.2u', [H, M]);
end;

function TScheduleView_Scale.ValueToPos(Value, Offset: Integer): Integer;
var Pos: Integer;
begin
    Pos := Round(Value * (FWidth - 0.99) / Scale.Max);
    Result := Pos + Offset + 1;
end;

constructor TScheduleView_Scale.Create(Parent: TLazVirtualDrawTree;
    _Zoom: PZoom);
begin
    Scale := TECScale.Create(Parent);
    Zoom := _Zoom;
    with Scale do
    begin
        DateTimeFormat := 'nn:ss';
        Min := 0;
        Max := TTimeInt.Wrap - 1;
        TickLength := Parent.Header.Height;
        TickLongValue := TTimeInt.Wrap div 6;
        TickShortValue := TTimeInt.Wrap div 12;
        TickVisible := etvLongShort;
        TickIndent := 0;
        TickAlign := etaOuter;
        ValueFormat := esvfTime;
        ValueVisible := evvValues;
        ValueShift := 20;
        OnPrepareValue := @EncodeHM;
        Width := Parent.Width;
    end;
end;

destructor TScheduleView_Scale.Destroy();
begin
    FreeAndNil(Scale);
    inherited Destroy();
end;

end.

