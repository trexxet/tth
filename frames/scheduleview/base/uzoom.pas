unit UZoom;

{$mode ObjFPC}{$H+}
{$modeswitch advancedrecords}

interface

uses
    SysUtils;

type

    TZoomChange = (zcIn, zcReset, zcOut);

    { TZoom }
    TZoom = record
    private
        FValue: Byte;
        FLevel: Byte;
        function GetCanZoomIn():    Boolean; inline;
        function GetCanResetZoom(): Boolean; inline;
        function GetCanZoomOut():   Boolean; inline;
    public
        property Value: Byte read FValue;
        property Level: Byte read FLevel;
        procedure Change(ZC: TZoomChange);

        property CanZoomIn:    Boolean read GetCanZoomIn;
        property CanResetZoom: Boolean read GetCanResetZoom;
        property CanZoomOut:   Boolean read GetCanZoomOut;

        procedure Default();
    end;
    PZoom = ^TZoom;

const
    ZoomDefault = 1;
    ZoomStep = 2;

    ZoomMinLevel = 1;
    ZoomDefaultLevel = 1;
    ZoomMaxLevel = 3;

implementation

{ TZoom }

function TZoom.GetCanZoomIn(): Boolean; inline;
begin
    Result := FLevel < ZoomMaxLevel;
end;

function TZoom.GetCanResetZoom(): Boolean; inline;
begin
    Result := FLevel <> ZoomDefaultLevel;
end;

function TZoom.GetCanZoomOut(): Boolean; inline;
begin
    Result := FLevel > ZoomMinLevel;
end;

procedure TZoom.Change(ZC: TZoomChange);
begin
    case ZC of
        zcIn: if GetCanZoomIn() then begin
            FValue := FValue * ZoomStep;
            FLevel += 1;
        end;
        zcReset: if GetCanResetZoom() then Default();
        zcOut: if GetCanZoomOut() then begin
            FValue := FValue div ZoomStep;
            FLevel -= 1;
        end;
    end;
end;

procedure TZoom.Default();
begin
    FValue := ZoomDefault;
    FLevel := ZoomDefaultLevel;
end;

end.

