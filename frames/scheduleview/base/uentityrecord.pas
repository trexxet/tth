unit UEntityRecord;

{$mode ObjFPC}{$H+}

interface

uses
    SysUtils, Generics.Collections, UEntity;

type

    TEntityRec = TEntity;
    PEntityRec = ^TEntityRec;
    TEntityRecList = specialize TList<TEntityRec>;

    function CheckData(const Data: PEntityRec): Boolean; inline; overload;

implementation

function CheckData(const Data: PEntityRec): Boolean;
begin
    Result := Data <> nil;
end;

end.

