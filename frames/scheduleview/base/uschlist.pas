unit USchList;

{$mode ObjFPC}{$H+}
{$modeswitch advancedrecords}

interface

uses
    Classes, SysUtils, Generics.Collections, UStopSchedule, ULogger;

type

    { TSchListItem }
    TSchListItem = record
        Sch: TStopSchedule;
        R1, R2: TRect;  // for cases like 59:00..01:00 we split into
        HasR2: Boolean; // two rects: 59:00..59:59 and 00:00..01:00
        HintText: String;

        class function Make(_Sch: TStopSchedule): TSchListItem; static;
    end;
    TSchList = specialize TList<TSchListItem>;
    PSchList = ^TSchList;
    TSchListList = specialize TObjectList<TSchList>;

    operator=(L, R: TSchListItem): Boolean;

    function CheckData(const Data: PSchList): Boolean; inline; overload;

implementation

operator = (L, R: TSchListItem): Boolean;
begin
    Result := L.Sch = R.Sch;
end;

function CheckData(const Data: PSchList): Boolean;
begin
    Result := Data <> nil;
end;

{ TSchListItem }

class function TSchListItem.Make(_Sch: TStopSchedule): TSchListItem;
begin
    Result.Sch := _Sch;
    Result.HintText := '';
end;

end.

