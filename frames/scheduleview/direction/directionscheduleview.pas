unit DirectionScheduleView;

{$mode ObjFPC}{$H+}

interface

uses
    Classes, SysUtils, laz.VirtualTrees, ScheduleView,
    UEntityRecord, USchList, UStation, UDirection, UID;

type

    { TDirectionScheduleViewFrame }
    TDirectionScheduleViewFrame = class(TScheduleViewFrame)
        procedure VTEInitNode(Sender: TBaseVirtualTree; ParentNode,
            Node: PVirtualNode; var InitialStates: TVirtualNodeInitStates);
        procedure VTSInitNode(Sender: TBaseVirtualTree; ParentNode,
            Node: PVirtualNode; var InitialStates: TVirtualNodeInitStates);
    private
        Station: TStation;
        Direction: TDirection;
        EntityRecList: TEntityRecList;
        SchLists: TSchListList;
    public
        procedure AssignData(StationID, DirectionID: TID);
        procedure Reassign();
    public
        constructor Create(TheOwner: TComponent); override;
        destructor Destroy(); override;
    end;

implementation

uses
    Graphics, LCLIntf,
    UTimetable;

{$R *.lfm}

{ TDirectionScheduleViewFrame }

procedure TDirectionScheduleViewFrame.VTEInitNode(Sender: TBaseVirtualTree;
    ParentNode, Node: PVirtualNode; var InitialStates: TVirtualNodeInitStates);
var
    Data: PEntityRec;
begin
    if (Node = nil) or (Station = nil) or (Direction = nil) then exit;
    Data := VTE.GetNodeData(Node);
    if Data = nil then exit;

    Data^ := EntityRecList[Node^.Index];
end;

procedure TDirectionScheduleViewFrame.VTSInitNode(Sender: TBaseVirtualTree;
    ParentNode, Node: PVirtualNode; var InitialStates: TVirtualNodeInitStates);
var
    Data: PSchList;
begin
    if (Node = nil) or (Station = nil) or (Direction = nil) then exit;
    Data := VTS.GetNodeData(Node);
    if Data = nil then exit;

    Data^ := SchLists[Node^.Index];
    CalcRects(Data^);
    MakeHintTexts(Data^);
end;

procedure TDirectionScheduleViewFrame.AssignData(StationID, DirectionID: TID);
var NodeCount: SizeUInt;
begin
    EntityRecList.Clear();
    SchLists.Clear();
    VTE.Clear();
    VTS.Clear();
    if (StationID > 0) and (DirectionID > 0) then begin
        if not Timetable.Stations.Exists(StationID) then exit;
        Station := Timetable.Stations[StationID];
        if not Station.Directions.Exists(DirectionID) then exit;
        Direction := Station.Directions[DirectionID];
        NodeCount := Direction.SchedulesSorted(EntityRecList, SchLists);
        VTE.RootNodeCount := NodeCount;
        VTS.RootNodeCount := NodeCount;
    end;
end;

procedure TDirectionScheduleViewFrame.Reassign();
begin
    if (Station = nil) or (Direction = nil) then exit;
    AssignData(Station.ID, Direction.ID);
    VTE.Invalidate();
    VTS.Invalidate();
end;

constructor TDirectionScheduleViewFrame.Create(TheOwner: TComponent);
begin
    EntityRecList := TEntityRecList.Create();
    SchLists := TSchListList.Create();

    VTE_NoFakeBG := False;
    VTE_AlignFakeRight := False;
    VTS_OwnsData := False;
    VTS_UseRouteColors := True;
    VTS_DrawTrainName := True;

    inherited Create(TheOwner);
end;

destructor TDirectionScheduleViewFrame.Destroy();
begin
    FreeAndNil(EntityRecList);
    FreeAndNil(SchLists);
    inherited Destroy();
end;

end.

