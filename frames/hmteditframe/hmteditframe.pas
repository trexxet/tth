unit HMTEditFrame;

{$mode ObjFPC}{$H+}

interface

uses
    Classes, SysUtils, Forms, Controls, StdCtrls, Buttons, SpinEx,
    USTime;

type

    { THMTEditFrame }

    THMTEditFrame = class(TFrame)
        ResetBtn: TBitBtn;
        Icons: TImageList;
        LabelSeparator: TLabel;
        HSpinEditEx: TSpinEditEx;
        MSpinEditEx: TSpinEditEx;
        procedure SpinEditExKeyDown(Sender: TObject; var Key: Word;
            Shift: TShiftState);
        procedure ResetBtnClick(Sender: TObject);
    public
        procedure AssignData(const _Value: THMTime);
        function RetrieveData(): THMTime;
    end;

implementation

uses LCLType;

{$R *.lfm}

{ THMTEditFrame }

procedure THMTEditFrame.ResetBtnClick(Sender: TObject);
begin
    HSpinEditEx.Value := 0;
    MSpinEditEx.Value := 0;
end;

procedure THMTEditFrame.SpinEditExKeyDown(Sender: TObject; var Key: Word;
    Shift: TShiftState);
begin
    // Do not handle Escape and Enter
    if (Key = VK_ESCAPE) or (Key = VK_RETURN) then
        if Assigned(Parent.OnKeyDown) then
            Parent.OnKeyDown(Sender, Key, Shift);
end;

procedure THMTEditFrame.AssignData(const _Value: THMTime);
begin
    HSpinEditEx.Value := _Value.H;
    MSpinEditEx.Value := _Value.M;
end;

function THMTEditFrame.RetrieveData(): THMTime;
begin
    Result := HMT(HSpinEditEx.Value, MSpinEditEx.Value);
end;

end.

