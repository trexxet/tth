unit EntityListView;

{$mode ObjFPC}{$H+}
{$modeswitch advancedrecords}

interface

uses
    Classes, SysUtils, Forms, Graphics, Controls, StdCtrls, Generics.Collections,
    UEntity, UID;

type

    { TEntityViewListItem }

    TEntityViewListItem = record
        Entity: TEntity;
        HasColor: Boolean;
        class function Make(_Entity: TEntity; _HasColor: Boolean):
            TEntityViewListItem; static;
    end;

    TEntityViewList = specialize TList<TEntityViewListItem>;

    { TEntityListViewFrame }
    TEntityListViewFrame = class(TFrame)
        ListBox: TListBox;
        procedure ListBoxDrawItem(Control: TWinControl; Index: Integer;
            ARect: TRect; State: TOwnerDrawState);
        procedure ListBoxShowHint(Sender: TObject; HintInfo: PHintInfo);
        procedure ListBoxKeyDown(Sender: TObject; var Key: Word;
            Shift: TShiftState);
        procedure ListBoxDblClick(Sender: TObject);
        procedure ListBoxMouseMove(Sender: TObject; Shift: TShiftState; X,
            Y: Integer);
        procedure ListBoxMouseLeave(Sender: TObject);
    public
        type TFillViewList = procedure(ID: TID;
                                       var ViewList: TEntityViewList) of object;
        type TViewListGoto = procedure(const Entity: TEntity) of object;
    protected
        OwnerID: TID;
        ViewList: TEntityViewList;
        FillViewList: TFillViewList;
        ViewListGoto: TViewListGoto;
        function DoFillViewList(): Boolean; virtual;
    protected
        IndexHovered: Integer;
    public
        procedure SetFocus(); override;
    public
        procedure AssignData(_OwnerID: TID);
        procedure Reassign();

        constructor Create(TheOwner: TComponent; FillViewListAction: TFillViewList;
            ViewListGotoAction: TViewListGoto); reintroduce;
        destructor Destroy(); override;
    end;

implementation

uses LCLType, TListControlViewUtils;

{$R *.lfm}

{ TEntityViewListItem }

class function TEntityViewListItem.Make(_Entity: TEntity; _HasColor: Boolean):
    TEntityViewListItem;
begin
    Result.Entity := _Entity;
    Result.HasColor := _HasColor;
end;

{ TEntityListViewFrame }

procedure TEntityListViewFrame.ListBoxDrawItem(Control: TWinControl;
    Index: Integer; ARect: TRect; State: TOwnerDrawState);
var BG, FG: TColor;
begin
    with ViewList[Index] do
    begin
        if HasColor then
            with Entity as TEntityColored do GetColor(BG, FG);
        DrawItemWithColorMark(ListBox, Index, ARect, State,
                              HasColor, BG, FG, IndexHovered);
    end;
end;

procedure TEntityListViewFrame.ListBoxShowHint(Sender: TObject;
    HintInfo: PHintInfo);
var Index: Integer;
begin
    Index := ListBox.ItemAtPos(HintInfo^.CursorPos, True);
    if Index = -1 then exit;

    GetLongItemHint(ListBox, Index, HintInfo, ViewList[Index].HasColor);
end;

procedure TEntityListViewFrame.ListBoxKeyDown(Sender: TObject; var Key: Word;
    Shift: TShiftState);
begin
    if Key = VK_RETURN then ListBoxDblClick(Sender);
end;

procedure TEntityListViewFrame.ListBoxDblClick(Sender: TObject);
var
    Index: Integer;
    Entity: TEntity;
begin
    Index := ListBox.ItemIndex;
    if (Index = -1) or (ViewListGoto = nil) then exit;

    Entity := ViewList[Index].Entity;
    if Entity.ID > 0 then ViewListGoto(Entity);
end;

procedure TEntityListViewFrame.ListBoxMouseMove(Sender: TObject;
    Shift: TShiftState; X, Y: Integer);
var IndexHoveredSave: Integer;
begin
    IndexHoveredSave := IndexHovered;
    IndexHovered := ListBox.ItemAtPos(Point(X, Y), True);
    if (IndexHovered >= 0) and (ViewList[IndexHovered].Entity.ID = 0) then
        IndexHovered := -1;
    if IndexHovered <> IndexHoveredSave then
        ListBox.Invalidate();
end;

procedure TEntityListViewFrame.ListBoxMouseLeave(Sender: TObject);
begin
    IndexHovered := -1;
    ListBox.Invalidate();
end;

function TEntityListViewFrame.DoFillViewList(): Boolean;
begin
    Result := FillViewList <> nil;
    if Result then FillViewList(OwnerID, ViewList);
end;

procedure TEntityListViewFrame.SetFocus();
begin
    inherited SetFocus();
    if (ListBox.ItemIndex = -1) and (ListBox.Items.Count > 0) then
        ListBox.ItemIndex := 0;
    ListBox.SetFocus();
end;

procedure TEntityListViewFrame.AssignData(_OwnerID: TID);
var Item: TEntityViewListItem;
begin
    OwnerID := _OwnerID;
    if OwnerID = 0 then exit;
    DoFillViewList();

    ListBox.Items.BeginUpdate();
    ListBox.Items.Clear();
    for Item in ViewList do
        Listbox.Items.Add(Item.Entity.Name);
    ListBox.Items.EndUpdate();
end;

procedure TEntityListViewFrame.Reassign();
begin
    if OwnerID > 0 then
        AssignData(OwnerID);
end;

constructor TEntityListViewFrame.Create(TheOwner: TComponent;
    FillViewListAction: TFillViewList; ViewListGotoAction: TViewListGoto);
begin
    inherited Create(TheOwner);
    OwnerID := 0;
    ViewList := TEntityViewList.Create();
    FillViewList := FillViewListAction;
    ViewListGoto := ViewListGotoAction;
    IndexHovered := -1;
end;

destructor TEntityListViewFrame.Destroy();
begin
    FreeAndNil(ViewList);
    inherited Destroy();
end;

end.

