unit ScheduleEditor_ColumnTypes;

{$mode ObjFPC}{$H+}

interface

uses
    SysUtils, Graphics, laz.VirtualTrees, VTUtils, UStopSchedule;

type

    TScheduleColumnType = (colIndicator, colStation, colArrival, colStopping,
                           colDeparture, colTravel, colTotal, colNonstop);

    operator:=(I: TColumnIndex) O: TScheduleColumnType;
    operator:=(I: TScheduleColumnType) O: TColumnIndex;
    operator=(L: TColumnIndex; R: TScheduleColumnType): Boolean;
    operator<(L: TColumnIndex; R: TScheduleColumnType): Boolean;
    operator>(L: TColumnIndex; R: TScheduleColumnType): Boolean;
    operator:=(I: TScheduleColumnType) O: TSchType;

const
    StopColumns = [colArrival..colTravel];
    TimeColumns = StopColumns + [colTotal];
    RouteDefaultsTimeColumns = [colStopping, colTravel];
    RouteDefaultsColumns = RouteDefaultsTimeColumns + [colNonstop];
    DataColumns = [colStation..colNonStop];

const
    sch_clTotal      = sch_clEntityName;
    sch_clGhostLoop  = TColor($DFDFDF);
    sch_clGStation   = TColor($E3DBDB);
    sch_clGTotal     = TColor($E3DBDB);
    sch_clGLStation  = TColor($E3D4D4);
    sch_clGLTotal    = TColor($E3D4D4);
    sch_clFTotal     = TColor($444444);

implementation

operator:=(I: TColumnIndex) O: TScheduleColumnType;
begin
    O := TScheduleColumnType(I);
end;

operator:=(I: TScheduleColumnType) O: TColumnIndex;
begin
    O := ord(I);
end;

operator=(L: TColumnIndex; R: TScheduleColumnType): Boolean;
begin
    Result := L = ord(R);
end;

operator<(L: TColumnIndex; R: TScheduleColumnType): Boolean;
begin
    Result := L < ord(R);
end;

operator>(L: TColumnIndex; R: TScheduleColumnType): Boolean;
begin
    Result := L > ord(R);
end;

operator:=(I: TScheduleColumnType) O: TSchType;
begin
    O := TSchType(ord(I) - ord(colArrival));
end;

end.

