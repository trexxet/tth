unit ScheduleEditor;

{$mode ObjFPC}{$H+}

interface

uses
    Classes, SysUtils, Forms, Controls, ExtCtrls, StdCtrls, Buttons, ECEditBtns,
    { Requires patched VT to show VDT text hints using vhkText }
    laz.VirtualTrees, ListControl,
    UScheduleRecord, URoute, UTrain, UStopSchedule, USTime, UID,
    ScheduleEditor_ColumnTypes;

type

    { TScheduleEditorFrame }
    TScheduleEditorFrame = class(TFrame)
        Icons: TImageList;
        HintLabel: TLabel;
        {%REGION VT} published
        VT: TLazVirtualDrawTree;
        procedure VTInitNode(Sender: TBaseVirtualTree; ParentNode,
            Node: PVirtualNode; var InitialStates: TVirtualNodeInitStates);
        procedure VTDrawNode(Sender: TBaseVirtualTree;
            const PaintInfo: TVTPaintInfo);
        procedure VTGetHintKind(Sender: TBaseVirtualTree; Node: PVirtualNode;
            Column: TColumnIndex; var Kind: TVTHintKind);
        procedure VTGetHint(Sender: TBaseVirtualTree; Node: PVirtualNode;
            Column: TColumnIndex; var LineBreakStyle: TVTTooltipLineBreakStyle;
            var HintText: String);
        procedure VTResetNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
        procedure VTFreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
        procedure VTKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
        procedure VTNodeDblClick(Sender: TBaseVirtualTree;
            const HitInfo: THitInfo);
        procedure VTEnter(Sender: TObject);
        procedure VTFocusChanged(Sender: TBaseVirtualTree; Node: PVirtualNode;
            Column: TColumnIndex);
        procedure VTExit(Sender: TObject);
        {%ENDREGION}
        {%REGION Route Tool Containers} published
        procedure ToolContainerMouseEnter(Sender: TObject);
        procedure ToolContainerMouseLeave(Sender: TObject);
        {%ENDREGION}
        {%REGION StopTool} published
        StopOptionsPanel: TPanel;
        StopAutoCalcToggleBox: TECBitBtn;
        procedure StopAutoCalcToggleBoxChange(Sender: TObject);
        {%ENDREGION}
        {%REGION RouteDefault} published
        RouteDefaultPanel: TPanel;
        RouteDefaultUseToggleBox: TECBitBtn;
        RouteDefaultSetBtn: TBitBtn;
        procedure RouteDefaultUseToggleBoxChange(Sender: TObject);
        procedure RouteDefaultSetBtnClick(Sender: TObject);
        {%ENDREGION}
        {%REGION CalcStopTrav } published
        CalcStopTravPanel: TPanel;
        CalcStopTravCalcBtn: TBitBtn;
        CalcStopTravSetRDBtn: TBitBtn;
        CalcStopTravUnsetRDBtn: TBitBtn;
        procedure CalcStopTravCalcBtnClick(Sender: TObject);
        procedure CalcStopTravDoSetAsRD(UseRD: Boolean);
        procedure CalcStopTravSetRDBtnClick(Sender: TObject);
        procedure CalcStopTravUnsetRDBtnClick(Sender: TObject);
        {%ENDREGION}
        {%REGION CalcArrDep } published
        CalcArrDepBox: TPanel;
        CalcArrDepCalcBtn: TBitBtn;
        procedure CalcArrDepCalcBtnClick(Sender: TObject);
        {%ENDREGION}
        {%REGION TotalTime} published
        TotalTimePanel: TPanel;
        TotalTimeTextLabel: TLabel;
        TotalTimeLabel: TLabel;
        TotalTimeOffLabel: TLabel;
        {%ENDREGION}
        {%REGION AutoRecalc} published
        AutoRecalcCheckbox: TCheckBox;
        procedure AutoRecalcCheckboxChange(Sender: TObject);
        {%ENDREGION}
    private
        Route: TRoute;
        Train: TTrain;
        TotalTime: TTotalTime;
        CanToggleAC: TCanToggleAC;
        CanCalcArrDep: TCanCalcArrDep;
        IsGameTime: Boolean;
        function IfGameTime(Value: Boolean): Boolean; inline;
    private
        procedure SetHintLabelText(Control: TControl);
        procedure SetOfftimeLabelText();
        procedure SwitchStopOptionsPanel(Data: PSchRec;
            SchColumn: TScheduleColumnType);
        procedure SwitchRouteDefaultPanel(Data: PSchRec;
            SchColumn: TScheduleColumnType);
        procedure DoEditSchedule(Data: PSchRec; SchColumn: TScheduleColumnType);
        procedure DoToggleNS(Data: PSchRec);
        procedure DoOnChange();
        procedure DoSyncView();
        procedure DoUpdateTotalTime();
        procedure DoSetAsRouteDefault(StopIndex: SizeUInt); inline;
    public
        procedure AssignData(RouteID, TrainID: TID);
        procedure Reassign(); inline;
        procedure TimeModeChanged(_IsGameTime: Boolean);
    public
        // How the fuck VT does not have these methods?
        procedure InsertNodeAt(Index: SizeUInt);
        procedure DeleteNodeAt(Index: SizeUInt);
        procedure MoveNodeAt(OldIndex, NewIndex: SizeUInt);
        function ResetNode(Node: PVirtualNode): Boolean;
        function ResetNodeAt(Index: SizeUInt): Boolean;
    public
        SyncFocus: TLCAction;
        SyncView: TNotifyEvent;
        OnSetAsRouteDefault: TLCAction;
    public
        constructor Create(TheOwner: TComponent); override;
        destructor Destroy(); override;
    end;

implementation

uses
    Math, Graphics, LCLIntf, LCLType,
    TButtonControlUtils, VTUtils,
    Main, HMTSetter, UTimetable, ULogger,
    ScheduleEditor_RS;

procedure SetFontColor(const Ed: TScheduleEditorFrame; const Node: PVirtualNode;
    SchColumn: TScheduleColumnType; CellCanvas: TCanvas); inline;
begin
    with Ed.VT, CellCanvas.Font do
    begin
        if (SchColumn = TScheduleColumnType(FocusedColumn)) and (Node = FocusedNode) then
            Color := clHighlightText
        else if (SchColumn = colTotal) then
            Color := sch_clFTotal
        else
            Color := clWindowText;
    end;
end;

procedure FillBG(const Ed: TScheduleEditorFrame; const Node: PVirtualNode;
    const Data: PSchRec; SchColumn: TScheduleColumnType; CellCanvas: TCanvas;
    CellRect: TRect); inline;
begin
    with Data^.StopSchedule, CellCanvas.Brush do
    begin
        if (SchColumn in DataColumns) and IsGhost then
        begin
            if Node^.Index mod Ed.Route.Stops.Count = 0 then
            begin
                case SchColumn of
                    colStation: Color := sch_clGLStation;
                    colTotal:   Color := sch_clGLTotal;
                    else        Color := sch_clGhostLoop
                end;
            end else
            begin
                case SchColumn of
                    colStation: Color := sch_clGStation;
                    colTotal:   Color := sch_clGTotal;
                    else        Color := sch_clGhost;
                end;
            end;
        end
        else if SchColumn = colStation then
            Color := sch_clEntityName
        else if SchColumn = colTotal then
            Color := sch_clTotal
        else if (SchColumn in StopColumns) and AutoCalcInvalid[SchColumn] then
            Color := sch_clInvalid
        else if (SchColumn in RouteDefaultsTimeColumns) and IsRouteTime[SchColumn] then
            Color := sch_clRouteTime
        else if (SchColumn = colNonstop) and UseRouteNS then
            Color := sch_clRouteTime
        else if IsUneditable(Data, SchColumn) then
            Color := sch_clUneditable
        else exit;
    end;
    CellCanvas.FillRect(CellRect);
end;

{$R *.lfm}

{ TScheduleEditorFrame }

procedure TScheduleEditorFrame.VTInitNode(Sender: TBaseVirtualTree; ParentNode,
    Node: PVirtualNode; var InitialStates: TVirtualNodeInitStates);
var
    Data: PSchRec;
    Sch: TStopSchedule;
begin
    if (Node = nil) or (Route = nil) or (Train = nil) then exit;
    Sch := Train.ScheduleAt(Node^.Index);
    if Sch = nil then exit;

    Data := VT.GetNodeData(Node);
    if Data = nil then exit;
    Data^.Fill(Sch);
end;

procedure TScheduleEditorFrame.VTDrawNode(Sender: TBaseVirtualTree;
    const PaintInfo: TVTPaintInfo);
var
    Data: PSchRec;
    PI: TVTPaintInfo;
    SchColumn: TScheduleColumnType;
    DrawS: String;    // String to be drawn (Data^.S[])
    BitmapX, BitmapY, BitmapW, BitmapH: Integer; // Used to center non-stop icon
begin
    PI := PaintInfo;  // ObjFPC can't put const arg into with..do
    with VT, PI do
    begin
        Data := GetNodeData(Node);
        if not CheckData(Data) then exit;
        SchColumn := TScheduleColumnType(Column);

        SetBKMode(Canvas.Handle, TRANSPARENT);
        SetFontColor(Self, Node, SchColumn, Canvas);
        if (Column <> FocusedColumn) or (Node <> FocusedNode) then
            FillBG(Self, Node, Data, SchColumn, Canvas, CellRect);

        // Draw the contents
        case SchColumn of
        colIndicator: DrawIndicator(Canvas, CellRect, Icons, Node = FocusedNode);
        colStation: DrawCaption(Canvas, ContentRect, Data^.S[Column], False);
        colArrival..colTotal:
            begin
                DrawS := Data^.S[Column];
                Canvas.Font.Height := 32;
                // Sacrifice TextMargins and use CellRect
                DrawText(Canvas.Handle, PChar(DrawS), Length(DrawS), CellRect,
                         DT_CENTER or DT_VCENTER or DT_SINGLELINE);
            end;
        colNonstop:
            begin
                with Data^.NonStopIcon do
                begin
                    BitmapX := ContentRect.Left +
                               (ContentRect.Width - Width) div 2;
                    BitmapY := ContentRect.Top +
                               (ContentRect.Height - Height) div 2;
                    BitmapW := Width;
                    BitmapH := Height;
                end;
                BitBlt(Canvas.Handle, BitmapX, BitmapY, BitmapW, BitmapH,
                       Data^.NonStopIcon.Canvas.Handle, 0, 0, SRCCOPY);
            end;
        end;
    end;
end;

procedure TScheduleEditorFrame.VTGetHintKind(Sender: TBaseVirtualTree;
    Node: PVirtualNode; Column: TColumnIndex; var Kind: TVTHintKind);
begin
    case TScheduleColumnType(Column) of
        colStation, colNonStop: Kind := vhkText;
    end;
end;

procedure TScheduleEditorFrame.VTGetHint(Sender: TBaseVirtualTree;
    Node: PVirtualNode; Column: TColumnIndex;
    var LineBreakStyle: TVTTooltipLineBreakStyle; var HintText: String);
var Data: PSchRec;
begin
    Data := VT.GetNodeData(Node);
    if not CheckData(Data) then exit;
    HintText := '';

    case TScheduleColumnType(Column) of
        colStation: HintText := Data^.S[colStation];
        colNonStop: HintText := Data^.NonStopIcon.Hint;
    end;
end;

procedure TScheduleEditorFrame.VTResetNode(Sender: TBaseVirtualTree;
    Node: PVirtualNode);
begin
    VTFreeNode(VT, Node);
end;

procedure TScheduleEditorFrame.VTFreeNode(Sender: TBaseVirtualTree;
    Node: PVirtualNode);
var Data: PSchRec;
begin
    Data := VT.GetNodeData(Node);
    if Data = nil then exit;
    FreeAndNil(Data^.NonStopIcon);
    Finalize(Data^);
end;

procedure TScheduleEditorFrame.VTKeyDown(Sender: TObject; var Key: Word;
    Shift: TShiftState);
var HitInfo: THitInfo;
begin
    if Key <> VK_RETURN then exit;
    with VT do
    begin
        if FocusedNode = nil then exit;
        HitInfo.HitNode := FocusedNode;
        HitInfo.HitColumn := FocusedColumn;
    end;
    VTNodeDblClick(VT, HitInfo);
end;

procedure TScheduleEditorFrame.VTNodeDblClick(Sender: TBaseVirtualTree;
    const HitInfo: THitInfo);
var
    SchColumn: TScheduleColumnType;
    Data: PSchRec;
begin
    if not IsGameTime then exit;
    Data := VT.GetNodeData(HitInfo.HitNode);
    if not CheckData(Data) then exit;
    SchColumn := TScheduleColumnType(HitInfo.HitColumn);
    if IsUneditable(Data, SchColumn) or (SchColumn = colIndicator) then exit;

    MainForm.FileChanged();
    if SchColumn in TimeColumns then
        DoEditSchedule(Data, SchColumn);
    if SchColumn = colNonstop then
        DoToggleNS(Data);

    ResetNode(HitInfo.HitNode);
end;

procedure TScheduleEditorFrame.VTEnter(Sender: TObject);
begin
    if (VT.FocusedNode = nil) or (TScheduleColumnType(VT.FocusedColumn) < colStation) then
        if VT.RootNodeCount > 0 then begin
            VT.FocusedNode := NodeAt(VT, 0);
            VT.FocusedColumn := colStation;
            VT.Selected[VT.FocusedNode] := True;
        end;
    VTFocusChanged(VT, VT.FocusedNode, VT.FocusedColumn);
end;

procedure TScheduleEditorFrame.VTFocusChanged(Sender: TBaseVirtualTree;
    Node: PVirtualNode; Column: TColumnIndex);
var
    Data: PSchRec;
    SchColumn: TScheduleColumnType;
begin
    Data := VT.GetNodeData(Node);
    if (not CheckData(Data)) or (Data^.StopSchedule.IsGhost) then
    begin
        StopAutoCalcToggleBox.Enabled := False;
        RouteDefaultUseToggleBox.Enabled := False;
        RouteDefaultUseToggleBox.SetCheckedSilent(False);
        RouteDefaultSetBtn.Enabled := False;
        exit;
    end;
    SchColumn := TScheduleColumnType(Column);

    SwitchRouteDefaultPanel(Data, SchColumn);
    SwitchStopOptionsPanel(Data, SchColumn);
end;

procedure TScheduleEditorFrame.VTExit(Sender: TObject);
var
    Data: PSchRec;
    ChildControl: TControl;
begin
    // Do not reset focused item if some schedule tool takes focus
    for ChildControl in StopOptionsPanel.GetEnumeratorControls do
        if ChildControl = MainForm.ActiveControl then exit;
    for ChildControl in RouteDefaultPanel.GetEnumeratorControls do
        if ChildControl = MainForm.ActiveControl then exit;
    for ChildControl in CalcStopTravPanel.GetEnumeratorControls do
        if ChildControl = MainForm.ActiveControl then exit;
    for ChildControl in CalcArrDepBox.GetEnumeratorControls do
        if ChildControl = MainForm.ActiveControl then exit;
    for ChildControl in TotalTimePanel.GetEnumeratorControls do
        if ChildControl = MainForm.ActiveControl then exit;

    // Sync focused node to selected stop in StopsLC
    if (VT.FocusedNode <> nil) and (SyncFocus <> nil) then
    begin
        Data := VT.GetNodeData(VT.FocusedNode);
        if not CheckData(Data) then exit;
        SyncFocus(Data^.StopSchedule.StopIndex);
    end;

    VTFocusChanged(VT, nil, 0);
end;

procedure TScheduleEditorFrame.ToolContainerMouseEnter(Sender: TObject);
begin
    SetHintLabelText(Sender as TControl);
end;

procedure TScheduleEditorFrame.ToolContainerMouseLeave(Sender: TObject);
var MousePos: TPoint;
begin
    with Sender as TControl do
    begin
        MousePos := ScreenToClient(Mouse.CursorPos);
        if ClientRect.Contains(MousePos) then exit;
    end;
    SetHintLabelText(nil);
end;

procedure TScheduleEditorFrame.CalcStopTravCalcBtnClick(Sender: TObject);
begin
    MainForm.FileChanged();
    Train.ScheduleStopTrav_DoAutoCalc();
    ResetAllNodes(VT);
    DoOnChange();
    VT.SetFocus();
end;

procedure TScheduleEditorFrame.CalcStopTravDoSetAsRD(UseRD: Boolean);
var
    Node: PVirtualNode;
    Data: PSchRec;
label fin;
begin
    MainForm.FileChanged();
    Logger.PushSeverity(LERROR);
    Node := VT.GetFirst();

    while Node <> nil do
    begin
        Data := VT.GetNodeData(Node);
        if not CheckData(Data) then goto fin;
        with Data^.StopSchedule, Self.Route do
        begin
            if UseRD then
            begin
                SetStopTime(StopIndex, Stopping, SchTimes[Stopping].HM);
                SetStopTime(StopIndex, Travel, SchTimes[Travel].HM);
                SetNS(StopIndex, NonStop);
                DoSetAsRouteDefault(StopIndex);
            end;
            IsRouteTime[Stopping] := UseRD;
            IsRouteTime[Travel] := UseRD;
            UseRouteNS := UseRD;
        end;
        Node := VT.GetNext(Node);
    end;

    ResetAllNodes(VT);
    DoOnChange();
    VT.SetFocus();
    fin: Logger.PopSeverity();
end;

procedure TScheduleEditorFrame.CalcStopTravSetRDBtnClick(Sender: TObject);
begin
    CalcStopTravDoSetAsRD(True);
end;

procedure TScheduleEditorFrame.CalcStopTravUnsetRDBtnClick(Sender: TObject);
begin
    CalcStopTravDoSetAsRD(False);
end;

procedure TScheduleEditorFrame.CalcArrDepCalcBtnClick(Sender: TObject);
begin
    MainForm.FileChanged();
    Train.ScheduleArrDep_DoAutoCalc();
    Train.MakeGhosts(TotalTime.LoopCount);
    // Nuke the VT because RootNodeCount changed, but nodes shouldn't be reset
    Reassign();
    VT.SetFocus();
end;

procedure TScheduleEditorFrame.AutoRecalcCheckboxChange(Sender: TObject);
begin
    MainForm.FileChanged();
    if Train <> nil then
    begin
        Train.AutoRecalc := AutoRecalcCheckbox.Checked;
        if Train.AutoRecalc then
        begin
            Train.RecalculateAll();
            ResetAllNodes(VT);
        end;
    end;
    VT.SetFocus();
    DoSyncView();
end;

function TScheduleEditorFrame.IfGameTime(Value: Boolean): Boolean;
begin
    Result := IsGameTime and Value;
end;

procedure TScheduleEditorFrame.SetHintLabelText(Control: TControl);
var
    LabelCaption: String = '';
    SchColumn: TScheduleColumnType;
begin
    if Control = nil then
    begin
        HintLabel.Caption := LabelCaption;
        exit;
    end;

    // I can't believe how miserable the Pascal that IT CAN'T TAKE POINTERS FOR THE CASE OF
    if Control = StopOptionsPanel then
    begin
        if CanToggleAC = TCanToggleAC.CTAC_OK then
        begin
            SchColumn := TScheduleColumnType(VT.FocusedColumn);
            case SchColumn of
                colArrival:   LabelCaption := StopAutoCalcRS_Arr;
                colStopping:  LabelCaption := StopAutoCalcRS_Stop;
                colDeparture: LabelCaption := StopAutoCalcRS_Dep;
                colTravel:    LabelCaption := StopAutoCalcRS_Trav;
            end;
        end else
        begin
            case CanToggleAC of
                TCanToggleAC.CTAC_IsFirstArr: LabelCaption := StopAutoCalcRS_FArr;
                TCanToggleAC.CTAC_IsRD:       LabelCaption := StopAutoCalcRS_URD;
            end;
        end;
    end
    else if Control = CalcStopTravPanel then
    begin
        if CalcStopTravCalcBtn.Enabled then LabelCaption := StopTravRS_OK
        else LabelCaption := StopTravRS_NeedNoAC;
    end else
    if Control = CalcArrDepBox then
    begin
        case CanCalcArrDep of
            TCanCalcArrDep.ADCC_OK:          LabelCaption := ArrDepRS_OK;
            TCanCalcArrDep.ADCC_NeedUR:      LabelCaption := ArrDepRS_NeedURD;
            TCanCalcArrDep.ADCC_Need1Loop:   LabelCaption := ArrDepRS_Need1Loop;
            TCanCalcArrDep.ADCC_FirstArrAC:  LabelCaption := ArrDepRS_FirstArrAC;
            TCanCalcArrDep.ADCC_LoopToShort: LabelCaption := ArrDepRS_LoopTooShort;
            TCanCalcArrDep.ADCC_Empty:       LabelCaption := ArrDepRS_Empty;
        end;
    end;

    HintLabel.Caption := LabelCaption;
end;

procedure TScheduleEditorFrame.SetOfftimeLabelText();
var OffTimeSignStr, OffTimeStr: String;
begin
    TotalTimeOffLabel.Font.Color := clRed;
    OffTimeSignStr := '';

    with TotalTime do
    begin
        if OffTime = HMUnused then OffTimeStr := TotalTimeRS_Unknown
        else if OffTime = HM0 then begin
            TotalTimeOffLabel.Font.Color := clGreen;
            if LoopCountInverse then
                OffTimeStr := Format(TotalTimeRS_OK_Inv, [LoopCount])
            else
                OffTimeStr := Format(TotalTimeRS_OK, [LoopCount])
        end else begin
            if OffTimeNegative then OffTimeSignStr := '-';
            with OffTime do
                OffTimeStr := Format(TotalTimeRS_Off,
                                    [OffTimeSignStr, Format('%.2u:%.2u', [H, M])]);
        end;
    end;

    TotalTimeOffLabel.Caption := OffTimeStr;
end;

procedure TScheduleEditorFrame.DoEditSchedule(Data: PSchRec;
    SchColumn: TScheduleColumnType);
var NewHM: THMTime;
begin
    Logger.PushSeverity(LERROR);
    with Data^, Data^.StopSchedule do
    begin
        NewHM := HMTSetterForm.Edit(StopSchedule[SchColumn].HM);
        if IsRouteTime[SchColumn] then
        begin
            Self.Route.SetStopTime(StopIndex, SchColumn, NewHM);
            DoSetAsRouteDefault(StopIndex);
        end
        else
            StopSchedule[SchColumn] := TSchTime.FromHMT(NewHM);
    end;
    Logger.PopSeverity();
end;

procedure TScheduleEditorFrame.DoToggleNS(Data: PSchRec);
begin
    with Data^.StopSchedule, Data^.NonStopIcon do
        if UseRouteNS then
        begin
            Timetable.Routes[Route].SetNS(StopIndex, not NS);
            DoSetAsRouteDefault(StopIndex);
        end else
        begin
            Toggle();
            NonStop := NS;
        end;
    if Train.HasGhosts() then
        Train.MakeGhosts(TotalTime.LoopCount);
end;

procedure TScheduleEditorFrame.DoOnChange();
begin
    if Train = nil then exit;
    DoUpdateTotalTime();

    CalcStopTravCalcBtn.Enabled := IfGameTime(Train.ScheduleStopTrav_CanAutoCalc());

    CanCalcArrDep := Train.ScheduleArrDep_CanAutoCalc(TotalTime);
    CalcArrDepCalcBtn.Enabled := IfGameTime(CanCalcArrDep = TCanCalcArrDep.ADCC_OK);

    DoSyncView();
end;

procedure TScheduleEditorFrame.DoSyncView();
begin
    if SyncView <> nil then SyncView(Self);
end;

procedure TScheduleEditorFrame.DoUpdateTotalTime();
begin
    Train.GetTotalTime(TotalTime);
    if Train.NeedGhostReset then Reassign();
    TotalTimeLabel.Caption := TotalTime.SchTime.ToStr();
    SetOfftimeLabelText();
end;

procedure TScheduleEditorFrame.DoSetAsRouteDefault(StopIndex: SizeUInt);
begin
    if (OnSetAsRouteDefault <> nil) then OnSetAsRouteDefault(StopIndex);
end;

procedure TScheduleEditorFrame.AssignData(RouteID, TrainID: TID);
begin
    VT.Clear();
    if (RouteID > 0) and (TrainID > 0) then
    begin
        Route := Timetable.Routes[RouteID];
        Train := Route.Trains[TrainID];
        VT.RootNodeCount := Train.CountWithGhosts();
        AutoRecalcCheckbox.SetCheckedSilent(Train.AutoRecalc);
        DoOnChange();
    end else
        Train := nil;
end;

procedure TScheduleEditorFrame.Reassign();
begin
    AssignData(Route.ID, Train.ID);
    VTFocusChanged(VT, nil, 0);
end;

procedure TScheduleEditorFrame.TimeModeChanged(_IsGameTime: Boolean);
begin
    IsGameTime := _IsGameTime;
    AutoRecalcCheckbox.Enabled := IfGameTime(True);
    CalcStopTravSetRDBtn.Enabled := IfGameTime(True);
    CalcStopTravUnsetRDBtn.Enabled := IfGameTime(True);
    VTEnter(Self);
    DoOnChange();
    ResetAllNodes(VT);
end;

procedure TScheduleEditorFrame.InsertNodeAt(Index: SizeUInt);
begin
    if (Train <> nil) and Train.NeedGhostReset then
        Reassign()
    else begin
        VT.InsertNode(NodeAt(VT, Index), amInsertAfter);
        VT.ResetNode(NodeAt(VT, Index));
        DoOnChange();
    end;
end;

procedure TScheduleEditorFrame.DeleteNodeAt(Index: SizeUInt);
begin
    if (Train <> nil) and Train.NeedGhostReset then
        Reassign()
    else begin
        VT.DeleteNode(NodeAt(VT, Index));
        DoOnChange();
    end;
end;

procedure TScheduleEditorFrame.MoveNodeAt(OldIndex, NewIndex: SizeUInt);
var
    Index, FocusedIndex: SizeUInt;
    FocusedNode: PVirtualNode;
begin
    // Check if focused node was ghost schedule
    FocusedNode := VT.FocusedNode;
    if ((FocusedNode <> nil) and (FocusedNode^.Index >= Route.Stops.Count))
        or ((Train <> nil) and Train.NeedGhostReset) then
    begin
        Reassign();
        exit;
    end;

    for Index := Min(OldIndex, NewIndex) to Max(OldIndex, NewIndex) do
        ResetNodeAt(Index);

    // Adjust focus
    FocusedNode := VT.FocusedNode;
    if FocusedNode = nil then exit;
    // If the focused node is moved, then move the focus as well
    if FocusedNode = NodeAt(VT, OldIndex) then
        VT.FocusedNode := NodeAt(VT, NewIndex)
    else begin
        // If the focused node is shifted by the moved node, shift the focus as well
        FocusedIndex := FocusedNode^.Index;
        // Moved node down - shift focus up
        if (FocusedIndex > OldIndex) and (FocusedIndex <= NewIndex) then
            VT.FocusedNode := VT.GetPrevious(FocusedNode)
        // Moved node up - shift focus down
        else if (FocusedIndex >= NewIndex) and (FocusedIndex < OldIndex) then
            VT.FocusedNode := VT.GetNext(FocusedNode);
    end;

    // Another dumb shit VT should do on it's own
    VT.Selected[FocusedNode] := False;
    VT.Selected[VT.FocusedNode] := True;

    DoOnChange();
end;

function TScheduleEditorFrame.ResetNode(Node: PVirtualNode): Boolean;
var Data: PSchRec;
begin
    Result := False;
    Data := VT.GetNodeData(Node);
    if not CheckData(Data) then exit;

    if (Train <> nil) and Train.NeedGhostReset then
        Reassign()
    else if Data^.StopSchedule.NeighborsInvalid then
        VT.Invalidate()
    else if Data^.StopSchedule.NeighborsRecalculated then
        ResetAllNodes(VT);
    VT.ResetNode(Node);

    DoOnChange();

    Result := True;
end;

function TScheduleEditorFrame.ResetNodeAt(Index: SizeUInt): Boolean;
begin
    Result := ResetNode(NodeAt(VT, Index));
end;

procedure TScheduleEditorFrame.StopAutoCalcToggleBoxChange(Sender: TObject);
var
    Data: PSchRec;
    SchColumn: TScheduleColumnType;
begin
    MainForm.FileChanged();
    if not (VT.FocusedColumn in StopColumns) then exit;
    Data := VT.GetNodeData(VT.FocusedNode);
    if not CheckData(Data) then exit;
    SchColumn := TScheduleColumnType(VT.FocusedColumn);

    with Data^.StopSchedule do
        AutoCalculate[SchColumn] := StopAutoCalcToggleBox.Checked;

    ResetNode(VT.FocusedNode);
    VT.SetFocus();
end;

procedure TScheduleEditorFrame.RouteDefaultUseToggleBoxChange(
    Sender: TObject);
var
    Data: PSchRec;
    SchColumn: TScheduleColumnType;
begin
    MainForm.FileChanged();
    if not (VT.FocusedColumn in RouteDefaultsColumns) then exit;
    Data := VT.GetNodeData(VT.FocusedNode);
    if not CheckData(Data) then exit;
    SchColumn := TScheduleColumnType(VT.FocusedColumn);

    with Data^.StopSchedule do
        case SchColumn of
            colStopping, colTravel:
                IsRouteTime[SchColumn] := RouteDefaultUseToggleBox.Checked;
            colNonstop:
                UseRouteNS := RouteDefaultUseToggleBox.Checked;
        end;

    ResetNode(VT.FocusedNode);
    SwitchRouteDefaultPanel(Data, SchColumn);
    SwitchStopOptionsPanel(Data, SchColumn);
    VT.SetFocus();
end;

procedure TScheduleEditorFrame.RouteDefaultSetBtnClick(Sender: TObject);
var
    Data: PSchRec;
    SchColumn: TScheduleColumnType;
begin
    MainForm.FileChanged();
    if not (VT.FocusedColumn in RouteDefaultsColumns) then exit;
    Data := VT.GetNodeData(VT.FocusedNode);
    if not CheckData(Data) then exit;
    SchColumn := TScheduleColumnType(VT.FocusedColumn);

    with Data^.StopSchedule, Self.Route do
    begin
        case SchColumn of
            colStopping, colTravel:
                SetStopTime(StopIndex, SchColumn, SchTimes[SchColumn].HM);
            colNonstop:
                SetNS(StopIndex, NonStop);
        end;
        DoSetAsRouteDefault(StopIndex);
    end;

    RouteDefaultUseToggleBox.Checked := True;
    VT.ResetNode(VT.FocusedNode);
    VT.SetFocus();
end;

procedure TScheduleEditorFrame.SwitchStopOptionsPanel(Data: PSchRec;
    SchColumn: TScheduleColumnType);
var IsAutoCalc: Boolean;
begin
    if not (SchColumn in StopColumns) then
    begin
        StopAutoCalcToggleBox.Enabled := False;
        exit;
    end;

    IsAutoCalc := Route.Stops.Count > 0;

    with Data^.StopSchedule, StopAutoCalcToggleBox do
    begin
        if SchColumn in StopColumns then
        begin
            IsAutoCalc := AutoCalculate[SchColumn];
            CanToggleAC := Self.Train.ScheduleCanToggleAC(
                Data^.StopSchedule, SchColumn);
            Enabled := IfGameTime(CanToggleAC = TCanToggleAC.CTAC_OK);
            IsAutoCalc := AutoCalculate[SchColumn] and Enabled;
        end;
    end;

    StopAutoCalcToggleBox.SetCheckedSilent(IsAutoCalc);
end;

procedure TScheduleEditorFrame.SwitchRouteDefaultPanel(Data: PSchRec;
    SchColumn: TScheduleColumnType);
var UseRouteDefault: Boolean;
begin
    if not (SchColumn in RouteDefaultsColumns) then
    begin
        RouteDefaultUseToggleBox.Enabled := False;
        RouteDefaultUseToggleBox.SetCheckedSilent(False);
        RouteDefaultSetBtn.Enabled := False;
        exit;
    end;

    RouteDefaultUseToggleBox.Enabled := IfGameTime(True);
    UseRouteDefault := False;
    with Data^.StopSchedule do
        case SchColumn of
            colStopping, colTravel: UseRouteDefault := IsRouteTime[SchColumn];
            colNonstop: UseRouteDefault := UseRouteNS;
        end;

    RouteDefaultUseToggleBox.SetCheckedSilent(IfGameTime(UseRouteDefault));
    RouteDefaultSetBtn.Enabled := IfGameTime(not UseRouteDefault);
end;

constructor TScheduleEditorFrame.Create(TheOwner: TComponent);
begin
    inherited Create(TheOwner);
    VT.NodeDataSize := SizeOf(TSchRec);
    TotalTime.SchTime := TSchTime.FromHMT(HM0);
    IsGameTime := True;
end;

destructor TScheduleEditorFrame.Destroy();
begin
    FreeAndNil(TotalTime.SchTime);
    inherited Destroy();
end;

end.

