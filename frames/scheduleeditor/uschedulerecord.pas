unit UScheduleRecord;

{$mode ObjFPC}{$H+}
{$modeswitch advancedrecords}

interface

uses
    Classes, SysUtils, Graphics, laz.VirtualTrees,
    UStopSchedule, ScheduleEditor_ColumnTypes;

type

    { TSchRec }
    TNSIcon = class;
    TSchRec = record
      S: Array[TScheduleColumnType] of String;
      NonStopIcon: TNSIcon;
      StopSchedule: TStopSchedule;
      procedure Fill(Sch: TStopSchedule);
    end;
    PSchRec = ^TSchRec;

    { TNSIcon }
    TNSIcon = class(TIcon)
    private
        const StopImageIndex = 14;
        const NonStopImageIndex = 15;
    private
        OwnerSchRec: PSchRec;
        FNS: Boolean;
        procedure SetNS(const AValue: Boolean);
        function GetHint(): String;
    public
        property NS: Boolean read FNS write SetNS;
        property Hint: String read GetHint;
        procedure Toggle();
        constructor Create(SchRecOwner: PSchRec); reintroduce;
    end;

    function CheckData(const Data: PSchRec): Boolean; inline;
    function IsUneditable(const Data: PSchRec; SchColumn: TScheduleColumnType):
        Boolean; inline;

implementation

uses Main, UTimetable, ScheduleEditor_RS;

function CheckData(const Data: PSchRec): Boolean; inline;
begin
    Result := (Data <> nil)
          and (Data^.StopSchedule <> nil)
          and (Data^.NonStopIcon <> nil);
end;

function IsUneditable(const Data: PSchRec;
    SchColumn: TScheduleColumnType): Boolean; inline;
begin
    with Data^ do
        Result := (SchColumn = colStation) or (SchColumn = colTotal)
              or StopSchedule.IsGhost
              or ((SchColumn in TimeColumns) and StopSchedule[SchColumn].AutoCalculated);
end;

{ TNSIcon }

procedure TNSIcon.SetNS(const AValue: Boolean);
var ImageIndex: Integer;
begin
    FNS := AValue;

    ImageIndex := specialize IfThen<Integer>(FNS,
                                             NonStopImageIndex,
                                             StopImageIndex);
    MainForm.Icons.Resolution[32].GetIcon(ImageIndex, Self);
end;

function TNSIcon.GetHint(): String;
begin
    Result := specialize IfThen<String>(FNS, TNS_RS_NonStopHint, TNS_RS_StopHint);
end;

procedure TNSIcon.Toggle();
begin
    NS := not NS;
end;

constructor TNSIcon.Create(SchRecOwner: PSchRec);
begin
    inherited Create();
    OwnerSchRec := SchRecOwner;
    NS := False;
end;

{ TSchRec }

procedure TSchRec.Fill(Sch: TStopSchedule);
var SchColumn: TScheduleColumnType;
begin
    StopSchedule := Sch;

    S[colStation] := Timetable.Stations[Sch.Station].Name;
    for SchColumn in TimeColumns do
        S[SchColumn] := Sch[SchColumn].ToStr();

    NonStopIcon := TNSIcon.Create(@Self);
    NonStopIcon.NS := Sch.NonStop;
end;

end.

