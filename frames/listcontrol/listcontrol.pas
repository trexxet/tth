unit ListControl;

{$mode ObjFPC}{$H+}
{$warn 5024 off}
{$modeswitch nestedprocvars}

interface

uses
    Classes, SysUtils, LCLType, Forms, Controls,
    Graphics, StdCtrls, ExtCtrls, Buttons,
    ListFilterEdit, Types;

type

    TLCOrientation = (lcoVertical, lcoHorizontal);
    TLCAddMode = (lcaAddRename, lcaCustom);

    TLCAction = function(Index: Integer): Boolean of object;
    TLCActionType = (lcTop, lcUp, lcAdd, lcRename, lcRemove, lcDown, lcBottom,
                     lcOnSelect);

    TLCGetColorAction = procedure(Index: Integer; out BG, FG: TColor) of object;

    { TListControlFrame }
    TListControlFrame = class(TFrame)
    public
        type TLCActions = Array[TLCActionType] of TLCAction;
    private
        type TButtonToggleMode = (lcbNoSel, lcbSelTop, lcbSelMid, lcbSelBottom,
                                  lcbSelSingle, lcbFilterNoSel, lcbFilterSel);
        type PTStringList = ^TStringList;
    private
        ListActions: TLCActions;
        PStringList: PTStringList;
        FGetItemColor: TLCGetColorAction;
    public
        procedure SetLCRefs(_PStringList: PTStringList; _Actions: TLCActions);
        property GetItemColor: TLCGetColorAction read FGetItemColor write FGetItemColor;
        function HasColors(): Boolean; inline;
        function HasItems(): Boolean; inline;
        procedure WaitRefresh();
        constructor Create(TheOwner: TComponent); override;
    published
    {%REGION Controls}
        Glyphs: TImageList;
        ListBox: TListBox;
        ListBoxFilter: TListFilterEdit;
        ListBoxRenamePanel: TPanel;
        ListBoxRenameEdit: TEdit;
        ListBoxRenameAcceptBtn: TSpeedButton;
        ListBoxRenameCancelBtn: TSpeedButton;
        ButtonPanel: TPanel;
        BtnBottom: TBitBtn;
        BtnTop: TBitBtn;
        BtnUp: TBitBtn;
        BtnAdd: TBitBtn;
        BtnRename: TBitBtn;
        BtnRemove: TBitBtn;
        BtnDown: TBitBtn;
    {%ENDREGION}
    published
    {%REGION ListBox}
        procedure ListBoxDrawItem(Control: TWinControl; Index: Integer;
            ARect: TRect; State: TOwnerDrawState);
        procedure ListBoxShowHint(Sender: TObject; HintInfo: PHintInfo);
        procedure ListBoxSelectionChange(Sender: TObject; User: boolean);
        procedure ListBoxFilterAfterFilter(Sender: TObject);
        procedure ListBoxKeyDown(Sender: TObject; var Key: Word;
            Shift: TShiftState);
        procedure ListBoxDblClick(Sender: TObject);
        procedure ListBoxFilterKeyDown(Sender: TObject; var Key: Word;
            Shift: TShiftState);
        procedure ListBoxChangeBounds(Sender: TObject);
    {%ENDREGION}
    {%REGION Item rename}
        procedure EnableItemRename(ItemRect: TRect; ItemText: String);
        procedure DisableItemRename(FocusOnListBox: Boolean = True);
        procedure DoItemRename();
        procedure AcceptItemRename(FocusOnListBox: Boolean = True);
        procedure ListBoxRenameEditExit(Sender: TObject);
        procedure ListBoxRenameEditKeyDown(Sender: TObject; var Key: Word;
            Shift: TShiftState);
        procedure ListBoxRenameAcceptBtnClick(Sender: TObject);
        procedure ListBoxRenameCancelBtnClick(Sender: TObject);
    {%ENDREGION}
    {%REGION Buttons}
        procedure ButtonsToggle();
        procedure BtnTopClick(Sender: TObject);
        procedure BtnUpClick(Sender: TObject);
        procedure BtnAddClick(Sender: TObject);
        procedure BtnRenameClick(Sender: TObject);
        procedure BtnRemoveClick(Sender: TObject);
        procedure BtnDownClick(Sender: TObject);
        procedure BtnBottomClick(Sender: TObject);
    {%ENDREGION}
    private
        FOrientation: TLCOrientation;
        FFilterEnabled: Boolean;
        FButtonPanelEnabled: Boolean;
        FRenameEnabled: Boolean;
        FRenameNewName: String;
        FAddMode: TLCAddMode;
        function getItems(): TStrings;
        procedure setOrientation(AOrientation: TLCOrientation);
        procedure setFilterEnabled(AFilterEnabled: Boolean);
        procedure setButtonPanelEnabled(AButtonPanelEnabled: Boolean);
        procedure setRenameEnabled(ARenameEnabled: Boolean);
        procedure setAddMode(AAddMode: TLCAddMode);
    public
        property Items: TStrings read getItems;
        property Orientation: TLCOrientation
                 read FOrientation
                 write setOrientation
                 default lcoVertical;
        property FilterEnabled: Boolean
                 read FFilterEnabled
                 write setFilterEnabled
                 default True;
        property ButtonPanelEnabled: Boolean
                 read FButtonPanelEnabled
                 write setButtonPanelEnabled
                 default True;
        property RenameEnabled: Boolean
                 read FRenameEnabled
                 write setRenameEnabled
                 default True;
        property RenameNewName: String read FRenameNewName;
        property AddMode: TLCAddMode
                 read FAddMode
                 write setAddMode
                 default lcaAddRename;
    end;

{%REGION Button toggle modes and transitions}
{bNS = btmNoSel       = No item selected, no filter input}
{bST = btmSelTop      = Top item selected, no filter input}
{bSM = btmSelMid      = Middle items selected, no filter input}
{bSB = btmSelBottom   = Bottom item selected, no filter input}
{bSS = btmSelSingle   = The only item selected, no filter input}
{bFN = btmFilterNoSel = Filter input, no item selected}
{bFS = btmFilterSel   = Filter input, item selected}
var ButtonToggleMode: Array[TListControlFrame.TButtonToggleMode]
                   of Array[0..6] of Byte = (
  {Button Top,     Up,    Add, Rename, Remove,   Down, Bottom}
{bNS}      (0,      0,      1,      0,      0,      0,      0),
{bST}      (0,      0,      1,      1,      1,      1,      1),
{bSM}      (1,      1,      1,      1,      1,      1,      1),
{bSB}      (1,      1,      1,      1,      1,      0,      0),
{bSS}      (0,      0,      1,      1,      1,      0,      0),
{bFN}      (0,      0,      0,      0,      0,      0,      0),
{bFS}      (0,      0,      0,      1,      1,      0,      0)
);
{%ENDREGION}

implementation

uses Math, UWait, TListControlViewUtils;

{$R *.lfm}

{ TListControlFrame }

procedure TListControlFrame.SetLCRefs(_PStringList: PTStringList;
    _Actions: TLCActions);
begin
    ListActions := _Actions;
    PStringList := _PStringList;
    WaitRefresh();
end;

function TListControlFrame.HasColors(): Boolean; inline;
begin
    Result := FGetItemColor <> nil;
end;

function TListControlFrame.HasItems(): Boolean;
begin
    Result := ListBox.Items.Count > 0;
end;

procedure TListControlFrame.WaitRefresh();
    function fFilterWaitCondition(): Boolean;
    begin Result := not ListBoxFilter.IdleConnected; end;
begin
    Screen.Cursor := crHourGlass;
    ListBoxFilter.Items.Assign(PStringList^);
    // ListBoxFilter restores selection using... ITEM STRING.
    // I won't come up with such a shitty idea even if I hit my head with a brick.
    // This idiotic decision causes absolutely unholy bugs if Listbox contains
    // several identical items.
    ListBoxFilter.InvalidateFilter();
    WaitFor(@fFilterWaitCondition);
    Screen.Cursor := crDefault;
end;

constructor TListControlFrame.Create(TheOwner: TComponent);
begin
    inherited Create(TheOwner);
    ListBoxRenamePanel.Parent := ListBox;
    ListBoxRenamePanel.Color := ListBox.GetColorResolvingParent();

    Orientation := lcoVertical;
    AddMode := lcaAddRename;
    FilterEnabled := True;

    ButtonsToggle();
end;

{%REGION List Box}

procedure TListControlFrame.ListBoxDrawItem(Control: TWinControl;
    Index: Integer; ARect: TRect; State: TOwnerDrawState);
var BG, FG: TColor;
begin
    if HasColors() then
        FGetItemColor(Index, BG, FG);
    DrawItemWithColorMark(ListBox, Index, ARect, State, HasColors(), BG, FG);
end;

procedure TListControlFrame.ListBoxShowHint(Sender: TObject;
    HintInfo: PHintInfo);
begin
    GetLongItemHint(ListBox, HintInfo, HasColors());
end;

// This is avaliable for filtered list, so ListBox.ItemIndex
// may mismatch with the one in the ^PStringList
procedure TListControlFrame.ListBoxSelectionChange(Sender: TObject;
    User: boolean);
begin
    if not ListBoxFilter.IdleConnected then
        ButtonsToggle();
    ListActions[lcOnSelect](ListBox.ItemIndex);
end;

procedure TListControlFrame.ListBoxFilterAfterFilter(Sender: TObject);
begin
    ListBoxSelectionChange(Sender, False);
end;

procedure TListControlFrame.ListBoxKeyDown(Sender: TObject;
    var Key: Word; Shift: TShiftState);
begin
    case Key of
        VK_ESCAPE: ListBox.ItemIndex := -1;
        VK_UP: begin
            if (ListBox.ItemIndex = 0) and FilterEnabled then
                ListBoxFilter.SetFocus()
            else if (ssShift in Shift) and HasItems() then
                ListBox.ItemIndex := 0;
        end;
        VK_DOWN: begin
            if (ssShift in Shift) and HasItems() then
                ListBox.ItemIndex := ListBox.Items.Count - 1;
        end;
        VK_LEFT, VK_RIGHT: Key := VK_UNKNOWN;
    end;
end;

procedure TListControlFrame.ListBoxDblClick(Sender: TObject);
begin
    BtnRenameClick(Sender);
end;

procedure TListControlFrame.ListBoxFilterKeyDown(Sender: TObject;
    var Key: Word; Shift: TShiftState);
begin
    case Key of
        VK_ESCAPE: begin
            ListBoxFilter.ResetFilter();
            ListBox.SetFocus();
        end;
        VK_DOWN: begin
            ListBox.SetFocus();
            Key := VK_UNKNOWN;
        end;
        // Bug again: if focused and arrow is pressed, it selects in ListBox
        VK_UP, VK_LEFT, VK_RIGHT: begin
            Key := VK_UNKNOWN;
        end;
    end;
end;

procedure TListControlFrame.ListBoxChangeBounds(Sender: TObject);
begin
    with ListBoxFilter do
        Width := ListBox.Width + Spacing + ButtonWidth;
    with ListBoxRenamePanel do
        Width := ListBox.Width;
end;

{%ENDREGION}

{%REGION Item rename}

procedure TListControlFrame.EnableItemRename(ItemRect: TRect;
    ItemText: String);
begin
    if not RenameEnabled then exit;
    with ListBoxRenamePanel do
    begin
        InflateRect(ItemRect, 0, 2);
        BoundsRect := ItemRect;
        Visible := True;
    end;
    ButtonsToggle();
    with ListBoxRenameEdit do
    begin
        Text := ItemText;
        SetFocus();
    end;
end;

procedure TListControlFrame.DisableItemRename(FocusOnListBox: Boolean);
begin
    ListBoxRenameEdit.Text := '';
    if FocusOnListBox then ListBox.SetFocus();
end;

// This is avaliable for filtered list, so ListBox.ItemIndex
// may mismatch with the one in the ^PStringList
procedure TListControlFrame.DoItemRename();
var IndexSave: Integer;
begin
    if FRenameNewName = '' then
    begin
        beep();
        exit;
    end;
    with ListBox do
    begin
        if not ListActions[lcRename](ItemIndex) then exit;
        IndexSave := ItemIndex;
        WaitRefresh();
        if Items.IndexOf(FRenameNewName) >= 0 then
            ItemIndex := IndexSave;
    end;
end;

procedure TListControlFrame.AcceptItemRename(FocusOnListBox: Boolean);
begin
    FRenameNewName := ListBoxRenameEdit.Text;
    DoItemRename();
    DisableItemRename(FocusOnListBox);
end;

procedure TListControlFrame.ListBoxRenameEditExit(Sender: TObject);
begin
    if GetParentForm(Self, True).ActiveControl.Parent = Self.ButtonPanel then
        AcceptItemRename(False);
    ListBoxRenamePanel.Visible := False;
    ButtonsToggle();
end;

procedure TListControlFrame.ListBoxRenameEditKeyDown(Sender: TObject;
    var Key: Word; Shift: TShiftState);
begin
    case Key of
        VK_ESCAPE: DisableItemRename();
        VK_RETURN: AcceptItemRename();
    end;
end;

procedure TListControlFrame.ListBoxRenameAcceptBtnClick(Sender: TObject);
begin
    AcceptItemRename();
end;

procedure TListControlFrame.ListBoxRenameCancelBtnClick(Sender: TObject);
begin
    DisableItemRename();
end;

{%ENDREGION}

{%REGION Buttons}

procedure TListControlFrame.ButtonsToggle();
var
    Mode: TButtonToggleMode;
    Index, Last, i: Integer;
begin
    Index := ListBox.ItemIndex;
    Last := ListBox.Items.Count - 1;
    if ListBoxRenamePanel.Visible then
        Mode := lcbFilterNoSel
    else if ListBoxFilter.Filter = '' then begin
        if (Index = 0) and (Last = 0) then Mode := lcbSelSingle
        else if Index = -1 then Mode := lcbNoSel
        else if Index = 0 then Mode := lcbSelTop
        else if Index = Last then Mode := lcbSelBottom
        else Mode := lcbSelMid;
    end
    else
        Mode := specialize IfThen<TButtonToggleMode>(Index = -1,
                                  lcbFilterNoSel, lcbFilterSel);
    for i := 0 to ButtonPanel.ControlCount - 1 do
        ButtonPanel.Controls[i].Enabled := ButtonToggleMode[Mode][i] = 1;
end;

procedure TListControlFrame.BtnTopClick(Sender: TObject);
begin
    with ListBox do
    begin
        if ItemIndex < 1 then exit;
        if not ListActions[lcTop](ItemIndex) then exit;
        SetFocus();
        WaitRefresh();
        ItemIndex := 0;
    end;
end;

procedure TListControlFrame.BtnUpClick(Sender: TObject);
var Index: Integer;
begin
    with ListBox do
    begin
        Index := ItemIndex;
        if Index < 1 then exit;
        if not ListActions[lcUp](Index) then exit;
        SetFocus();
        WaitRefresh();
        ItemIndex := Index - 1;
    end;
end;

procedure TListControlFrame.BtnAddClick(Sender: TObject);
var Index: Integer;
begin
    with ListBox do
    begin
        Index := IfThen(ItemIndex = -1, Items.Count, ItemIndex + 1);
        if not ListActions[lcAdd](Index) then exit;
        SetFocus();
        WaitRefresh();
        ItemIndex := Index;
        if RenameEnabled then
            EnableItemRename(ItemRect(Index), Items[Index]);
    end;
end;

procedure TListControlFrame.BtnRenameClick(Sender: TObject);
var Index: Integer;
begin
    with ListBox do begin
        Index := ItemIndex;
        if (Index = -1) then exit;
        EnableItemRename(ItemRect(Index), Items[Index]);
    end;
end;

// This is avaliable for filtered list, so ListBox.ItemIndex
// may mismatch with the one in the ^PStringList
procedure TListControlFrame.BtnRemoveClick(Sender: TObject);
begin
    with ListBox do
    begin
        if not ListActions[lcRemove](ItemIndex) then exit;
        ItemIndex := -1;
        SetFocus();
        WaitRefresh();
    end;
end;

procedure TListControlFrame.BtnDownClick(Sender: TObject);
var Index: Integer;
begin
    with ListBox do
    begin
        Index := ItemIndex;
        if Index > Items.Count - 2 then exit;
        if not ListActions[lcDown](Index) then exit;
        SetFocus();
        WaitRefresh();
        ItemIndex := Index + 1;
    end;
end;

procedure TListControlFrame.BtnBottomClick(Sender: TObject);
begin
    with ListBox do
    begin
        if ItemIndex > Items.Count - 2 then exit;
        if not ListActions[lcBottom](ItemIndex) then exit;
        SetFocus();
        WaitRefresh();
        ItemIndex := Items.Count - 1;
    end;
end;

{%ENDREGION}

function TListControlFrame.getItems(): TStrings;
begin
    Result := ListBox.Items;
end;

procedure TListControlFrame.setOrientation(AOrientation: TLCOrientation);
begin
    if AOrientation = lcoVertical then
    begin
        ButtonPanel.Width := 40;
        ButtonPanel.Constraints.MinHeight := 320;
        ButtonPanel.Constraints.MaxHeight := 320;
        ButtonPanel.Height := 320;
        ButtonPanel.Anchors := ButtonPanel.Anchors - [akLeft];
        ButtonPanel.AnchorParallel(akRight, 0, Self);
        ButtonPanel.AnchorVerticalCenterTo(ListBox);
        ListBox.AnchorToNeighbour(akRight, 0, ButtonPanel);
        ListBox.AnchorParallel(akBottom, 0, Self);
        BtnAdd.Anchors := BtnAdd.Anchors - [akTop] - [akRight];
        BtnAdd.AnchorHorizontalCenterTo(ButtonPanel);
        BtnAdd.AnchorToNeighbour(akBottom, 0, BtnRename);
        BtnUp.Anchors := BtnUp.Anchors - [akTop] - [akRight];
        BtnUp.AnchorHorizontalCenterTo(ButtonPanel);
        BtnUp.AnchorToNeighbour(akBottom, 0, BtnAdd);
        BtnTop.Anchors := BtnTop.Anchors - [akTop] - [akRight];
        BtnTop.AnchorHorizontalCenterTo(ButtonPanel);
        BtnTop.AnchorToNeighbour(akBottom, 0, BtnUp);
        BtnRemove.Anchors := BtnRemove.Anchors - [akBottom] - [akRight];
        BtnRemove.AnchorHorizontalCenterTo(ButtonPanel);
        BtnRemove.AnchorToNeighbour(akTop, 0, BtnRename);
        BtnDown.Anchors := BtnDown.Anchors - [akBottom] - [akRight];
        BtnDown.AnchorHorizontalCenterTo(ButtonPanel);
        BtnDown.AnchorToNeighbour(akTop, 0, BtnRemove);
        BtnBottom.Anchors := BtnBottom.Anchors - [akBottom] - [akRight];
        BtnBottom.AnchorHorizontalCenterTo(ButtonPanel);
        BtnBottom.AnchorToNeighbour(akTop, 0, BtnDown);
    end else
    begin
        ButtonPanel.Width := 320;
        ButtonPanel.Constraints.MinHeight := 40;
        ButtonPanel.Constraints.MaxHeight := 40;
        ButtonPanel.Height := 40;
        ButtonPanel.Anchors := ButtonPanel.Anchors - [akTop];
        ButtonPanel.AnchorParallel(akBottom, 0, Self);
        ButtonPanel.AnchorHorizontalCenterTo(ListBox);
        ListBox.AnchorParallel(akRight, 0, Self);
        ListBox.AnchorToNeighbour(akBottom, 0, ButtonPanel);
        BtnAdd.Anchors := BtnAdd.Anchors - [akBottom] - [akLeft];
        BtnAdd.AnchorVerticalCenterTo(ButtonPanel);
        BtnAdd.AnchorToNeighbour(akRight, 0, BtnRename);
        BtnUp.Anchors := BtnUp.Anchors - [akBottom] - [akLeft];
        BtnUp.AnchorVerticalCenterTo(ButtonPanel);
        BtnUp.AnchorToNeighbour(akRight, 0, BtnAdd);
        BtnTop.Anchors := BtnTop.Anchors - [akBottom] - [akLeft];
        BtnTop.AnchorVerticalCenterTo(ButtonPanel);
        BtnTop.AnchorToNeighbour(akRight, 0, BtnUp);
        BtnRemove.Anchors := BtnRemove.Anchors - [akBottom] - [akRight];
        BtnRemove.AnchorVerticalCenterTo(ButtonPanel);
        BtnRemove.AnchorToNeighbour(akLeft, 0, BtnRename);
        BtnDown.Anchors := BtnDown.Anchors - [akBottom] - [akRight];
        BtnDown.AnchorVerticalCenterTo(ButtonPanel);
        BtnDown.AnchorToNeighbour(akLeft, 0, BtnRemove);
        BtnBottom.Anchors := BtnBottom.Anchors - [akBottom] - [akRight];
        BtnBottom.AnchorVerticalCenterTo(ButtonPanel);
        BtnBottom.AnchorToNeighbour(akLeft, 0, BtnDown);
    end;
    FOrientation := AOrientation;
end;

procedure TListControlFrame.setFilterEnabled(AFilterEnabled: Boolean);
begin
    ListBoxFilter.Visible := AFilterEnabled;
    FFilterEnabled := AFilterEnabled;
end;

procedure TListControlFrame.setButtonPanelEnabled(AButtonPanelEnabled: Boolean);
begin
    ButtonPanel.Visible := AButtonPanelEnabled;
    FButtonPanelEnabled := AButtonPanelEnabled;
end;

procedure TListControlFrame.setRenameEnabled(ARenameEnabled: Boolean);
begin
    // Changing BtnRename.Visible makes Add and Remove buttons overlap
    if ARenameEnabled then
    begin
        BtnRename.Height := 36;
        BtnRename.Width := 36;
        BtnAdd.BorderSpacing.Around := 8;
        BtnRemove.BorderSpacing.Around := 8;
    end
    else begin
        BtnRename.Height := 0;
        BtnRename.Width := 0;
        BtnAdd.BorderSpacing.Around := 4;
        BtnRemove.BorderSpacing.Around := 4;
        AddMode := lcaCustom;
    end;
    FRenameEnabled := ARenameEnabled;
end;

procedure TListControlFrame.setAddMode(AAddMode: TLCAddMode);
begin
    if AAddMode = lcaAddRename then
        RenameEnabled := True;
    FAddMode := AAddMode;
end;

end.

