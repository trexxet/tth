unit ListControl_Conn;

{$mode ObjFPC}{$H+}

interface

uses
    SysUtils, Controls, ListControl, Graphics,
    UEntity, UEntityList, UID, UFocusGroup;

type

    TLCCAction = function(var ID: TID; Index: SizeInt): Boolean of object;
    TLCCActions = Array[TLCActionType] of TLCCAction;

    TLCCGetColorAction = procedure(ID: TID; out BG, FG: TColor) of object;

    { TLC_Connector }
    generic TLC_Connector<T: TEntity> = class(TObject)
    public
        LCFrame: TListControlFrame;
    private
        type TSEntityList = specialize TEntityList<T>;
    private
        function LCActionTop(Index: Integer): Boolean; virtual; abstract;
        function LCActionUp(Index: Integer): Boolean; virtual; abstract;
        function LCActionAdd(Index: Integer): Boolean; virtual; abstract;
        function LCActionRename(Index: Integer): Boolean; virtual; abstract;
        function LCActionRemove(Index: Integer): Boolean; virtual; abstract;
        function LCActionDown(Index: Integer): Boolean; virtual; abstract;
        function LCActionBottom(Index: Integer): Boolean; virtual; abstract;
        function LCActionOnSelect(Index: Integer): Boolean; virtual; abstract;
    public
        EntityList: TSEntityList;
    private
        LCActions: TListControlFrame.TLCActions;
        LCCActions: TLCCActions;
    private
        FocusGroup: TFocusGroup;
    public
        function IsFiltered(): Boolean; inline;
        procedure ResetFilter();
        function GetSelectedIndex(): SizeInt; inline;
        function GetSelectedID(): TID; overload; inline;
        function GetSelectedID(Index: Integer): TID; overload; inline;
        function GetDisplayedIndex(ID: TID): SizeInt; inline;
        function GetTrueIndex(ID: TID): SizeInt; overload; inline;
        function GetTrueIndex(Index: Integer): SizeInt; overload; inline;
        function GetIDAndCheckIndex(Index: Integer; out ID: TID): Boolean;
        function GetIDAndTrueIndex(Index: Integer; out ID: TID;
                                   out TrueIndex: SizeInt): Boolean;
    public
        procedure SetFocus();
        function HasFocus(): Boolean;
        procedure SetOnID(ID: TID);
        constructor Create(TheOwner: TWinControl;
                           _TLCCActions: TLCCActions);
        destructor Destroy(); override;
    end;

implementation

{ TLC_Connector }

function TLC_Connector.IsFiltered(): Boolean;
begin
    Result := LCFrame.ListBoxFilter.Text <> '';
end;

procedure TLC_Connector.ResetFilter();
begin
    LCFrame.ListBoxFilter.Text := '';
    LCFrame.WaitRefresh();
end;

function TLC_Connector.GetSelectedIndex(): SizeInt;
begin
    Result := LCFrame.ListBox.ItemIndex;
end;

function TLC_Connector.GetSelectedID(): TID;
begin
    Result := GetSelectedID(GetSelectedIndex());
end;

function TLC_Connector.GetSelectedID(Index: Integer): TID;
begin
    if Index > -1 then
        Result := TID(LCFrame.Items.Objects[Index])
    else Result := 0;
end;

function TLC_Connector.GetDisplayedIndex(ID: TID): SizeInt;
var Index: SizeInt;
begin
    for Index := 0 to LCFrame.Items.Count - 1 do
        if GetSelectedID(Index) = ID then begin
            Result := Index;
            exit;
        end;
    Result := -1;
end;

function TLC_Connector.GetTrueIndex(ID: TID): SizeInt;
begin
    if ID > 0 then
        Result := EntityList.OrderOf(ID)
    else Result := -1;
end;

function TLC_Connector.GetTrueIndex(Index: Integer): SizeInt;
begin
    if Index > -1 then
        Result := EntityList.OrderOf(GetSelectedID(Index))
    else Result := -1;
end;

function TLC_Connector.GetIDAndCheckIndex(Index: Integer; out ID: TID): Boolean;
begin
    ID := GetSelectedID(Index);
    Result := GetTrueIndex(ID) > -1;
end;

function TLC_Connector.GetIDAndTrueIndex(Index: Integer; out ID: TID;
    out TrueIndex: SizeInt): Boolean;
begin
    ID := GetSelectedID(Index);
    TrueIndex := GetTrueIndex(ID);
    Result := TrueIndex > -1;
end;

procedure TLC_Connector.SetFocus();
begin
    if not LCFrame.ListBox.CanFocus then exit;
    LCFrame.ListBox.SetFocus();
    if (GetSelectedIndex() = -1) and LCFrame.HasItems() then
        LCFrame.ListBox.ItemIndex := 0;
end;

function TLC_Connector.HasFocus(): Boolean;
var ChildControl: TWinControl;
begin
    Result := False;
    for ChildControl in FocusGroup do begin
        Result := ChildControl.Focused;
        if Result then exit;
    end;
end;

procedure TLC_Connector.SetOnID(ID: TID);
var Index: SizeInt;
begin
    Index := GetDisplayedIndex(ID);
    if (Index = -1) and IsFiltered() then begin
        ResetFilter();
        Index := GetDisplayedIndex(ID);
    end;
    if Index >= 0 then
        LCFrame.ListBox.ItemIndex := Index;
end;

constructor TLC_Connector.Create(TheOwner: TWinControl;
    _TLCCActions: TLCCActions);
begin
    inherited Create();

    LCActions[lcTop]      := @LCActionTop;
    LCActions[lcUp]       := @LCActionUp;
    LCActions[lcAdd]      := @LCActionAdd;
    LCActions[lcRename]   := @LCActionRename;
    LCActions[lcRemove]   := @LCActionRemove;
    LCActions[lcDown]     := @LCActionDown;
    LCActions[lcBottom]   := @LCActionBottom;
    LCActions[lcOnSelect] := @LCActionOnSelect;

    LCCActions := _TLCCActions;

    LCFrame := TListControlFrame.Create(TheOwner);
    LCFrame.Parent := TheOwner;

    FocusGroup := TFocusGroup.Create;
    FocusGroup.AddWithChildrenRecursive(LCFrame);
end;

destructor TLC_Connector.Destroy();
begin
    FreeAndNil(FocusGroup);
    FreeAndNil(LCFrame);
    inherited Destroy();
end;

end.

