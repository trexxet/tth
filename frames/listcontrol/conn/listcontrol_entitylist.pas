unit ListControl_EntityList;

{$mode ObjFPC}{$H+}

interface

uses
    SysUtils, Controls, Graphics,
    ListControl, ListControl_Conn,
    UEntity, UID;

type
    { TLC_EL_Connector }
    generic TLC_EL_Connector<T: TEntity> = class(specialize TLC_Connector<T>)
    private
        function LCActionTop(Index: Integer): Boolean; override;
        function LCActionUp(Index: Integer): Boolean; override;
        function LCActionAdd(Index: Integer): Boolean; override;     // Filter should be disabled
        function LCActionRename(Index: Integer): Boolean; override;
        function LCActionRemove(Index: Integer): Boolean; override;
        function LCActionDown(Index: Integer): Boolean; override;
        function LCActionBottom(Index: Integer): Boolean; override;
        function LCActionOnSelect(Index: Integer): Boolean; override;
    private
        GetIDColor: TLCCGetColorAction;
        procedure LCGetColor(Index: Integer; out BG, FG: TColor);
    public
        procedure AssignData(constref EList: TSEntityList);
        procedure EnableColors(GetColorAction: TLCCGetColorAction);
        constructor Create(TheOwner: TWinControl;
                           _TLCCActions: TLCCActions);
        destructor Destroy(); override;
    end;

implementation

{ TLC_EL_Connector }

function TLC_EL_Connector.LCActionTop(Index: Integer): Boolean;
var DummyID: TID = 0;
begin
    Result := EntityList.Move(Index, 0);
    if LCCActions[lcTop] <> nil then
        Result := Result and LCCActions[lcTop](DummyID, Index);
end;

function TLC_EL_Connector.LCActionUp(Index: Integer): Boolean;
var DummyID: TID = 0;
begin
    Result := EntityList.Exchange(Index - 1, Index);
    if LCCActions[lcUp] <> nil then
        Result := Result and LCCActions[lcUp](DummyID, Index);
end;

// Filter should be disabled
function TLC_EL_Connector.LCActionAdd(Index: Integer): Boolean;
var DummyID: TID = 0;
begin
    Result := False;
    if IsFiltered() then exit;
    if LCCActions[lcAdd] <> nil then
        Result := LCCActions[lcAdd](DummyID, Index);
end;

function TLC_EL_Connector.LCActionRename(Index: Integer): Boolean;
var ID: TID;
begin
    Result := GetIDAndCheckIndex(Index, ID);
    if not Result then exit;
    Result := EntityList.Rename(ID, LCFrame.RenameNewName);
    if LCCActions[lcRename] <> nil then
        Result := Result and LCCActions[lcRename](ID, Index);
end;

function TLC_EL_Connector.LCActionRemove(Index: Integer): Boolean;
var ID: TID;
begin
    ID := GetSelectedID(Index);
    Result := ID > 0;
    if not Result then exit;
    if LCCActions[lcRemove] <> nil then
    begin
        Result := LCCActions[lcRemove](ID, -1)
    end else
        Result := False;
end;

function TLC_EL_Connector.LCActionDown(Index: Integer): Boolean;
var DummyID: TID = 0;
begin
    Result := EntityList.Exchange(Index, Index + 1);
    if LCCActions[lcDown] <> nil then
        Result := Result and LCCActions[lcDown](DummyID, Index);
end;

function TLC_EL_Connector.LCActionBottom(Index: Integer): Boolean;
var DummyID: TID = 0;
begin
    Result := EntityList.Move(Index, EntityList.StringList.Count - 1);
    if LCCActions[lcBottom] <> nil then
        Result := Result and LCCActions[lcBottom](DummyID, Index);
end;

function TLC_EL_Connector.LCActionOnSelect(Index: Integer): Boolean;
var SelectedID: TID;
begin
    if LCCActions[lcOnSelect] <> nil then
    begin
        SelectedID := GetSelectedID(Index);
        Result := LCCActions[lcOnSelect](SelectedID, Index)
    end else
        Result := False;
end;

procedure TLC_EL_Connector.LCGetColor(Index: Integer; out BG, FG: TColor);
begin
    if GetIDColor <> nil then
        GetIDColor(GetSelectedID(Index), BG, FG);
end;

procedure TLC_EL_Connector.AssignData(constref EList: TSEntityList);
begin
    EntityList := EList;
    LCFrame.SetLCRefs(@EList.StringList, LCActions);
end;

procedure TLC_EL_Connector.EnableColors(GetColorAction: TLCCGetColorAction);
begin
    if GetColorAction = nil then exit;
    GetIDColor := GetColorAction;
    LCFrame.GetItemColor := @LCGetColor;
end;

constructor TLC_EL_Connector.Create(TheOwner: TWinControl;
    _TLCCActions: TLCCActions);
begin
    inherited Create(TheOwner, _TLCCActions);
end;

destructor TLC_EL_Connector.Destroy();
begin
    inherited Destroy();
end;

end.

