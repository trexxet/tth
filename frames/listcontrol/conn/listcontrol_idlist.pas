unit ListControl_IDList;

{$mode ObjFPC}{$H+}

interface

uses
    Classes, SysUtils, Controls,
    ListControl, ListControl_Conn,
    UEntity, UEntityList, UID;

type
    // Holds and manages a StringList passed to LCFrame with corresponding
    // names of Entites in EntityList with IDs from IDList
    { TLC_IDL_Connector }
    generic TLC_IDL_Connector<T: TEntity> = class(specialize TLC_Connector<T>)
    private
        function LCActionTop(Index: Integer): Boolean; override;
        function LCActionUp(Index: Integer): Boolean; override;
        function LCActionAdd(Index: Integer): Boolean; override;    // Filter should be disabled
        function LCActionRename(Index: Integer): Boolean; override; // Disabled
        function LCActionRemove(Index: Integer): Boolean; override; // Filter should be disabled
        function LCActionDown(Index: Integer): Boolean; override;
        function LCActionBottom(Index: Integer): Boolean; override;
        function LCActionOnSelect(Index: Integer): Boolean; override;
    public
        IDList: TIDList;
    private
        StringList: TStringList;
    public
        procedure AssignData(constref _IDList: TIDList;
                             constref _LinkedEntityList: TSEntityList);
        procedure ClearData();
        procedure RenameByID(ID: TID);
        constructor Create(TheOwner: TWinControl;
                           _TLCCActions: TLCCActions);
        destructor Destroy(); override;
    end;

implementation

{ TLC_IDL_Connector }

function TLC_IDL_Connector.LCActionTop(Index: Integer): Boolean;
var DummyID: TID = 0;
begin
    IDList.Move(Index, 0);
    StringList.Move(Index, 0);
    if LCCActions[lcTop] <> nil then
        LCCActions[lcTop](DummyID, Index);
    Result := True;
end;

function TLC_IDL_Connector.LCActionUp(Index: Integer): Boolean;
var DummyID: TID = 0;
begin
    IDList.Exchange(Index, Index - 1);
    StringList.Exchange(Index, Index - 1);
    if LCCActions[lcUp] <> nil then
        LCCActions[lcUp](DummyID, Index);
    Result := True;
end;

// Filter should be disabled
function TLC_IDL_Connector.LCActionAdd(Index: Integer): Boolean;
var PickedID: TID = 0;
begin
    Result := False;
    if IsFiltered() then exit;
    if LCCActions[lcAdd] <> nil then
        Result := LCCActions[lcAdd](PickedID, Index)
    else
        Result := False;
    if not Result then exit;

    StringList.InsertObject(Index,
                            EntityList.At(PickedID).Name, TObject(PickedID));
end;

// Disabled
function TLC_IDL_Connector.LCActionRename(Index: Integer): Boolean;
begin
    Result := False;
end;

// Filter should be disabled
function TLC_IDL_Connector.LCActionRemove(Index: Integer): Boolean;
var SelectedID: TID = 0;
begin
    Result := False;
    if IsFiltered() then exit;
    if LCCActions[lcRemove] <> nil then
    begin
        SelectedID := GetSelectedID(Index);
        Result := LCCActions[lcRemove](SelectedID, Index)
    end else
        Result := False;
    if not Result then exit;

    StringList.Delete(Index);
    Result := True;
end;

function TLC_IDL_Connector.LCActionDown(Index: Integer): Boolean;
var DummyID: TID = 0;
begin
    IDList.Exchange(Index, Index + 1);
    StringList.Exchange(Index, Index + 1);
    if LCCActions[lcDown] <> nil then
        LCCActions[lcDown](DummyID, Index);
    Result := True;
end;

function TLC_IDL_Connector.LCActionBottom(Index: Integer): Boolean;
var DummyID: TID = 0;
begin
    IDList.Move(Index, StringList.Count - 1);
    StringList.Move(Index, StringList.Count - 1);
    if LCCActions[lcBottom] <> nil then
        LCCActions[lcBottom](DummyID, Index);
    Result := True;
end;

function TLC_IDL_Connector.LCActionOnSelect(Index: Integer): Boolean;
var SelectedID: TID;
begin
    if LCCActions[lcOnSelect] <> nil then
    begin
        SelectedID := GetSelectedID(Index);
        Result := LCCActions[lcOnSelect](SelectedID, Index);
    end else
        Result := False;
end;

procedure TLC_IDL_Connector.AssignData(constref _IDList: TIDList; constref
    _LinkedEntityList: TSEntityList);
var ID: TID;
begin
    IDList := _IDList;
    EntityList := _LinkedEntityList;

    StringList.Clear();
    for ID in IDList do
        StringList.AddObject(EntityList.At(ID).Name, TObject(ID));

    LCFrame.SetLCRefs(@StringList, LCActions);
end;

procedure TLC_IDL_Connector.ClearData();
begin
    IDList := nil;
    EntityList := nil;
    StringList.Clear();
    LCFrame.SetLCRefs(@StringList, LCActions);
end;

procedure TLC_IDL_Connector.RenameByID(ID: TID);
var
    Index: SizeInt;
    NeedLCRefresh: Boolean;
begin
    NeedLCRefresh := False;
    for Index := 0 to IDList.Count - 1 do
        if IDList[Index] = ID then
        begin
            StringList[Index] := EntityList.At(ID).Name;
            NeedLCRefresh := True;
        end;
    if NeedLCRefresh then
        LCFrame.WaitRefresh();
end;

constructor TLC_IDL_Connector.Create(TheOwner: TWinControl;
    _TLCCActions: TLCCActions);
begin
    inherited Create(TheOwner, _TLCCActions);
    StringList := TStringList.Create();
    StringList.OwnsObjects := False;
end;

destructor TLC_IDL_Connector.Destroy();
begin
    FreeAndNil(StringList);
    inherited Destroy();
end;

end.

