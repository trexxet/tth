unit RouteStopParams;

{$mode ObjFPC}{$H+}

interface

uses
    Classes, SysUtils, Forms, StdCtrls, ExtCtrls, ECEditBtns,
    ListControl, HMTSetter,
    UStopSchedule, USTime, UID;

type

    { TRouteStopParamsFrame }

    TRouteStopParamsFrame = class(TFrame)
    {%REGION Stop Params} published
        StopParams: TPanel;
        TravelTimeLabel: TLabel;
        TravelTimeBtn: TButton;
        StoppingTimeLabel: TLabel;
        StoppingTimeBtn: TButton;
        NSToggleBox: TECBitBtn;
        procedure StoppingTimeBtnClick(Sender: TObject);
        procedure TravelTimeBtnClick(Sender: TObject);
        procedure NSToggleBoxChange(Sender: TObject);
        procedure DoTimeBtnClick(Btn: TButton; SchType: TSchType);
        procedure DoOnChange();
    {%ENDREGION}
    {%REGION Route Params} published
        RouteParams: TPanel;
        ColorLabel: TLabel;
        ColorCombo: TECColorCombo;
        DirectionLabel: TLabel;
        DirectionNameLabel: TLabel;
        procedure ColorComboCustomColorChanged(Sender: TObject);
    {%ENDREGION}
    private
        RouteID: TID;
        StopIndex: SizeUInt;
    public
        OnChange: TLCAction;
        NotifyColorChange: TNotifyEvent;
        procedure TimeModeChanged(IsGameTime: Boolean);
        procedure AssignData(_RouteID: TID; _StopIndex: SizeUInt);
        procedure Reassign();
    end;

implementation

uses
    Math,
    UTimetable, TButtonControlUtils, TColorComboUtils,
    ScheduleEditor_RS;

{$R *.lfm}

{ TRouteStopParamsFrame }

procedure TRouteStopParamsFrame.StoppingTimeBtnClick(Sender: TObject);
begin
    DoTimeBtnClick(StoppingTimeBtn, Stopping);
end;

procedure TRouteStopParamsFrame.TravelTimeBtnClick(Sender: TObject);
begin
    DoTimeBtnClick(TravelTimeBtn, Travel);
end;

procedure TRouteStopParamsFrame.NSToggleBoxChange(Sender: TObject);
begin
    Timetable.Routes[RouteID].SetNS(StopIndex, NSToggleBox.Checked);
    NSToggleBox.Hint := specialize IfThen<String>(NSToggleBox.Checked,
        TNS_RS_NonStopHint, TNS_RS_StopHint);
    DoOnChange();
end;

procedure TRouteStopParamsFrame.DoTimeBtnClick(Btn: TButton; SchType: TSchType);
var HM: THMTime;
begin
    if TimeMode <> GameTime then exit;
    with Timetable.Routes[RouteID] do
    begin
        HM := GetStopTime(StopIndex, SchType).HM;
        SetStopTime(StopIndex, SchType, HMTSetterForm.Edit(HM));
        Btn.Caption := GetStopTime(StopIndex, SchType).ToStr();
    end;
    DoOnChange();
end;

procedure TRouteStopParamsFrame.DoOnChange();
begin
    if OnChange <> nil then OnChange(StopIndex);
end;

procedure TRouteStopParamsFrame.ColorComboCustomColorChanged(Sender: TObject);
begin
    Timetable.Routes[RouteID].Color := ColorCombo.CustomColor;
    if NotifyColorChange <> nil then
        NotifyColorChange(Self);
end;

procedure TRouteStopParamsFrame.TimeModeChanged(IsGameTime: Boolean);
begin
    Reassign();
end;

procedure TRouteStopParamsFrame.AssignData(_RouteID: TID; _StopIndex: SizeUInt);
begin
    RouteID := _RouteID;
    StopIndex := _StopIndex;

    with Timetable.Routes[RouteID] do
    begin
        StoppingTimeBtn.Caption := GetStopTime(StopIndex, Stopping).ToStr();
        TravelTimeBtn.Caption := GetStopTime(StopIndex, Travel).ToStr();
        NSToggleBox.SetCheckedSilent(GetNS(StopIndex));
        NSToggleBox.Hint := specialize IfThen<String>(NSToggleBox.Checked,
            TNS_RS_NonStopHint, TNS_RS_StopHint);

        ColorCombo.SetColorSilent(Color);
        DirectionNameLabel.Caption := DirectionForStop(StopIndex).Name;
    end;
end;

procedure TRouteStopParamsFrame.Reassign();
begin
    if Timetable.Routes.Exists(RouteID) then
        AssignData(RouteID, StopIndex);
end;

end.

