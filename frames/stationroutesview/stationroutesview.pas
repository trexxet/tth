unit StationRoutesView;

{$mode ObjFPC}{$H+}

interface

uses
    Classes, SysUtils, Controls, StdCtrls, EntityListView,
    UID;

type

    { TStationRoutesViewFrame }
    TStationRoutesViewFrame = class(TEntityListViewFrame)
        ShowDirectionsCheckbox: TCheckBox;
        procedure ShowDirectionsCheckboxChange(Sender: TObject);
    public
        type TFillRouteList = procedure(ID: TID; var ViewList: TEntityViewList;
                                        NeedDirections: Boolean) of object;
    protected
        FillRouteList: TFillRouteList;
        function DoFillViewList(): Boolean; override;
    public
        constructor Create(TheOwner: TComponent; FillRouteListAction: TFillRouteList;
            ViewListGotoAction: TViewListGoto); reintroduce;
    end;

implementation

{$R *.lfm}

{ TStationRoutesViewFrame }

procedure TStationRoutesViewFrame.ShowDirectionsCheckboxChange(Sender: TObject);
begin
    Reassign();
end;

function TStationRoutesViewFrame.DoFillViewList(): Boolean;
begin
    Result := FillRouteList <> nil;
    if Result then FillRouteList(OwnerID, ViewList,
                                 ShowDirectionsCheckbox.Checked);
end;

constructor TStationRoutesViewFrame.Create(TheOwner: TComponent;
    FillRouteListAction: TFillRouteList; ViewListGotoAction: TViewListGoto);
begin
    inherited Create(TheOwner, nil, ViewListGotoAction);
    FillRouteList := FillRouteListAction;
end;

end.

