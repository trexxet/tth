unit Editor;

{$mode delphi}{$H+}

interface

uses
    Classes, SysUtils, Forms, Controls, ComCtrls, StdCtrls, ExtCtrls, Buttons,
    ActnList,
    ScheduleEditor, RouteStopParams,
    ScheduleView, StationScheduleView, DirectionScheduleView, StopScheduleView,
    ListControl_EntityList, ListControl_IDList,
    EntityListView, StationRoutesView,
    UStation, UDirection, URoute, UTrain, UFocusGroup;

type

    { TEditorFrame }

    TEditorFrame = class(TFrame)
        published
        LabelRunning: TLabel;
        PGC: TPageControl;
        procedure PGCChange(Sender: TObject);
        {%REGION TabStations } published
        TabStations: TTabSheet;
        StationLCBox: TGroupBox;
        StationLCSplitter: TSplitter;
        StationPGC: TPageControl;
        {%ENDREGION}
        {%REGION TabRoutes } published
        TabRoutes: TTabSheet;
        RouteLCBox: TGroupBox;
        RouteLCSplitter: TSplitter;
        RoutesPGC: TPageControl;
        procedure RoutesPGCChange(Sender: TObject);
        {%ENDREGION}
        {%REGION TabStationSchedule } published
        TabStationSchedule: TTabSheet;
        StationRoutesBox: TGroupBox;
        StationRoutesListView: TStationRoutesViewFrame;
        StationRoutesSplitter: TSplitter;
        StationScheduleBox: TGroupBox;
        StationSchedule: TStationScheduleViewFrame;
        {%ENDREGION}
        {%REGION TabDirections } published
        TabDirections: TTabSheet;
        DirectionLCBox: TGroupBox;
        DirectionLCSplitter: TSplitter;
        DirectionEPanel: TPanel;
        DirectionInfoPanel: TPanel;
        DirectionRoutesBox: TGroupBox;
        DirectionRoutesListView: TEntityListViewFrame;
        DirectionRoutesSplitter: TSplitter;
        DirectionStationsLCBox: TGroupBox;
        DirectionInfoPanelSplitter: TSplitter;
        DirectionScheduleBox: TGroupBox;
        DirectionSchedule: TDirectionScheduleViewFrame;
        {%ENDREGION}
        {%REGION TabStops } published
        TabStops: TTabSheet;
        StopLCBox: TGroupBox;
        StopLCSplitter: TSplitter;
        StopsReverseBtn: TBitBtn;
        StopBox: TGroupBox;
        StopParamsPanel: TPanel;
        StopParams: TRouteStopParamsFrame;
        StopScheduleBox: TGroupBox;
        StopSchedule: TStopScheduleViewFrame;
        procedure StopsReverseBtnClick(Sender: TObject);
        {%ENDREGION}
        {%REGION TabTrains } published
        TabTrains: TTabSheet;
        TrainLCBox: TGroupBox;
        TrainLCSplitter: TSplitter;
        TrainScheduleBox: TGroupBox;
        TrainScheduleEditor: TScheduleEditorFrame;
        {%ENDREGION}
        {%REGION ListControl Connectors } public
        StationLC: TLC_EL_Connector<TStation>;
        RouteLC: TLC_EL_Connector<TRoute>;
        DirectionLC: TLC_EL_Connector<TDirection>;
        TrainLC: TLC_EL_Connector<TTrain>;
        StopsLC: TLC_IDL_Connector<TStation>;
        DirectionStationsLC: TLC_IDL_Connector<TStation>;
        {%ENDREGION}
        {%REGION Focus groups } private
        StationScheduleFocusGroup: TFocusGroup;
        DirectionsFocusGroup: TFocusGroup;
        StopsFocusGroup: TFocusGroup;
        TrainFocusGroup: TFocusGroup;
        {%ENDREGION}
        {%REGION SwitchActions } published
        SwitchActions: TActionList;
        SwitchTabStations: TAction;
        SwitchTabRoutes: TAction;
        SwitchTabStationSchedule: TAction;
        SwitchTabDirections: TAction;
        SwitchTabStops: TAction;
        SwitchTabTrains: TAction;
        procedure SwitchTabStationsExecute(Sender: TObject);
        procedure SwitchTabRoutesExecute(Sender: TObject);
        procedure SwitchTabStationScheduleExecute(Sender: TObject);
        procedure SwitchTabDirectionsExecute(Sender: TObject);
        procedure SwitchTabStopsExecute(Sender: TObject);
        procedure SwitchTabTrainsExecute(Sender: TObject);
        {%ENDREGION}
        {%REGION Zoom Actions} published
        // Why are these there? Because the braindead piece of pascal shit
        // can't catch key shortcuts right in the TScheduleViewFrame
        ZoomActions: TActionList;
        ZoomInAction: TAction;
        ZoomResetAction: TAction;
        ZoomOutAction: TAction;
        procedure ZoomInActionExecute(Sender: TObject);
        procedure ZoomOutActionExecute(Sender: TObject);
        procedure ZoomResetActionExecute(Sender: TObject);
        {%ENDREGION}
    private
        function GetVisibleScheduleView(): TScheduleViewFrame;
        procedure SetDirectionStationsLCParameters();
    public
        procedure FocusUp();
        procedure FocusDown();
    public
        procedure AssignData();
        procedure TimeModeChanged(IsGameTime: Boolean);
        constructor Create(TheOwner: TComponent); override;
        destructor Destroy(); override;
    end;

implementation

uses
    Main, Graphics, ListControl,
    UEditorLCActions, UEntity, UTimetable;

procedure InitLC<T: TEntity>(var LC: TLC_EL_Connector<T>;
    constref OwnerBox: TGroupBox; ConnName: String); overload;
begin
    OwnerBox.Color := clDefault;
    LC := TLC_EL_Connector<T>.Create(OwnerBox,
                                     MakeLCCActionsFor(ConnName));
end;

procedure InitLC<T: TEntity>(var LC: TLC_IDL_Connector<T>;
    constref OwnerBox: TGroupBox; ConnName: String); overload;
begin
    OwnerBox.Color := clDefault;
    LC := TLC_IDL_Connector<T>.Create(OwnerBox,
                                      MakeLCCActionsFor(ConnName));
end;

{$R *.lfm}

{ TEditorFrame }

procedure TEditorFrame.PGCChange(Sender: TObject);
begin
    // Restore focus for ScheduleEditor VT
    if PGC.ActivePage = TabRoutes then
        RoutesPGCChange(Sender);
end;

procedure TEditorFrame.RoutesPGCChange(Sender: TObject);
begin
    // Restore focus for ScheduleEditor VT
    if (RoutesPGC.ActivePage = TabTrains) and TrainScheduleBox.Visible then
        TrainScheduleEditor.VT.SetFocus();
end;

procedure TEditorFrame.StopsReverseBtnClick(Sender: TObject);
begin
    EdAct.StopsReverse();
end;

procedure TEditorFrame.SetDirectionStationsLCParameters();
var
    i: Integer;
    Btn: TBitBtn;
begin
    with DirectionStationsLC.LCFrame do
    begin
        FilterEnabled := False;
        Orientation := lcoHorizontal;
        with ButtonPanel do
        begin
            for i := 0 to ControlCount - 1 do
            begin
                Btn := Controls[i] as TBitBtn;
                Btn.ImageWidth := 20;
                Btn.Height := 24;
                Btn.Width := 24;
            end;
            Constraints.MinHeight := 30;
            Constraints.MaxHeight := 30;
            Height := 30;
        end;
        RenameEnabled := False;
    end;
end;

procedure TEditorFrame.FocusUp();
var AC: TWinControl;
begin
    AC := MainForm.ActiveControl;
    // TabStations
    if StationScheduleFocusGroup.HasFocus(AC) then
        StationLC.SetFocus()
    else if DirectionsFocusGroup.HasFocus(AC) then
        DirectionLC.SetFocus()
    else if DirectionLC.HasFocus() then
        StationLC.SetFocus()
    // TabRoutes
    else if StopsFocusGroup.HasFocus(AC) then
        StopsLC.SetFocus()
    else if StopsLC.HasFocus() then
        RouteLC.SetFocus()
    else if TrainFocusGroup.HasFocus(AC) then
        TrainLC.SetFocus()
    else if TrainLC.HasFocus() then
        RouteLC.SetFocus();
end;

procedure TEditorFrame.FocusDown();
begin
    // TabStations
    if StationLC.HasFocus() then begin
        if StationPGC.ActivePage = TabStationSchedule then
            StationRoutesListView.SetFocus()
        else if StationPGC.ActivePage = TabDirections then
            DirectionLC.SetFocus();
    end
    // TabRoutes
    else if RouteLC.HasFocus() then begin
        if RoutesPGC.ActivePage = TabStops then
            StopsLC.SetFocus()
        else if RoutesPGC.ActivePage = TabTrains then
            TrainLC.SetFocus();
    end
    else if TrainLC.HasFocus() then begin
        TrainScheduleEditor.VT.SetFocus();
    end;
end;

procedure TEditorFrame.AssignData();
begin
    StationLC.AssignData(Timetable.Stations);
    RouteLC.EnableColors(EdAct.RouteLCGetColor);
    RouteLC.AssignData(Timetable.Routes);
end;

procedure TEditorFrame.TimeModeChanged(IsGameTime: Boolean);
begin
    TrainScheduleEditor.TimeModeChanged(IsGameTime);
    StopSchedule.TimeModeChanged(IsGameTime);
    StationSchedule.TimeModeChanged(IsGameTime);
    DirectionSchedule.TimeModeChanged(IsGameTime);
    StopParams.TimeModeChanged(IsGameTime);
end;

constructor TEditorFrame.Create(TheOwner: TComponent);
begin
    inherited Create(TheOwner);
    EdAct.Ed := Self;
    PGC.ActivePage := TabStations;

    StationPGC.Visible := False;
    StationPGC.ActivePage := TabStationSchedule;
    DirectionEPanel.Visible := False;

    RoutesPGC.Visible := False;
    RoutesPGC.ActivePage := TabStops;

    // Station tab LCs
    InitLC<TStation>(StationLC, StationLCBox, 'StationLC');
    InitLC<TDirection>(DirectionLC, DirectionLCBox, 'DirectionLC');

    // Station Schedule -> Routes through station
    StationRoutesBox.Color := clDefault;
    StationRoutesListView := TStationRoutesViewFrame.Create(StationRoutesBox,
        EdAct.StationLCFillRouteList, EdAct.EntityListViewGoto);
    StationRoutesListView.Parent := StationRoutesBox;
    // Station Schedule -> Schedule
    StationScheduleBox.Color := clDefault;
    StationSchedule := TStationScheduleViewFrame.Create(StationScheduleBox);
    StationSchedule.Parent := StationScheduleBox;

    // Directions -> Routes at direction
    DirectionRoutesBox.Color := clDefault;
    DirectionRoutesListView := TEntityListViewFrame.Create(DirectionRoutesBox,
        EdAct.DirectionLCFillRouteList, EdAct.EntityListViewGoto);
    DirectionRoutesListView.Parent := DirectionRoutesBox;
    // Directions -> Stations
    InitLC<TStation>(DirectionStationsLC, DirectionStationsLCBox,
                     'DirectionStationsLC');
    SetDirectionStationsLCParameters();
    // Directions -> Schedule
    DirectionScheduleBox.Color := clDefault;
    DirectionSchedule := TDirectionScheduleViewFrame.Create(DirectionScheduleBox);
    DirectionSchedule.Parent := DirectionScheduleBox;

    // Route LC
    InitLC<TRoute>(RouteLC, RouteLCBox, 'RouteLC');

    // Route stops LC
    InitLC<TStation>(StopsLC, StopLCBox, 'StopsLC');
    StopsLC.LCFrame.FilterEnabled := False;
    StopsLC.LCFrame.RenameEnabled := False;
    StopBox.Visible := False;
    // Route stops -> Stop Params
    StopParamsPanel.Color := clDefault;
    StopParams := TRouteStopParamsFrame.Create(StopParamsPanel);
    StopParams.Parent := StopParamsPanel;
    StopParams.NotifyColorChange := EdAct.StopParamsNotifyColorChange;
    // Route stops -> Stop Schedule View
    StopScheduleBox.Color := clDefault;
    StopSchedule := TStopScheduleViewFrame.Create(StopScheduleBox);
    StopSchedule.Parent := StopScheduleBox;
    StopSchedule.SyncFocus := EdAct.StopScheduleViewSyncSelection;
    // Stops reverse button
    StopsReverseBtn.Parent := StopsLC.LCFrame;
    StopsReverseBtn.AnchorParallel(akTop, 0, StopsLC.LCFrame.ListBox);
    StopsReverseBtn.AnchorHorizontalCenterTo(StopsLC.LCFrame.ButtonPanel);

    // Route trains LC
    InitLC<TTrain>(TrainLC, TrainLCBox, 'TrainLC');
    TrainScheduleBox.Color := clDefault;
    TrainScheduleBox.Visible := False;
    // Route trains -> Schedule Editor
    TrainScheduleEditor := TScheduleEditorFrame.Create(TrainScheduleBox);
    TrainScheduleEditor.Parent := TrainScheduleBox;
    StopParams.OnChange := EdAct.RouteStopParamsOnChange;
    TrainScheduleEditor.SyncFocus := EdAct.ScheduleEditorSyncSelection;
    TrainScheduleEditor.SyncView := EdAct.ReassignScheduleViews;
    TrainScheduleEditor.OnSetAsRouteDefault :=
        EdAct.ScheduleEditorSetAsRouteDefault;

    // Focus Groups
    StationScheduleFocusGroup := TFocusGroup.Create();
    DirectionsFocusGroup := TFocusGroup.Create();
    StopsFocusGroup := TFocusGroup.Create();
    TrainFocusGroup := TFocusGroup.Create();

    StationScheduleFocusGroup.AddWithChildrenRecursive(TabStationSchedule);
    DirectionsFocusGroup.AddWithChildrenRecursive(DirectionEPanel);
    StopsFocusGroup.AddWithChildrenRecursive(StopBox);
    TrainFocusGroup.AddWithChildrenRecursive(TrainScheduleBox);
end;

destructor TEditorFrame.Destroy();
begin
    FreeAndNil(StationScheduleFocusGroup);
    FreeAndNil(DirectionsFocusGroup);
    FreeAndNil(StopsFocusGroup);
    FreeAndNil(TrainFocusGroup);

    FreeAndNil(DirectionRoutesListView);
    FreeAndNil(DirectionStationsLC);
    FreeAndNil(DirectionSchedule);
    FreeAndNil(DirectionLC);
    FreeAndNil(StationRoutesListView);
    FreeAndNil(StationSchedule);
    FreeAndNil(StationLC);
    FreeAndNil(StopParams);
    FreeAndNil(StopSchedule);
    FreeAndNil(StopsLC);
    FreeAndNil(TrainScheduleEditor);
    FreeAndNil(TrainLC);
    FreeAndNil(RouteLC);
    inherited Destroy();
end;

{%REGION SwitchActions }

procedure TEditorFrame.SwitchTabStationsExecute(Sender: TObject);
begin
    PGC.ActivePage := TabStations;
    StationLC.SetFocus();
end;

procedure TEditorFrame.SwitchTabRoutesExecute(Sender: TObject);
begin
    PGC.ActivePage := TabRoutes;
    RouteLC.SetFocus();
end;

procedure TEditorFrame.SwitchTabStationScheduleExecute(Sender: TObject);
begin
    PGC.ActivePage := TabStations;
    StationPGC.ActivePage := TabStationSchedule;
    StationRoutesListView.SetFocus();
end;

procedure TEditorFrame.SwitchTabDirectionsExecute(Sender: TObject);
begin
    PGC.ActivePage := TabStations;
    StationPGC.ActivePage := TabDirections;
    DirectionLC.SetFocus();
end;

procedure TEditorFrame.SwitchTabStopsExecute(Sender: TObject);
begin
    PGC.ActivePage := TabRoutes;
    RoutesPGC.ActivePage := TabStops;
    StopsLC.SetFocus();
end;

procedure TEditorFrame.SwitchTabTrainsExecute(Sender: TObject);
begin
    PGC.ActivePage := TabRoutes;
    RoutesPGC.ActivePage := TabTrains;
    TrainLC.SetFocus();
end;

{%ENDREGION}

{%REGION ZoomActions }

procedure TEditorFrame.ZoomInActionExecute(Sender: TObject);
var VisibleSchedule: TScheduleViewFrame;
begin
    VisibleSchedule := GetVisibleScheduleView();
    if (VisibleSchedule <> nil) then
        VisibleSchedule.ZoomInAction.Execute();
end;

procedure TEditorFrame.ZoomOutActionExecute(Sender: TObject);
var VisibleSchedule: TScheduleViewFrame;
begin
    VisibleSchedule := GetVisibleScheduleView();
    if (VisibleSchedule <> nil) then
        VisibleSchedule.ZoomOutAction.Execute();
end;

procedure TEditorFrame.ZoomResetActionExecute(Sender: TObject);
var VisibleSchedule: TScheduleViewFrame;
begin
    VisibleSchedule := GetVisibleScheduleView();
    if (VisibleSchedule <> nil) then
        VisibleSchedule.ZoomResetAction.Execute();
end;

function TEditorFrame.GetVisibleScheduleView(): TScheduleViewFrame;
begin
    Result := nil;
    if (PGC.ActivePage = TabStations)
    and (StationLC.GetSelectedIndex() >= 0) then begin
        if StationPGC.ActivePage = TabStationSchedule then
            Result := StationSchedule
        else
        if (StationPGC.ActivePage = TabDirections)
        and (DirectionLC.GetSelectedIndex() >= 0) then
            Result := DirectionSchedule;
    end else
    if (PGC.ActivePage = TabRoutes)
    and (RouteLC.GetSelectedIndex() >= 0) then begin
        if (RoutesPGC.ActivePage = TabStops)
        and (StopsLC.GetSelectedIndex() >= 0) then
            Result := StopSchedule;
    end;
end;

{%ENDREGION}

end.

