unit UEditorLCActions;

{$mode ObjFPC}{$H+}

interface

uses
    Classes, SysUtils, Graphics,
    Editor, ListControl_Conn, EntityListView,
    UEntity, UID;

type

    { TEditorActions }
    TEditorActions = class(TObject)
    private
        function HasSelectedStation(): Boolean; inline;
        function HasSelectedDirection(): Boolean; inline;
        function HasSelectedRoute(): Boolean; inline;
        function HasSelectedTrain(): Boolean; inline;
        function HasSelectedStop(): Boolean; inline;

        function GetSelectedStation(out StationID: TID): Boolean; inline;
        function GetSelectedDirection(out DirectionID: TID): Boolean; inline;
        function GetSelectedRoute(out RouteID: TID): Boolean; inline;
        function GetSelectedTrain(out TrainID: TID): Boolean; inline;
        function GetSelectedStop(out StopIndex: SizeUInt): Boolean; inline;
        function GetSelectedStationPicker(out StationID: TID): Boolean; inline;
    public
        Ed: TEditorFrame;

        function StationLCAdd(var ID: TID; Index: SizeInt): Boolean;
        function StationLCRename(var ID: TID; Index: SizeInt): Boolean;
        function StationLCRemove(var ID: TID; Index: SizeInt): Boolean;
        function StationLCOnSelect(var ID: TID; Index: SizeInt): Boolean;
        function StationLCMove(var ID: TID; Index: SizeInt): Boolean;
        procedure StationLCFillRouteList(ID: TID; var ViewList: TEntityViewList;
                                        NeedDirections: Boolean);

        function RouteLCAdd(var ID: TID; Index: SizeInt): Boolean;
        function RouteLCRename(var ID: TID; Index: SizeInt): Boolean;
        function RouteLCRemove(var ID: TID; Index: SizeInt): Boolean;
        function RouteLCOnSelect(var ID: TID; Index: SizeInt): Boolean;
        function RouteLCMove(var ID: TID; Index: SizeInt): Boolean;
        procedure RouteLCGetColor(ID: TID; out BG, FG: TColor);

        function DirectionLCAdd(var ID: TID; Index: SizeInt): Boolean;
        function DirectionLCRemove(var ID: TID; Index: SizeInt): Boolean;
        function DirectionLCOnSelect(var ID: TID; Index: SizeInt): Boolean;
        function DirectionLCMove(var ID: TID; Index: SizeInt): Boolean;
        procedure DirectionLCFillRouteList(ID: TID; var ViewList: TEntityViewList);

        function TrainLCAdd(var ID: TID; Index: SizeInt): Boolean;
        function TrainLCRemove(var ID: TID; Index: SizeInt): Boolean;
        function TrainLCOnSelect(var ID: TID; Index: SizeInt): Boolean;
        function TrainLCMove(var ID: TID; Index: SizeInt): Boolean;

        function DirectionStationsLCAdd(var ID: TID; Index: SizeInt): Boolean;
        function DirectionStationsLCRemove(var ID: TID; Index: SizeInt): Boolean;
        function DirectionStationsLCMove(var ID: TID; Index: SizeInt): Boolean;

        function StopsLCTop(var ID: TID; Index: SizeInt): Boolean;
        function StopsLCUp(var ID: TID; Index: SizeInt): Boolean;
        function StopsLCAdd(var ID: TID; Index: SizeInt): Boolean;
        function StopsLCRemove(var ID: TID; Index: SizeInt): Boolean;
        function StopsLCOnSelect(var ID: TID; Index: SizeInt): Boolean;
        function StopsLCDown(var ID: TID; Index: SizeInt): Boolean;
        function StopsLCBottom(var ID: TID; Index: SizeInt): Boolean;

        procedure StopsReverse();

        procedure ReassignScheduleViews(Sender: TObject);
        procedure StopParamsNotifyColorChange(Sender: TObject);
        function ScheduleEditorSyncSelection(Index: Integer): Boolean;
        function ScheduleEditorSetAsRouteDefault(Index: Integer): Boolean;
        function StopScheduleViewSyncSelection(Index: Integer): Boolean;
        function StopScheduleReassign(): Boolean;
        function RouteStopParamsReassign(): Boolean;
        function RouteStopParamsOnChange(Index: Integer): Boolean;
        function StationScheduleReassign(): Boolean;
        function StationRoutesListViewReassign(): Boolean;
        function StationOnDirectionStationsChange(): Boolean;
        procedure EntityListViewGoto(const Entity: TEntity);
    end;

    function MakeLCCActionsFor(ConnName: String): TLCCActions;

var
    EdAct: TEditorActions;

implementation

uses
    Controls,
    Main, ListControl, PickerStation,
    UTimetable, URoute;

function MakeLCCActionsFor(ConnName: String): TLCCActions;
begin
    Result := Default(TLCCActions);
    case ConnName of
        'StationLC': begin
            Result[lcTop]      := @EdAct.StationLCMove;
            Result[lcUp]       := @EdAct.StationLCMove;
            Result[lcAdd]      := @EdAct.StationLCAdd;
            Result[lcRename]   := @EdAct.StationLCRename;
            Result[lcRemove]   := @EdAct.StationLCRemove;
            Result[lcOnSelect] := @EdAct.StationLCOnSelect;
            Result[lcDown]     := @EdAct.StationLCMove;
            Result[lcBottom]   := @EdAct.StationLCMove;
        end;
        'DirectionLC': begin
            Result[lcTop]      := @EdAct.DirectionLCMove;
            Result[lcUp]       := @EdAct.DirectionLCMove;
            Result[lcAdd]      := @EdAct.DirectionLCAdd;
            Result[lcRemove]   := @EdAct.DirectionLCRemove;
            Result[lcOnSelect] := @EdAct.DirectionLCOnSelect;
            Result[lcDown]     := @EdAct.DirectionLCMove;
            Result[lcBottom]   := @EdAct.DirectionLCMove;
        end;
        'DirectionStationsLC': begin
            Result[lcTop]      := @EdAct.DirectionStationsLCMove;
            Result[lcUp]       := @EdAct.DirectionStationsLCMove;
            Result[lcAdd]      := @EdAct.DirectionStationsLCAdd;
            Result[lcRemove]   := @EdAct.DirectionStationsLCRemove;
            Result[lcDown]     := @EdAct.DirectionStationsLCMove;
            Result[lcBottom]   := @EdAct.DirectionStationsLCMove;
        end;
        'RouteLC': begin
            Result[lcTop]      := @EdAct.RouteLCMove;
            Result[lcUp]       := @EdAct.RouteLCMove;
            Result[lcAdd]      := @EdAct.RouteLCAdd;
            Result[lcRename]   := @EdAct.RouteLCRename;
            Result[lcRemove]   := @EdAct.RouteLCRemove;
            Result[lcOnSelect] := @EdAct.RouteLCOnSelect;
            Result[lcDown]     := @EdAct.RouteLCMove;
            Result[lcBottom]   := @EdAct.RouteLCMove;
        end;
        'StopsLC': begin
            Result[lcTop]      := @EdAct.StopsLCTop;
            Result[lcUp]       := @EdAct.StopsLCUp;
            Result[lcAdd]      := @EdAct.StopsLCAdd;
            Result[lcRemove]   := @EdAct.StopsLCRemove;
            Result[lcOnSelect] := @EdAct.StopsLCOnSelect;
            Result[lcDown]     := @EdAct.StopsLCDown;
            Result[lcBottom]   := @EdAct.StopsLCBottom;
        end;
        'TrainLC': begin
            Result[lcTop]      := @EdAct.TrainLCMove;
            Result[lcUp]       := @EdAct.TrainLCMove;
            Result[lcAdd]      := @EdAct.TrainLCAdd;
            Result[lcRemove]   := @EdAct.TrainLCRemove;
            Result[lcOnSelect] := @EdAct.TrainLCOnSelect;
            Result[lcDown]     := @EdAct.TrainLCMove;
            Result[lcBottom]   := @EdAct.TrainLCMove;
        end;
    end;
end;

{ TEditorActions }

function TEditorActions.HasSelectedStation(): Boolean;
begin
    Result := Ed.StationLC.GetSelectedID() > 0;
end;

function TEditorActions.HasSelectedDirection(): Boolean;
begin
    Result := Ed.DirectionLC.GetSelectedID() > 0;
end;

function TEditorActions.HasSelectedRoute(): Boolean;
begin
    Result := Ed.RouteLC.GetSelectedID() > 0;
end;

function TEditorActions.HasSelectedTrain(): Boolean;
begin
    Result := Ed.TrainLC.GetSelectedID() > 0;
end;

function TEditorActions.HasSelectedStop(): Boolean;
begin
    Result := Ed.StopsLC.GetSelectedIndex() >= 0;
end;

function TEditorActions.GetSelectedStation(out StationID: TID): Boolean;
begin
    StationID := Ed.StationLC.GetSelectedID();
    Result := StationID > 0;
end;

function TEditorActions.GetSelectedDirection(out DirectionID: TID): Boolean;
begin
    DirectionID := Ed.DirectionLC.GetSelectedID();
    Result := DirectionID > 0;
end;

function TEditorActions.GetSelectedRoute(out RouteID: TID): Boolean;
begin
    RouteID := Ed.RouteLC.GetSelectedID();
    Result := RouteID > 0;
end;

function TEditorActions.GetSelectedTrain(out TrainID: TID): Boolean;
begin
    TrainID := Ed.TrainLC.GetSelectedID();
    Result := TrainID > 0;
end;

function TEditorActions.GetSelectedStop(out StopIndex: SizeUInt): Boolean;
var Index: SizeInt;
begin
    Index := Ed.StopsLC.GetSelectedIndex();
    Result := Index >= 0;
    if Result then StopIndex := Index;
end;

function TEditorActions.GetSelectedStationPicker(out StationID: TID): Boolean;
begin
    StationID := PickerStationForm.SelectedID;
    Result := StationID > 0;
end;

function TEditorActions.StationLCAdd(var ID: TID; Index: SizeInt): Boolean;
begin
    MainForm.FileChanged();
    Result := Timetable.AddStation('', Index) > 0;
end;

function TEditorActions.StationLCRename(var ID: TID; Index: SizeInt): Boolean;
begin
    MainForm.FileChanged();
    Result := True;

    if not HasSelectedRoute() then exit;
    Ed.StopsLC.RenameByID(ID);

    if not HasSelectedTrain() then exit;
    Ed.TrainScheduleEditor.Reassign();
end;

function TEditorActions.StationLCRemove(var ID: TID; Index: SizeInt): Boolean;
var SelectedRoute, SelectedTrain: TID;
begin
    MainForm.FileChanged();
    Result := Timetable.RemoveStation(ID);
    if not Result then exit;

    if not GetSelectedRoute(SelectedRoute) then exit;
    Ed.StopsLC.AssignData(Timetable.Routes[SelectedRoute].Stops,
                          Timetable.Stations);

    if GetSelectedTrain(SelectedTrain) then
        Ed.TrainScheduleEditor.AssignData(SelectedRoute, SelectedTrain);

    RouteStopParamsReassign();
end;

function TEditorActions.StationLCOnSelect(var ID: TID; Index: SizeInt): Boolean;
var Enable: Boolean;
begin
    Enable := ID > 0;
    if Enable then
        with Timetable do
        begin
            Ed.StationRoutesListView.AssignData(ID);
            Ed.StationSchedule.AssignData(ID);
            Ed.DirectionLC.AssignData(Stations[ID].Directions);
            Ed.DirectionStationsLC.ClearData();
        end;
    Ed.StationPGC.Visible := Enable;
    Result := True;

    Ed.SwitchTabStationSchedule.Enabled := Enable;
    Ed.SwitchTabDirections.Enabled := Enable;
end;

function TEditorActions.StationLCMove(var ID: TID; Index: SizeInt): Boolean;
begin
    MainForm.FileChanged();
    Result := True;
end;

procedure TEditorActions.StationLCFillRouteList(ID: TID;
    var ViewList: TEntityViewList; NeedDirections: Boolean);
begin
    if not Timetable.Stations.Exists(ID) then exit;
    Timetable.Stations[ID].GetViewList(ViewList, NeedDirections);
end;

function TEditorActions.RouteLCAdd(var ID: TID; Index: SizeInt): Boolean;
begin
    MainForm.FileChanged();
    Result := Timetable.AddRoute('', Index) > 0;
end;

function TEditorActions.RouteLCRename(var ID: TID; Index: SizeInt): Boolean;
begin
    MainForm.FileChanged();
    Result := StationRoutesListViewReassign();
end;

function TEditorActions.RouteLCRemove(var ID: TID; Index: SizeInt): Boolean;
begin
    MainForm.FileChanged();
    Result := Timetable.RemoveRoute(ID);
    StationRoutesListViewReassign();
end;

function TEditorActions.RouteLCOnSelect(var ID: TID; Index: SizeInt): Boolean;
var Enable: Boolean;
begin
    Enable := ID > 0;
    if Enable then
    begin
        Ed.TrainLC.AssignData(Timetable.Routes[ID].Trains);
        Ed.StopsLC.AssignData(Timetable.Routes[ID].Stops,
                              Timetable.Stations);
    end;
    Ed.RoutesPGC.Visible := Enable;
    Result := True;

    Ed.SwitchTabStops.Enabled := Enable;
    Ed.SwitchTabTrains.Enabled := Enable;
end;

function TEditorActions.RouteLCMove(var ID: TID; Index: SizeInt): Boolean;
begin
    MainForm.FileChanged();
    Timetable.UpdateAllStationsRoutes();
    Result := StationRoutesListViewReassign();
end;

procedure TEditorActions.RouteLCGetColor(ID: TID; out BG, FG: TColor);
begin
    if not Timetable.Routes.Exists(ID) then exit;
    Timetable.Routes[ID].GetColor(BG, FG);
end;

function TEditorActions.DirectionLCAdd(var ID: TID; Index: SizeInt): Boolean;
var SelectedStation: TID;
begin
    MainForm.FileChanged();
    Result := GetSelectedStation(SelectedStation);
    if not Result then exit;

    Result := Timetable.Stations[SelectedStation].AddDirection('', Index) > 0;
end;

function TEditorActions.DirectionLCRemove(var ID: TID; Index: SizeInt): Boolean;
var SelectedStation: TID;
begin
    MainForm.FileChanged();
    Result := GetSelectedStation(SelectedStation);
    if not Result then exit;

    Result := Timetable.Stations[SelectedStation].RemoveDirection(ID);
    if not Result then exit;

    StationOnDirectionStationsChange();
    RouteStopParamsReassign();
end;

function TEditorActions.DirectionLCOnSelect(var ID: TID; Index: SizeInt): Boolean;
var SelectedStation: TID;
begin
    if not GetSelectedStation(SelectedStation) then exit;
    if ID > 0 then
    begin
        Ed.DirectionRoutesListView.AssignData(ID);
        Ed.DirectionStationsLC.AssignData(
            Ed.DirectionLC.EntityList[ID].GetStations(), Timetable.Stations);
        Ed.DirectionSchedule.AssignData(SelectedStation, ID);
    end;
    Ed.DirectionEPanel.Visible := ID > 0;

    if Ed.DirectionLC.LCFrame.ListBox.Focused and (Index = -1) then
        Ed.StationLC.LCFrame.ListBox.SetFocus();
    Result := True;
end;

function TEditorActions.DirectionLCMove(var ID: TID; Index: SizeInt): Boolean;
begin
    MainForm.FileChanged();
    Result := StationScheduleReassign();
end;

procedure TEditorActions.DirectionLCFillRouteList(ID: TID; var ViewList: TEntityViewList);
var SelectedStation: TID;
begin
    if not GetSelectedStation(SelectedStation) then exit;
    if not Timetable.Stations[SelectedStation].Directions.Exists(ID) then exit;
    Timetable.Stations[SelectedStation].Directions[ID].GetViewList(ViewList);
end;

function TEditorActions.TrainLCAdd(var ID: TID; Index: SizeInt): Boolean;
var SelectedRoute, NewTrainID: TID;
begin
    Result := GetSelectedRoute(SelectedRoute);
    if not Result then exit;

    NewTrainID := Timetable.Routes[SelectedRoute].AddTrain('', Index);
    Result := NewTrainID > 0;
    if not Result then exit;

    Timetable.Routes[SelectedRoute].Trains[NewTrainID].InitSchedule();
    Ed.StopSchedule.Reassign();
end;

function TEditorActions.TrainLCRemove(var ID: TID; Index: SizeInt): Boolean;
var SelectedRoute: TID;
begin
    Result := GetSelectedRoute(SelectedRoute);
    if not Result then exit;

    Result := Timetable.Routes[SelectedRoute].RemoveTrain(ID);
    Ed.StopSchedule.Reassign();
    StationRoutesListViewReassign();
end;

function TEditorActions.TrainLCOnSelect(var ID: TID; Index: SizeInt): Boolean;
var SelectedRoute: TID;
begin
    Result := GetSelectedRoute(SelectedRoute);
    if not Result then exit;

    if ID > 0 then
        Ed.TrainScheduleEditor.AssignData(SelectedRoute, ID);
    Ed.TrainScheduleBox.Visible := ID > 0;

    if Ed.TrainLC.LCFrame.ListBox.Focused and (Index = -1) then
        Ed.RouteLC.LCFrame.ListBox.SetFocus();
    Result := True;
end;

function TEditorActions.TrainLCMove(var ID: TID; Index: SizeInt): Boolean;
begin
    MainForm.FileChanged();
    Result := StopScheduleReassign();
end;

function TEditorActions.DirectionStationsLCAdd(var ID: TID;
    Index: SizeInt): Boolean;
var SelectedStation, SelectedDirection: TID;
begin
    MainForm.FileChanged();
    Result := GetSelectedStation(SelectedStation);
    if not Result then exit;

    Result := GetSelectedDirection(SelectedDirection);
    if not Result then exit;

    PickerStationForm.LC.AssignData(Timetable.Stations);
    Result := PickerStationForm.ShowModal() = mrOk;
    if not Result then exit;

    Result := GetSelectedStationPicker(ID);
    if not Result then exit;

    Timetable.Stations[SelectedStation].Directions[SelectedDirection]
             .AddStation(ID, Index);

    StationOnDirectionStationsChange();
    RouteStopParamsReassign();
end;

function TEditorActions.DirectionStationsLCRemove(var ID: TID;
    Index: SizeInt): Boolean;
var SelectedStation, SelectedDirection: TID;
begin
    MainForm.FileChanged();
    Result := GetSelectedStation(SelectedStation);
    if not Result then exit;

    Result := GetSelectedDirection(SelectedDirection);
    if not Result then exit;

    Timetable.Stations[SelectedStation].Directions[SelectedDirection]
             .RemoveStation(ID, Index);

    StationOnDirectionStationsChange();
    RouteStopParamsReassign();
end;

function TEditorActions.DirectionStationsLCMove(var ID: TID; Index: SizeInt): Boolean;
begin
    MainForm.FileChanged();
    Result := StationOnDirectionStationsChange();
    RouteStopParamsReassign();
end;

function TEditorActions.StopsLCTop(var ID: TID; Index: SizeInt): Boolean;
var SelectedRoute: TID;
begin
    MainForm.FileChanged();
    Result := GetSelectedRoute(SelectedRoute);
    if not Result then exit;

    Timetable.Routes[SelectedRoute].MoveStopTime(Index, 0);
    Ed.TrainScheduleEditor.MoveNodeAt(Index, 0);

    Timetable.UpdateStationsDirectionsRoutesByRoute(SelectedRoute);
    StationRoutesListViewReassign();
    RouteStopParamsReassign();
end;

function TEditorActions.StopsLCUp(var ID: TID; Index: SizeInt): Boolean;
var SelectedRoute: TID;
begin
    MainForm.FileChanged();
    Result := GetSelectedRoute(SelectedRoute);
    if not Result then exit;

    Timetable.Routes[SelectedRoute].MoveStopTime(Index, Index - 1);
    Ed.TrainScheduleEditor.MoveNodeAt(Index, Index - 1);

    Timetable.UpdateStationsDirectionsRoutesByRoute(SelectedRoute);
    StationRoutesListViewReassign();
    RouteStopParamsReassign();
end;

function TEditorActions.StopsLCAdd(var ID: TID; Index: SizeInt): Boolean;
var SelectedRoute: TID;
begin
    MainForm.FileChanged();
    Result := GetSelectedRoute(SelectedRoute);
    if not Result then exit;

    PickerStationForm.LC.AssignData(Timetable.Stations);
    Result := PickerStationForm.ShowModal() = mrOk;
    if not Result then exit;

    Result := GetSelectedStationPicker(ID);
    if not Result then exit;

    Timetable.Routes[SelectedRoute].AddStop(ID, Index);

    if HasSelectedTrain() then
        Ed.TrainScheduleEditor.InsertNodeAt(Index);

    Timetable.UpdateStationsDirectionsRoutesByRoute(SelectedRoute);
    StationRoutesListViewReassign();
    RouteStopParamsReassign();
end;

function TEditorActions.StopsLCRemove(var ID: TID; Index: SizeInt): Boolean;
var SelectedRoute: TID;
begin
    MainForm.FileChanged();
    Result := GetSelectedRoute(SelectedRoute);
    if not Result then exit;

    Timetable.Routes[SelectedRoute].RemoveStop(ID, Index);

    if HasSelectedTrain() then
        Ed.TrainScheduleEditor.DeleteNodeAt(Index);

    Timetable.UpdateStationsDirectionsRoutesByRoute(SelectedRoute);
    StationRoutesListViewReassign();
end;

function TEditorActions.StopsLCOnSelect(var ID: TID; Index: SizeInt): Boolean;
var SelectedRoute: TID;
begin
    Result := GetSelectedRoute(SelectedRoute);
    if not Result then exit;

    if ID > 0 then
    begin
        Ed.StopParams.AssignData(SelectedRoute, Index);
        Ed.StopSchedule.AssignData(SelectedRoute, Index);
    end;
    Ed.StopBox.Visible := ID > 0;

    if Ed.StopsLC.LCFrame.ListBox.Focused and (Index = -1) then
        Ed.RouteLC.LCFrame.ListBox.SetFocus();
    Result := True;
end;

function TEditorActions.StopsLCDown(var ID: TID; Index: SizeInt): Boolean;
var SelectedRoute: TID;
begin
    MainForm.FileChanged();
    Result := GetSelectedRoute(SelectedRoute);
    if not Result then exit;

    Timetable.Routes[SelectedRoute].MoveStopTime(Index, Index + 1);
    Ed.TrainScheduleEditor.MoveNodeAt(Index, Index + 1);

    Timetable.UpdateStationsDirectionsRoutesByRoute(SelectedRoute);
    StationRoutesListViewReassign();
    RouteStopParamsReassign();
end;

function TEditorActions.StopsLCBottom(var ID: TID; Index: SizeInt): Boolean;
var
    SelectedRoute: TID;
    StopsCount: SizeInt;
begin
    MainForm.FileChanged();
    Result := GetSelectedRoute(SelectedRoute);
    if not Result then exit;

    StopsCount := Timetable.Routes[SelectedRoute].Stops.Count;
    Timetable.Routes[SelectedRoute].MoveStopTime(Index, StopsCount - 1);
    Ed.TrainScheduleEditor.MoveNodeAt(Index, StopsCount - 1);

    Timetable.UpdateStationsDirectionsRoutesByRoute(SelectedRoute);
    StationRoutesListViewReassign();
    RouteStopParamsReassign();
end;

procedure TEditorActions.StopsReverse();
var SelectedRoute: TID;
begin
    MainForm.FileChanged();
    if not GetSelectedRoute(SelectedRoute) then exit;

    Timetable.Routes[SelectedRoute].AddReverseTrip();
    Ed.StopsLC.AssignData(Ed.StopsLC.IDList, Ed.StopsLC.EntityList);

    Timetable.UpdateStationsDirectionsRoutesByRoute(SelectedRoute);
    StationRoutesListViewReassign();
    RouteStopParamsReassign();
end;

procedure TEditorActions.ReassignScheduleViews(Sender: TObject);
begin
    if HasSelectedStation() then begin
        Ed.StationSchedule.Reassign();
        if HasSelectedDirection() then
            Ed.DirectionSchedule.Reassign();
    end;
    if HasSelectedRoute() then
        Ed.StopSchedule.Reassign();
end;

procedure TEditorActions.StopParamsNotifyColorChange(Sender: TObject);
begin
    MainForm.FileChanged();
    Ed.RouteLC.LCFrame.ListBox.Invalidate();
    Ed.StationRoutesListView.ListBox.Invalidate();
    Ed.StationSchedule.Invalidate();
    Ed.DirectionRoutesListView.ListBox.Invalidate();
    Ed.DirectionSchedule.Invalidate();
end;

function TEditorActions.ScheduleEditorSyncSelection(Index: Integer): Boolean;
begin
    Ed.StopsLC.LCFrame.ListBox.ItemIndex := Index;
    Result := True;
end;

function TEditorActions.ScheduleEditorSetAsRouteDefault(Index: Integer): Boolean;
var
    SelectedRoute, StationID: TID;
    SelectedStop: SizeUInt;
    Route: TRoute;
begin
    MainForm.FileChanged();
    Result := GetSelectedRoute(SelectedRoute);
    if not Result then exit;
    Route := Timetable.Routes[SelectedRoute];

    Route.ValidateTotalTimes();

    if Index >= Route.Stops.Count then exit;
    StationID := Route.Stops[Index];
    Timetable.Stations[StationID].UpdateDirectionsRoutes();

    if GetSelectedStop(SelectedStop) and (SelectedStop = Index) then
        RouteStopParamsReassign();
end;

function TEditorActions.StopScheduleViewSyncSelection(Index: Integer): Boolean;
begin
    Ed.TrainLC.LCFrame.ListBox.ItemIndex := Index;
    Result := True;
end;

function TEditorActions.StopScheduleReassign(): Boolean;
begin
    Result := HasSelectedRoute();
    if not Result then exit;

    Ed.StopSchedule.Reassign();
end;

function TEditorActions.RouteStopParamsReassign(): Boolean;
begin
    Result := True;
    if HasSelectedStop() then
        Ed.StopParams.Reassign();
end;

function TEditorActions.RouteStopParamsOnChange(Index: Integer): Boolean;
begin
    Result := True;
    MainForm.FileChanged();
    Ed.TrainScheduleEditor.Reassign();
    Timetable.UpdateAllStationsDirectionsRoutes();
    StationRoutesListViewReassign();
end;

function TEditorActions.StationScheduleReassign(): Boolean;
begin
    Result := HasSelectedStation();
    if not Result then exit;

    Ed.StationSchedule.Reassign();
end;

function TEditorActions.StationRoutesListViewReassign(): Boolean;
begin
    Result := True;
    if HasSelectedStation() then begin
        Ed.StationRoutesListView.Reassign();
        Ed.StationSchedule.Reassign();
        if HasSelectedDirection() then
        begin
            Ed.DirectionRoutesListView.Reassign();
            Ed.DirectionSchedule.Reassign();
        end;
    end;
end;

function TEditorActions.StationOnDirectionStationsChange(): Boolean;
var SelectedStation: TID;
begin
    MainForm.FileChanged();
    if GetSelectedStation(SelectedStation) then
        Timetable.Stations[SelectedStation].OnDirectionStationsChange(Self);
    Result := StationRoutesListViewReassign();
end;

procedure TEditorActions.EntityListViewGoto(const Entity: TEntity);
begin
    case Entity.EType of
        direction: begin
            // we can double-click direction only if the corresponding station
            // is already selected; fix if that would change
            if not Ed.SwitchTabDirections.Execute() then exit;
            Ed.DirectionLC.SetOnID(Entity.ID);
        end;
        route: begin
            if not Ed.SwitchTabRoutes.Execute() then exit;
            Ed.RouteLC.SetOnID(Entity.ID);
        end;
    end;
end;

initialization
    EdAct := TEditorActions.Create();

finalization
    FreeAndNil(EdAct);

end.

