unit HMTSetter;

{$mode ObjFPC}{$H+}

interface

uses
    Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls,
    Buttons,
    USTime,
    HMTEditFrame;

type

    { THMTSetterForm }

    THMTSetterForm = class(TForm)
        OKBtn: TBitBtn;
        CancelBtn: TBitBtn;
        HMTEditPanel: TPanel;
        procedure FormKeyDown(Sender: TObject; var Key: Word;
            Shift: TShiftState);
        procedure FormShow(Sender: TObject);
    public
        HMTEdit: THMTEditFrame;
        procedure AssignData(const _Value: THMTime);
        function Edit(const _Value: THMTime): THMTime;

        constructor Create(TheOwner: TComponent); override;
        destructor Destroy(); override;
    end;

var
    HMTSetterForm: THMTSetterForm;

implementation

uses LCLType;

{$R *.lfm}

{ THMTSetterForm }

procedure THMTSetterForm.FormKeyDown(Sender: TObject; var Key: Word;
    Shift: TShiftState);
begin
    case Key of
        VK_ESCAPE: CancelBtn.Click();
        VK_RETURN: OkBtn.Click();
    end;
end;

procedure THMTSetterForm.FormShow(Sender: TObject);
begin
    HMTEdit.HSpinEditEx.SetFocus();
end;

procedure THMTSetterForm.AssignData(const _Value: THMTime);
begin
    HMTEdit.AssignData(_Value);
end;

function THMTSetterForm.Edit(const _Value: THMTime): THMTime;
begin
    AssignData(_Value);
    if ShowModal() = mrOK then
        Result := HMTEdit.RetrieveData()
    else
        Result := _Value;
end;

constructor THMTSetterForm.Create(TheOwner: TComponent);
begin
    inherited Create(TheOwner);
    HMTEditPanel.Color := clDefault;
    HMTEdit := THMTEditFrame.Create(HMTEditPanel);
    HMTEdit.Parent := Self;
end;

destructor THMTSetterForm.Destroy();
begin
    FreeAndNil(HMTEdit);
    inherited Destroy();
end;

end.

