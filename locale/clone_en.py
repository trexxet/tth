lines = []
linesCopy = []
replaceNext = False
copyNext = False
replaceStr = ''

with open('tth.en.po', 'r') as file:
    lines = [line.rstrip() for line in file]

for i, line in enumerate(lines): 
    if replaceNext and line.startswith('msgstr'):
        lines[i] = line.replace('""', replaceStr)
        replaceNext = False
    elif copyNext:
        if line.startswith('msgstr'):
            copyNext = False
            lines[i + 1 : i + 1] = linesCopy
            linesCopy.clear()
        else:
            linesCopy.append(line)
    elif line.startswith('msgid'):
        replaceStr = line[line.find('"') : line.rfind('"') + 1]
        replaceNext = replaceStr != '""'
        copyNext = not replaceNext

with open('tth1.en.po', 'w') as file:
    for line in lines:
        file.write(f'{line}\n')