unit Main_RS;

{$mode ObjFPC}{$H+}

interface

resourcestring
    MainRS_OpenFail = 'Failed to open or load the file';
    MainRS_SaveFail = 'Failed to save to file';

    MainRS_UnsavedCaption        = 'Unsaved changes';
    MainRS_UnsavedCreateQuestion = 'Do you want to create another timetable without saving the current one?';
    MainRS_UnsavedOpenQuestion   = 'Do you want to open another timetable without saving the current one?';
    MainRS_UnsavedExitQuestion   = 'Do you want to exit without saving your changes?';

implementation

end.

