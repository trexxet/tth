unit ScheduleEditor_RS;

{$mode ObjFPC}{$H+}

interface

resourcestring
    TNS_RS_StopHint       = 'Do stop at station';
    TNS_RS_NonStopHint    = 'Pass station without stopping';

    StopAutoCalcRS_Arr    = 'Arrival = previous Departure + previous Travel';
    StopAutoCalcRS_Stop   = 'Stopping = Departure - Arrival';
    StopAutoCalcRS_Dep    = 'Departure = Arrival + Stopping';
    StopAutoCalcRS_Trav   = 'Travel = next Arrival - Departure';
    StopAutoCalcRS_FArr   = 'Can''t auto calculate Arrival for first stop';
    StopAutoCalcRS_URD    = 'Can''t auto calculate time with Use Route Default parameter';

    StopTravRS_OK         = 'Can auto calculate all Stopping && Travel times';
    StopTravRS_NeedNoAC   = 'To auto calculate Stopping && Travel, disable Use Route Defalut for them, and disable auto calculation for all Arrival && Departure';

    TotalTimeRS_OK        = 'Makes a perfect loop %u times a day';
    TotalTimeRS_OK_Inv    = 'Makes a perfect loop in %u days';
    TotalTimeRS_Off       = 'Off by %s%s';
    TotalTimeRS_Unknown   = 'Offset unknown';

    ArrDepRS_OK           = 'Can auto calculate all Arrival && Departure times';
    ArrDepRS_NeedURD      = 'To auto calculate Arrival && Departure, set all Stopping && Travel times as Use Route Default or not auto calculated';
    ArrDepRS_Need1Loop    = 'To auto calculate Arrival && Departure, a perfect loop not longer than a day is required';
    ArrDepRS_FirstArrAC   = 'To auto calculate Arrival && Departure, disable auto calculation of Arrival for first stop';
    ArrDepRS_LoopTooShort = 'To auto calculate Arrival && Departure, the perfect loop must be at least 1 hour long';
    ArrDepRS_Empty        = 'Schedule is empty';

implementation

end.

