unit UEntity_RS;

{$mode ObjFPC}{$H+}

interface

resourcestring
    UEntityRS_NewStation   = 'New Station';
    UEntityRS_NewDirection = 'New Direction';
    UEntityRS_NewRoute     = 'New Route';
    UEntityRS_NewTrain     = 'New Train';
    UEntityRS_NewFake      = 'New Fake Entity';
    UEntityRS_NewError     = 'ERROR';

    UEntityRS_NoDirection  = 'no direction';
    UEntityRS_DoesStop     = 'Does stop';
    UEntityRS_NonStop      = 'Non stop';

implementation

end.

