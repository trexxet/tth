# TTH

Timetable Helper is a program designed to help you in making train timetables for transport simulation games.

Initially, the program was created with an eye on the mod [Timetables 1.3](https://steamcommunity.com/sharedfiles/filedetails/?id=2918585390) for the game Transport Fever 2. The program may also happen to be convenient to use for others games (for example, JGRPP for OpenTTD), or for any other purposes.

The latest release is available [here](https://gitlab.com/trexxet/tth/-/releases).

The manual (in English and Russian) is available [here](https://gitlab.com/trexxet/tth-manual/-/blob/main/README.md?ref_type=heads).

This program uses icons from the free KDE Oxygen icon set (GNU LGPL 3) and [icons8.ru](https://icons8.ru/icons) website.