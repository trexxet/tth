unit VTUtils;

{$mode ObjFPC}{$H+}

interface

uses
    Classes, SysUtils, Graphics, Controls, laz.VirtualTrees;

    procedure DrawIndicator(Canvas: TCanvas; CellRect: TRect;
        Icons: TImageList; Focused: Boolean);
    procedure DrawCaption(Canvas: TCanvas; ContentRect: TRect; Text: String;
        AlignRight: Boolean);

    // How the fuck VT does not have these methods?
    function NodeAt(VT: TLazVirtualDrawTree; Index: SizeInt): PVirtualNode;
    procedure ResetAllNodes(VT: TLazVirtualDrawTree);

const
    sch_clEntityName = TColor($EDDEDE);
    sch_clInvalid    = TColor($C0C0FF);
    sch_clRouteTime  = TColor($DEEDDE);
    sch_clUneditable = TColor($D1D1D1);
    sch_clGhost      = TColor($EAEAEA);

implementation

uses
    Math, LCLIntf, LCLType;

procedure DrawIndicator(Canvas: TCanvas; CellRect: TRect; Icons: TImageList;
    Focused: Boolean);
begin
    InflateRect(CellRect, 1, 1);
    DrawEdge(Canvas.Handle, CellRect, BDR_RAISED, BF_RECT or BF_MIDDLE);
    if Focused then
        Icons.Draw(Canvas,
                   (CellRect.Width - Icons.Width) div 2,
                   (CellRect.Height - Icons.Height) div 2, 0);
end;

procedure DrawCaption(Canvas: TCanvas; ContentRect: TRect; Text: String;
    AlignRight: Boolean);
var
    TextRect: TRect;
    TextAlign: Byte;
begin
    Canvas.Font.Height := 0;
    TextRect := ContentRect;
    TextAlign := specialize IfThen<Byte>(AlignRight, DT_RIGHT, DT_CENTER);
    DrawText(Canvas.Handle, PChar(Text), Length(Text), TextRect,
             TextAlign or DT_CALCRECT or DT_WORDBREAK);
    with TextRect do
    begin
        Top := (ContentRect.Bottom - Bottom) div 2;
        Bottom := ContentRect.Bottom;
        Right := ContentRect.Right;
    end;
    DrawText(Canvas.Handle, PChar(Text), Length(Text), TextRect,
             TextAlign or DT_WORDBREAK);
end;

function NodeAt(VT: TLazVirtualDrawTree; Index: SizeInt): PVirtualNode;
var PNode: PVirtualNode;
begin
    Result := nil;
    if Index >= VT.RootNodeCount then exit;
    PNode := VT.GetFirst();
    while Index > 0 do
    begin
        PNode := VT.GetNext(PNode);
        Index -= 1;
    end;
    Result := PNode;
end;

procedure ResetAllNodes(VT: TLazVirtualDrawTree);
var PNode: PVirtualNode;
begin
    PNode := VT.GetFirst();
    while PNode <> nil do
    begin
        VT.ResetNode(PNode);
        PNode := VT.GetNext(PNode);
    end;
end;

end.

