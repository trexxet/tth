unit UColorUtils;

{$mode ObjFPC}{$H+}

interface

uses
    SysUtils, Graphics;

    function GetContrastFGColor(const BG: TColor): TColor;

implementation

uses Math;

function GetContrastFGColor(const BG: TColor): TColor;
var
    sR, sG, sB: Byte;
    R, G, B, Luminance: Double;

    function Linearize(C: Byte): Double; inline;
    begin
        Result := Power(C/255.0, 2.2);
    end;

begin
    RedGreenBlue(BG, sR, sG, sB);
    R := Linearize(sR);
    G := Linearize(sG);
    B := Linearize(sB);
    Luminance := R * 0.2126 + G * 0.7152 + B * 0.0722;
    Result := specialize IfThen<TColor>(Luminance < 0.36, clWhite, clBlack);
end;

end.

