unit UVersion;

{$mode ObjFPC}{$H+}

interface

uses
    SysUtils;

const
    TTH_NAME = 'Timetable Helper';
    TTH_NAME_SHORT = 'TTH';

    TTH_VERSION_MAJOR = 1;
    TTH_VERSION_MINOR = 0;
    TTH_VERSION_FIX   = 1;

    TTH_VERSION_YEAR_START = 2024;
    TTH_VERSION_YEAR_END = 2024;
    TTH_VERSION_AUTHOR = 'trexxet';

    TTH_LICENCSE = 'WTFPL License';

    function GetTTHVersion(): String; inline;
    function GetTTHAuthor(): String; inline;

implementation

function GetTTHVersion(): String;
begin
    Result := Format('%d.%d.%d', [TTH_VERSION_MAJOR, TTH_VERSION_MINOR, TTH_VERSION_FIX]);
end;

function GetTTHAuthor(): String;
begin
    Result := Format('(c) %s %d', [TTH_VERSION_AUTHOR, TTH_VERSION_YEAR_START]);
    //if TTH_VERSION_YEAR_END > TTH_VERSION_YEAR_START then
    //    Result := Format('%s-%d', [Result, TTH_VERSION_YEAR_END]);
end;

end.

