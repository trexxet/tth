unit UIndent;

{$mode ObjFPC}{$H+}

interface

uses
    SysUtils;

function Reindent(var S: String): String;

implementation

function MakeIndents(size: SizeUInt): String;
begin
    Result := '';
    while size > 0 do
    begin
        Result += '    ';
        size -= 1;
    end;
end;

procedure InsertAndShift(const Source: String; var S: String;
    var counter: SizeUInt);
begin
    Insert(Source, S, counter);
    counter += Length(Source);
end;

function Reindent(var S: String): String;
var
    i, indent: SizeUInt;
    disableIndent: Boolean = False;
begin
    indent := 0;
    i := Low(S);
    while i <= High(S) do
    begin
        if not disableIndent then
        begin
            case S[i] of
                '{': // increase indent
                    begin
                        if (i+3 <= High(S)) and (S[i+3] <> '}') then
                        begin
                            indent += 1;
                            i += 2;
                            InsertAndShift(#10 + MakeIndents(indent), S, i);
                        end
                        else i += 4; // skip {  }
                        continue;
                    end;
                '}': // decrease indent
                    begin
                        indent -= 1;
                        InsertAndShift(#10 + MakeIndents(indent), S, i);
                        i += 1;
                        continue;
                    end;
                ';': // new line
                    begin
                        if (i+1 <= High(S)) and (S[i+1] = ' ') then
                        begin
                            i += 2;
                            if (i <= High(S)) and (S[i] = '}') then
                                continue; // no new line for last element
                            InsertAndShift(#10 + MakeIndents(indent), S, i);
                        end;
                        continue;
                    end;
            end;
        end;
        if S[i] = '"' then disableIndent := not disableIndent;
        i += 1;
    end;
    Result := S;
end;

end.

