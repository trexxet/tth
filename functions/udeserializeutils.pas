unit UDeserializeUtils;

{$mode ObjFPC}{$H+}

interface

uses
    Classes, SysUtils, UID;

// Functions operate 0-based index

type ExtractFieldMode = (efValue, efBlock);

procedure TrimBrackets(var S: String);

// -1 = position unknown, let LocateBracketBlock decide
procedure ExtractBracketBlock(var S: String; out D: String;
                             BlockStart: SizeInt = -1);

procedure ExtractField(const F: String; var S: String;
                       out D: String; mode: ExtractFieldMode);

procedure ExtractIDList(var S: String; var D: TIDList);

function DelSpaceUnqoted(var S: String): Boolean;

implementation

procedure TrimBrackets(var S: String);
begin
    if (Length(S) < 2) or (S[Low(S)] <> '{') or (S[High(S)] <> '}') then
        raise EFormatError.Create({$I %CURRENTROUTINE%});
    S := S.Remove(Low(S) - 1, 1);
    S := S.Remove(High(S) - 1, 1);
end;

procedure LocateBracketBlock(const S: String; SearchStart: SizeInt;
    out BlockStart, BlockEnd: SizeInt);
var nest: SizeInt = 1;
begin
    BlockStart := S.IndexOf('{', SearchStart);
    BlockEnd := BlockStart;
    while BlockEnd >= 0 do
    begin
        BlockEnd := S.IndexOfAnyUnquoted(['{', '}'], '"', '"', BlockEnd + 1);
        if BlockEnd < 0 then break;
        case S[BlockEnd + 1] of
            '{': nest += 1;
            '}': nest -= 1;
        end;
        if nest = 0 then break;
    end;
    if (BlockStart < 0) or (BlockEnd < 0) or (nest <> 0) then
        raise EFormatError.Create({$I %CURRENTROUTINE%});
end;

procedure ExtractBracketBlock(var S: String; out D: String;
    BlockStart: SizeInt);
var BlockEnd: SizeInt;
begin
    if (BlockStart = -1) then
        LocateBracketBlock(S, 0, BlockStart, BlockEnd)
    else
        LocateBracketBlock(S, BlockStart, BlockStart, BlockEnd);

    D := S.Substring(BlockStart, BlockEnd - BlockStart + 1);
    S := S.Remove(BlockStart, BlockEnd - BlockStart + 1); // ; is left in place
end;

procedure ExtractField(const F: String; var S: String;
    out D: String; mode: ExtractFieldMode);
var BlockStart, FieldStart, FieldEnd: SizeInt;
begin
    // extract 'F=D;' string
    FieldStart := S.IndexOf(F + '=');
    if FieldStart < 0 then
        raise EFormatError.Create(Format('%s: %s',
                                         [{$I %CURRENTROUTINE%}, F]));
    if mode = efValue then
    begin
        FieldEnd := S.IndexOfUnQuoted(';', '"', '"', FieldStart);
        if FieldEnd < 0 then
            raise EFormatError.Create(Format('%s: %s',
                                             [{$I %CURRENTROUTINE%}, F]));
    end;
    if mode = efBlock then
    begin
        LocateBracketBlock(S, FieldStart, BlockStart, FieldEnd);
    end;

    // extract D from 'F=D;' string
    if mode = efValue then
    begin
        D := S.Substring(FieldStart, FieldEnd - FieldStart + 1);
        D := D.Substring(D.IndexOf('=') + 1,
                         D.IndexOf(';') - D.IndexOf('=') - 1);
    end;
    if mode = efBlock then
    begin
        ExtractBracketBlock(S, D, BlockStart);
        FieldEnd := S.IndexOfUnQuoted(';', '"', '"', FieldStart);
    end;
    S := S.Remove(FieldStart, FieldEnd - FieldStart + 1);
end;

procedure ExtractIDList(var S: String; var D: TIDList);
var
    Val: String;
    Vals: TStringArray;
begin
    D.Clear();
    TrimBrackets(S);
    Vals := S.Split(',', TStringSplitOptions.ExcludeEmpty);
    for Val in Vals do
        D.Add(Val.ToInt64);
    S := '';
end;

function DelSpaceUnqoted(var S: String): Boolean;
var i: SizeInt = 0;
begin
    while i < High(S) do
    begin
        if (S[i+1] = ' ') or (S[i+1] = #13) or (S[i+1] = #10) then
            S := S.Remove(i, 1)
        else
        begin
            if S[i+1] = '"' then begin
                i := S.IndexOfUnQuoted(';', '"', '"', i);
                if i < 0 then exit(False);
            end;
            i += 1
        end;
    end;
    Result := True;
end;

end.

