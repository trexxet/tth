unit TButtonControlUtils;

{$mode ObjFPC}{$H+}

interface

uses
    Classes, SysUtils, StdCtrls, ECEditBtns;

type

    { TButtonControlHelper }
    TButtonControlHelper = class helper for TButtonControl
        procedure SetCheckedSilent(const AValue: Boolean);
    end;

    { TECBitBtnControlHelper }
    TECBitBtnControlHelper = class helper for TCustomECBitBtn
        procedure SetCheckedSilent(const AValue: Boolean);
    end;

implementation

{ TButtonControlHelper }

procedure TButtonControlHelper.SetCheckedSilent(const AValue: Boolean);
var OnChangeSave: TNotifyEvent;
begin
    OnChangeSave := OnChange;
    OnChange := nil;
    Checked := AValue;
    OnChange := OnChangeSave;
end;

{ TECBitBtnControlHelper }

procedure TECBitBtnControlHelper.SetCheckedSilent(const AValue: Boolean);
var OnChangeSave: TNotifyEvent;
begin
    OnChangeSave := OnChange;
    OnChange := nil;
    Checked := AValue;
    OnChange := OnChangeSave;
end;

end.

