unit TColorComboUtils;

{$mode ObjFPC}{$H+}

interface

uses
    Classes, SysUtils, Graphics, ECEditBtns;

type

    { TTECColorComboHelper }
    TTECColorComboHelper = class helper for TECColorCombo
        procedure SetColorSilent(const AValue: TColor);
    end;

implementation

{ TTECColorComboHelper }

procedure TTECColorComboHelper.SetColorSilent(const AValue: TColor);
var OnCustomColorChangedSave: TNotifyEvent;
begin
    OnCustomColorChangedSave := OnCustomColorChanged;
    OnCustomColorChanged := nil;
    CustomColor := AValue;
    OnCustomColorChanged := OnCustomColorChangedSave;
end;

end.

