unit USTimeBounds;

{$mode ObjFPC}{$H+}

interface

uses
    SysUtils, USTime;

function GetTimeBounds(T: TTimeInt; out LowBound, HighBound: TTimeInt): Boolean;
function GetClosestRoundTime(T: TTimeInt; out R: TTimeInt): Boolean;
// Returns True if negative
function ExtractSignAndAbs(var T: Integer): Boolean;

// Used to round the time
const WrapDivs: Array of TTimeInt =
    (3600, 1800, 1200, 900, 720, 600, 450, 400, 360, 300, 240, 225, 200, 180,
     150, 144, 120, 100, 90, 80, 75, 72, 60, 50, 48, 45, 40, 36, 30, 25, 24, 20,
     18, 16, 15, 12, 10, 9, 8, 6, 5, 4, 3, 2, 1);

implementation

uses Math;

function GetTimeBounds(T: TTimeInt; out LowBound, HighBound: TTimeInt): Boolean;
var i: SizeUInt;
begin
    Result := False;
    if T <= TTimeInt.Wrap then
    begin
        for i := Low(WrapDivs) + 1 to High(WrapDivs) do
            if WrapDivs[i] <= T then
            begin
                LowBound := WrapDivs[i];
                HighBound := WrapDivs[i-1];
                Result := True;
                break;
            end;
    end else
    begin
        LowBound := T div TTimeInt.Wrap;
        HighBound := (LowBound + 1) * TTimeInt.Wrap;
        LowBound := LowBound * TTimeInt.Wrap;
        Result := True;
    end;
end;

function GetClosestRoundTime(T: TTimeInt; out R: TTimeInt): Boolean;
var LowBound, HighBound: TTimeInt;
begin
    Result := GetTimeBounds(T, LowBound, HighBound);
    if not Result then exit;
    R := specialize IfThen<TTimeInt>(abs(T - LowBound) < abs(T - HighBound),
                                     LowBound, HighBound);
end;

function ExtractSignAndAbs(var T: Integer): Boolean;
begin
    Result := T < 0;
    if Result then T := -T;
end;

end.

