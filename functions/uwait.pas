unit UWait;

{$mode ObjFPC}{$H+}
{$modeswitch nestedprocvars}

interface

uses
    SysUtils;

type
    TWaitForCondition = function(): Boolean of object;
    TWaitForConditionNested = function(): Boolean is nested;

procedure WaitFor(F: TWaitForCondition); overload;
procedure WaitFor(F: TWaitForConditionNested); overload;

const SleepInterval: Cardinal = 5;

implementation

uses Forms;

procedure WaitFor(F: TWaitForCondition);
begin
    while (not F()) do begin
        Application.ProcessMessages();
        Sleep(SleepInterval);
    end;
end;

procedure WaitFor(F: TWaitForConditionNested);
begin
    while (not F()) do begin
        Application.ProcessMessages();
        Sleep(SleepInterval);
    end;
end;

end.

