unit TListControlViewUtils;

{$mode ObjFPC}{$H+}

interface

uses
    Classes, SysUtils, Graphics, Controls, StdCtrls, LCLType;

    procedure DrawColorMark(Canvas: TCanvas; constref ARect: TRect; BG: TColor);
    procedure DrawItemWithColorMark(ListBox: TListBox; Index: Integer;
        ARect: TRect; State: TOwnerDrawState; HasColors: Boolean; BG, FG: TColor;
        IndexHovered: Integer = -1);
    procedure GetLongItemHint(ListBox: TListBox; Index: Integer; HintInfo: PHintInfo;
        HasColors: Boolean); overload;
    procedure GetLongItemHint(ListBox: TListBox; HintInfo: PHintInfo;
        HasColors: Boolean); overload;

const
    ColorMarkSize = 12;
    ColorMarkSpace = 2;
    ColorMarkWidthWithSpace = ColorMarkSize + 2 * ColorMarkSpace;

implementation

procedure DrawColorMark(Canvas: TCanvas; constref ARect: TRect; BG: TColor);
var
    ColorMark: TRect;
    BGSave, FGSave: TColor;
begin
    with Canvas do
    begin
        BGSave := Brush.Color;
        FGSave := Pen.Color;
        with ColorMark do
        begin
            Top    := ARect.Top + 1 + (ARect.Height - ColorMarkSize) div 2;
            Left   := ColorMarkSpace;
            Width  := ColorMarkSize;
            Height := ColorMarkSize;
        end;
        Brush.Color := BG;
        Pen.Color := clBlack;
        Rectangle(ColorMark);
        Brush.Color := BGSave;
        Pen.Color := FGSave;
    end;
end;

procedure DrawItemWithColorMark(ListBox: TListBox; Index: Integer;
    ARect: TRect; State: TOwnerDrawState; HasColors: Boolean; BG, FG: TColor;
    IndexHovered: Integer);
var TextLeft: Integer;
begin
    with ListBox.Canvas do
    begin
        if (not ListBox.Focused) and (Index = ListBox.ItemIndex) then begin
            Font.Color := clWindowText;
            Brush.Color := clSkyBlue;
        end;
        FillRect(ARect);

        if (IndexHovered >= 0) and (IndexHovered = Index) then
            Font.Style := Font.Style + [fsUnderline];

        TextLeft := ARect.Left;
        if HasColors then
        begin
            DrawColorMark(ListBox.Canvas, ARect, BG);
            TextLeft += ColorMarkWidthWithSpace;
        end;
        TextOut(TextLeft, ARect.Top, ListBox.Items[Index]);
    end;
end;

procedure GetLongItemHint(ListBox: TListBox; Index: Integer;
    HintInfo: PHintInfo; HasColors: Boolean);
var
    CanvasWidth: Integer;
    Item: String;
begin
    if (HintInfo^.HintControl <> ListBox) or (Index = -1) then exit;
    Item := Listbox.Items[Index];

    // At the first moment Canvas.Width = 0, so we use this instead
    CanvasWidth := Listbox.ClientRect.Width;
    if HasColors then CanvasWidth -= ColorMarkWidthWithSpace;

    if ListBox.Canvas.TextWidth(Item) > CanvasWidth then
    begin
        HintInfo^.HintStr := Item;
        HintInfo^.HideTimeout := 1600;
    end;
end;

procedure GetLongItemHint(ListBox: TListBox; HintInfo: PHintInfo;
    HasColors: Boolean);
var Index: Integer;
begin
    if HintInfo^.HintControl <> ListBox then exit;
    Index := ListBox.ItemAtPos(HintInfo^.CursorPos, True);
    if Index = -1 then exit;
    GetLongItemHint(ListBox, Index, HintInfo, HasColors);
end;

end.

