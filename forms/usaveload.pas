unit USaveload;

{$mode ObjFPC}{$H+}

interface

uses
    Classes, SysUtils, LazFileUtils, ULogger;

type

    { TSaveLoad }
    TSaveLoad = class
    private
        Content: TStringList;
        ContentString: String;
    public
        FilePath: String;
        DisplayFilename: String;
        FilePathSetManually: Boolean;

        function LoadFile(Path: String): Boolean;
        function SaveFile(Path: String = ''): Boolean;

        procedure LoadExampleRu();

        constructor Create();
        destructor Destroy(); override;
    end;

var
    SaveLoad: TSaveLoad;

implementation

uses Main, UTimetable, UIndent, UDeserializeUtils;

{ TSaveLoad }

function TSaveLoad.LoadFile(Path: String): Boolean;
begin
    FilePath := Path;
    FilePathSetManually := True;
    Logger.Log(LINFO, 'Opening ', FilePath);

    try
        Content.LoadFromFile(FilePath);
    except
        on E: EFOpenError do
        begin
            Logger.Log(LERROR, 'ERROR: Opening %s: %s', [FilePath, E.Message]);
            exit(False);
        end;
    end;

    ContentString := Content.DelimitedText;
    if not DelSpaceUnqoted(ContentString) then begin // DelSpace() can't handle quotes
        Logger.Log(LERROR, 'ERROR: Wrong file format: %s', [FilePath]);
        exit(False);
    end;
    Logger.PushSeverity(LERROR);
    ResetTimetable();
    Result := True;
    try
        Timetable.Deserialize(ContentString);
    except
        on E: EFormatError do begin
            Logger.Log(LERROR, 'ERROR: Deserializing %s: %s', [FilePath, E.Message]);
            FreeAndNil(Timetable);
            exit(False);
        end;
    end;
    Logger.PopSeverity();
    Logger.Log(LINFO, 'Loaded ', FilePath);
    DisplayFilename := ExtractFileName(FilePath);
end;

function TSaveLoad.SaveFile(Path: String): Boolean;
begin
    if Path <> '' then
    begin
        FilePath := Path;
        FilePathSetManually := True;
    end;
    Logger.Log(LINFO, 'Saving as ', FilePath);
    Result := True;

    ContentString := Timetable.Serialize();
    Content.DelimitedText := Reindent(ContentString);
    try
        Content.SaveToFile(FilePath);
    except
        on E: EStreamError do
        begin
            Logger.Log(LERROR, 'ERROR: Saving as %s: %s', [FilePath, E.Message]);
            Result := False;
        end;
    end;
    Logger.Log(LINFO, 'Saved ', FilePath);
    DisplayFilename := ExtractFileName(FilePath);
    MainForm.FileSaved();
end;

procedure TSaveLoad.LoadExampleRu();
begin
    LoadFile(GetCurrentDir() + DirectorySeparator + 'example_ru.tth')
end;

constructor TSaveLoad.Create();
begin
    FilePath := GetCurrentDir() + DirectorySeparator + 'timetable.tth';
    DisplayFilename := '';
    FilePathSetManually := False;
    Logger.Log(LINFO, 'Set default filepath to ', FilePath);

    Content := TStringList.Create();
    Content.Delimiter := #10;
    Content.StrictDelimiter := True;
    // no idea but works for any non-whitespase char other than "
    Content.QuoteChar := '_';
    Content.Duplicates := dupAccept;
end;

destructor TSaveLoad.Destroy();
begin
    FreeAndNil(Content);
    inherited Destroy();
end;

initialization
    SaveLoad := TSaveLoad.Create();

finalization
    FreeAndNil(SaveLoad);

end.

