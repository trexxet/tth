unit Main;

{$mode objfpc}{$H+}

interface

uses
    Classes, SysUtils,
    Forms, Controls, Menus, ComCtrls, StdCtrls, ActnList, StdActns, ExtCtrls,
    About, Editor,
    URunningControl, Main_RS;

type

    { TMainForm }
    TMainForm = class(TForm)
        procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    private
        RunningControl: TRunningControl;
    {%REGION Main } published
        Icons: TImageList;
        MainEditorFrameScrollBox: TScrollBox;
        MainEditorFrame: TEditorFrame;
        MainSplitter: TSplitter;
        LogMemo: TMemo;
        procedure FormCreate(Sender: TObject);
        procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
        procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
        procedure FormResize(Sender: TObject);
        procedure FormDestroy(Sender: TObject);
    {%ENDREGION}
    {%REGION Toolbar } published
        TB: TToolBar;
        TBNew: TToolButton;
        TBOpen: TToolButton;
        TBSave: TToolButton;
        TBSaveAs: TToolButton;
        TB24H: TToolButton;
    {%ENDREGION}
    {%REGION Menu } published
        MM: TMainMenu;
        MMFile: TMenuItem;
        MMNew: TMenuItem;
        MMOpen: TMenuItem;
        MMSave: TMenuItem;
        MMSaveAs: TMenuItem;
        MMFileSep1: TMenuItem;
        MM24H: TMenuItem;
        MMFileSep2: TMenuItem;
        MMExit: TMenuItem;
        MMView: TMenuItem;
        MMViewStations: TMenuItem;
        MMViewRoutes: TMenuItem;
        MMViewStationSchedule: TMenuItem;
        MMViewDirections: TMenuItem;
        MMViewStops: TMenuItem;
        MMViewTrains: TMenuItem;
        MMAbout: TMenuItem;
        MMAboutTTH: TMenuItem;
        procedure MMLangEnClick(Sender: TObject);
        procedure MMLangRuClick(Sender: TObject);
    {%ENDREGION}
    {%REGION Actions } published
        AL: TActionList;
        ActionExit: TFileExit;
        ActionFNew: TAction;
        ActionFOpen: TFileOpen;
        ActionFSave: TAction;
        ActionFSaveAs: TFileSaveAs;
        ActionAbout: TAction;
        Action24H: TAction;
        procedure ActionFNewExecute(Sender: TObject);
        procedure ActionFOpenAccept(Sender: TObject);
        procedure ActionFSaveExecute(Sender: TObject);
        procedure ActionFSaveAsBeforeExecute(Sender: TObject);
        procedure ActionFSaveAsAccept(Sender: TObject);
        procedure ActionAboutExecute(Sender: TObject);
        procedure Action24HExecute(Sender: TObject);
    {%ENDREGION}
    public
        procedure FileSaved();
        procedure FileChanged();
        procedure SetFilenameCaption(HasFileChanged: Boolean);
        procedure AssignViewActions();
    end;

var
    MainForm: TMainForm;

implementation

uses
    LCLType, LazFileUtils, Dialogs, Graphics,
    UVersion, ULogger, ULocale,
    USaveload, UTimetable, USTime;

{$R *.lfm}

{ TMainForm }

{%REGION Form }

procedure TMainForm.Action24HExecute(Sender: TObject);
begin
    ToggleTimeMode();
    Action24H.Checked := TimeMode = RealTime;
    MainEditorFrame.TimeModeChanged(TimeMode = GameTime);
end;

procedure TMainForm.FormKeyDown(Sender: TObject; var Key: Word;
    Shift: TShiftState);
begin
    case Key of
        VK_ESCAPE: MainEditorFrame.FocusUp();
        VK_RETURN: MainEditorFrame.FocusDown();
    end;
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
    MainEditorFrameScrollBox.Color := clDefault;
    MainEditorFrame := TEditorFrame.Create(MainEditorFrameScrollBox);
    MainEditorFrame.Parent := MainEditorFrameScrollBox;

    RunningControl.FileChanged := False;
    RunningControl.Running := False;
    RunningControl.ReleaseLoading();
    FormResize(Sender);
    Logger.AttachMemo(LogMemo);

    Action24H.Checked := False;
    AssignViewActions();
end;

procedure TMainForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    if RunningControl.FileChanged then
        CanClose := MessageDlg(MainRS_UnsavedCaption, MainRS_UnsavedExitQuestion,
                               mtConfirmation, [mbYes, mbNo], 0) = mrYes
    else
        CanClose := True;
end;

procedure TMainForm.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
    if CloseAction = caFree then
        Logger.DetachMemo();
end;

procedure TMainForm.FormResize(Sender: TObject);
begin
    MainSplitter.MinSize := MainForm.Height div 6;
end;

procedure TMainForm.FormDestroy(Sender: TObject);
begin
    FreeAndNil(MainEditorFrame);
end;

{%ENDREGION}

{%REGION Menu }

procedure TMainForm.MMLangEnClick(Sender: TObject);
begin
    //MMLangEn.Checked := True;
    Locale.Lang := en;
end;

procedure TMainForm.MMLangRuClick(Sender: TObject);
begin
    //MMLangRu.Checked := True;
    Locale.Lang := ru;
end;

{%ENDREGION}

{%REGION Actions }

procedure TMainForm.ActionFNewExecute(Sender: TObject);
label fin;
begin
    if not RunningControl.AcquireLoading() then exit;
    Logger.PushSeverity(LERROR);

    if RunningControl.FileChanged then
        if MessageDlg(MainRS_UnsavedCaption, MainRS_UnsavedCreateQuestion,
                      mtConfirmation, [mbYes, mbNo], 0) <> mrYes then goto fin;

    RunningControl.InitRerun();
    ResetTimetable();
    //SaveLoad.LoadExampleRu();

    SaveLoad.FilePathSetManually := False;
    SaveLoad.DisplayFilename := 'new';
    RunningControl.FileChanged := True;
    RunningControl.Running := True;

    fin:
    Logger.PopSeverity();
    RunningControl.ReleaseLoading();
end;

procedure TMainForm.ActionFOpenAccept(Sender: TObject);
label fin;
begin
    if not RunningControl.AcquireLoading() then exit;

    if RunningControl.FileChanged then
        if MessageDlg(MainRS_UnsavedCaption, MainRS_UnsavedOpenQuestion,
                      mtConfirmation, [mbYes, mbNo], 0) <> mrYes then goto fin;

    RunningControl.InitRerun();

    RunningControl.Running := SaveLoad.LoadFile(ActionFOpen.Dialog.FileName);
    if not RunningControl.Running then
        ShowMessage(MainRS_OpenFail);
    RunningControl.FileChanged := False;

    fin: RunningControl.ReleaseLoading();
end;

procedure TMainForm.ActionFSaveExecute(Sender: TObject);
begin
    with SaveLoad do
        if FilePathSetManually then begin
            if not SaveFile() then ShowMessage(MainRS_SaveFail);
        end else
            ActionFSaveAs.Execute();
end;

procedure TMainForm.ActionFSaveAsBeforeExecute(Sender: TObject);
begin
    ActionFSaveAs.Dialog.FileName := ExtractFileNameOnly(SaveLoad.FilePath);
end;

procedure TMainForm.ActionFSaveAsAccept(Sender: TObject);
begin
    if not SaveLoad.SaveFile(ActionFSaveAs.Dialog.FileName) then
        ShowMessage(MainRS_SaveFail);
end;

procedure TMainForm.ActionAboutExecute(Sender: TObject);
begin
    AboutForm.ShowModal();
end;

{%ENDREGION}

procedure TMainForm.FileSaved();
begin
    RunningControl.FileChanged := False;
end;

procedure TMainForm.FileChanged();
begin
    RunningControl.FileChanged := True;
end;

procedure TMainForm.SetFilenameCaption(HasFileChanged: Boolean);
var Cap, Fmt: String;
begin
    if SaveLoad.DisplayFilename = '' then exit;
    Fmt := specialize IfThen<String>(HasFileChanged, '*%s - %s', '%s - %s');
    Cap := Format(Fmt, [SaveLoad.DisplayFilename, TTH_NAME]);
    Caption := Cap;
    Application.Title := Cap;
end;

procedure TMainForm.AssignViewActions();
begin
    if MainEditorFrame = nil then exit;
    MMViewStations.Action := MainEditorFrame.SwitchTabStations;
    MMViewRoutes.Action := MainEditorFrame.SwitchTabRoutes;
    MMViewStationSchedule.Action := MainEditorFrame.SwitchTabStationSchedule;
    MMViewDirections.Action := MainEditorFrame.SwitchTabDirections;
    MMViewStops.Action := MainEditorFrame.SwitchTabStops;
    MMViewTrains.Action := MainEditorFrame.SwitchTabTrains;
end;

end.
