// Unfortunately, the i18n support in Lazarus is fucked beyond repair.
// ALL of the translators (LRS, LCL, rk) can't handle a fucking frame
// created dynamically; instead, they cause a fucking memory leak at opening
// .po file. I'm quitting the attempts to translate the program after wasting
// several hours on the braindamaged pascal shit.

unit ULocale;

{$mode ObjFPC}{$H+}

interface

uses
    SysUtils;

type

    { TLocale }

    TLocale = object
        type TLang = (en, ru);
    private
        //FCurrLang: TLang; static;
        procedure SetLang(const ALang: TLang); static;
    public
        property Lang: TLang write SetLang;
        constructor Create();
    end;

var
    Locale: TLocale;

implementation

// uses rkResTrans, ULogger;

procedure TLocale.SetLang(const ALang: TLang);
//var
//    LangStr: String;
begin
//    FCurrLang := ALang;
//    Str(ALang, LangStr);
//    TranslateFromResources(LangStr);
//    Logger.Log(LINFO, 'Switched lang to ', LangStr);
end;

constructor TLocale.Create();
begin
    Lang := en;
end;

end.

