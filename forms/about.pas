unit About;

{$mode ObjFPC}{$H+}

interface

uses
    Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls,
    ECLink, UVersion;

type

    { TAboutForm }

    TAboutForm = class(TForm)
        CopyrightLabel: TLabel;
        ManualLink: TECLink;
        IconsLink: TECLink;
        Images: TImageList;
        OtherInfoLabel: TLabel;
        LicenseLabel: TLabel;
        NameLabel: TLabel;
        Logo: TImage;
        InfoPanel: TPanel;
        procedure FormKeyDown(Sender: TObject; var Key: Word;
            Shift: TShiftState);
        procedure FormShow(Sender: TObject);
        procedure LogoClick(Sender: TObject);
    private
        LogoClicks: Byte;
    end;

var
    AboutForm: TAboutForm;

implementation

uses LCLType;

{$R *.lfm}

{ TAboutForm }

procedure TAboutForm.FormShow(Sender: TObject);
begin
    Caption := Format('%s v%s', [TTH_NAME_SHORT, GetTTHVersion()]);
    NameLabel.Caption := Format('%s v%s', [TTH_NAME, GetTTHVersion()]);
    CopyrightLabel.Caption := GetTTHAuthor();
    LicenseLabel.Caption := TTH_LICENCSE;

    LogoClicks := 0;
    Logo.ImageIndex := 0;
end;

procedure TAboutForm.FormKeyDown(Sender: TObject; var Key: Word;
    Shift: TShiftState);
begin
    if Key = VK_ESCAPE then Close();
end;

procedure TAboutForm.LogoClick(Sender: TObject);
begin
    LogoClicks += 1;
    if LogoClicks = 5 then begin
        LogoClicks := 0;
        Logo.ImageIndex := 1;
    end;
end;

end.

