unit ULogger;

{$mode ObjFPC}{$H+}

interface

uses
    Classes, SysUtils, StdCtrls, Generics.Collections, LazLoggerBase;

type

    LogSeverity = (LINFO, LERROR);

    { TLogger }
    TLogger = object
    private
        Memo: TMemo;
        FSeverity: LogSeverity;
    public
        SeverityStack: specialize TStack<LogSeverity>;
        procedure AttachMemo(_Memo: TMemo);
        procedure DetachMemo();
        constructor Create();
        destructor Destroy();
    public
        property Severity: LogSeverity read FSeverity;
        procedure PushSeverity(Level: LogSeverity);
        procedure PopSeverity();

        // Wrappers for DebugLn
        procedure Log(Level: LogSeverity; const S: string = ''); overload;
        // similar to Format(S, Args)
        procedure Log(Level: LogSeverity; const S: String; Args: array of const); overload;
        procedure Log(Level: LogSeverity; const s1, s2: string; const s3: string = '';
                      const s4: string = ''; const s5: string = ''; const s6: string = '';
                      const s7: string = ''; const s8: string = ''; const s9: string = '';
                      const s10: string = ''); overload;
    end;

var
    Logger: TLogger;

implementation

{ TLogger }

procedure TLogger.AttachMemo(_Memo: TMemo);
begin
    Memo := _Memo;
end;

procedure TLogger.DetachMemo();
begin
    Memo := nil;
end;

constructor TLogger.Create();
begin
    DetachMemo();
    SeverityStack := specialize TStack<LogSeverity>.Create();
    FSeverity := LINFO;
end;

destructor TLogger.Destroy();
begin
    FreeAndNil(SeverityStack);
end;

procedure TLogger.PushSeverity(Level: LogSeverity);
begin
    SeverityStack.Push(FSeverity);
    FSeverity := Level;
end;

procedure TLogger.PopSeverity();
begin
    FSeverity := SeverityStack.Pop();
end;

procedure TLogger.Log(Level: LogSeverity; const S: string);
begin
    if Level < FSeverity then exit;
    DebugLn(S);
    if Memo <> nil then Memo.Lines.Add(S);
end;

procedure TLogger.Log(Level: LogSeverity; const S: String; Args: array of const);
var Line: String;
begin
    if Level < FSeverity then exit;
    Line := Format(S, Args);
    DebugLn(Line);
    if Memo <> nil then Memo.Lines.Add(Line);
end;

procedure TLogger.Log(Level: LogSeverity; const s1, s2: string; const s3: string;
    const s4: string; const s5: string; const s6: string; const s7: string;
    const s8: string; const s9: string; const s10: string);
var Line: String;
begin
    if Level < FSeverity then exit;
    Line := s1+s2+s3+s4+s5+s6+s7+s8+s9+s10;
    DebugLn(Line);
    if Memo <> nil then Memo.Lines.Add(Line);
end;

initialization
    Logger.Create();

finalization
    Logger.Destroy();

end.

