program tth;

{$mode objfpc}{$H+}

uses
    {$IFDEF UNIX}
    cthreads,
    {$ENDIF}
    {$IFDEF HASAMIGA}
    athreads,
    {$ENDIF}
    Interfaces, // this includes the LCL widgetset
    LazLogger, ULogger, ULocale,
    Forms, Main, About, Editor, ScheduleEditor,
    ScheduleView, StopScheduleView, DirectionScheduleView,
    RouteStopParams,
    ListControl, PickerStation,
    EntityListView, StationRoutesView,
    HMTEditFrame, HMTSetter,
    UEditorLCActions,
    USaveload, UTimetable, UDirection, StationScheduleView;

{$R *.res}

begin
    RequireDerivedFormResource:=True;
    Application.Scaled := True;
    Application.Initialize;
    Application.CreateForm(TMainForm, MainForm);
    Application.CreateForm(TPickerStationForm, PickerStationForm);
    Application.CreateForm(THMTSetterForm, HMTSetterForm);
    Application.CreateForm(TAboutForm, AboutForm);
    Application.Run;
end.

